<?php

/**
 * Settings default controller
 * @author James Makau <jacjimus@gmail.com>
 */
class DefaultController extends CustomizationModuleController {

        public function init()
        {
                parent::init();
                $this->resourceLabel = Lang::t('Settings Module');
                $this->resource = UserResources::RES_SETTINGS_GENERAL;
                $this->activeMenu = self::MENU_SETTINGS_GENERAL;
                $this->showPageTitle = false;
        }

        /**
         * @return array action filters
         */
        public function filters()
        {
                return array(
                    'accessControl', // perform access control for CRUD operations
                );
        }

        /**
         * Specifies the access control rules.
         * This method is used by the 'accessControl' filter.
         * @return array access control rules
         */
        public function accessRules()
        {
                return array(
                    array('allow',
                        'actions' => array('index', 'runtime', 'update', 'sms', 'create', 'delete' , 'activate' , 'signatures'),
                        'users' => array('@'),
                    ),
                    array('deny', // deny all users
                        'users' => array('*'),
                    ),
                );
        }

        public function actionIndex()
        {
                $this->resource = UserResources::RES_SETTINGS_GENERAL;
                $this->hasPrivilege(Acl::ACTION_VIEW);
                $this->showPageTitle = TRUE;
                $this->pageTitle = "Main Account Configuration";
                $this->activeMenu = self::MENU_SETTINGS_GENERAL;
                $accounts = new Accounts();
                if (isset($_POST['settings'])) {
                        $this->hasPrivilege(Acl::ACTION_UPDATE);

                        foreach ($_POST['settings'] as $key => $value) {
                                if (!empty($value)):
                                    if($key == Constants::KEY_LOGO){
                                       $value =  $accounts->setProfileImage($value , false);
                                   }
                                   Yii::app()->settings->set(Constants::CATEGORY_GENERAL, $key, $value);
                        
                                endif;
                        }
                       Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                        $this->refresh();
                }

                $settings = Yii::app()->settings->get(Constants::CATEGORY_GENERAL, array(
                    Constants::KEY_ADMIN_EMAIL,
                    Constants::KEY_COMPANY_NAME,
                    Constants::KEY_COMPANY_EMAIL,
                    Constants::KEY_CURRENCY,
                    Constants::KEY_PAGINATION,
                    Constants::KEY_SITE_NAME,
                    Constants::KEY_MIN_SMS_THREASHHOLD,
                    Constants::KEY_VENDOR_EMAIL,
                    Constants::KEY_MAIN_ACC_BALANCE,
                    Constants::KEY_DEFAULT_TIMEZONE,
                    Constants::KEY_LOGO,
                ));
               $this->render('index', array(
                    'settings' => $settings,
                ));
        }
/**
         * Creates a new model.
         * If creation is successful, the browser will be redirected to the 'view' page.
         */
        public function actionCreate()
        {
                $this->hasPrivilege(Acl::ACTION_CREATE);
                 $this->resourceLabel = "Sms Signatures";
                 $this->pageTitle = Lang::t('Add ' . $this->resourceLabel);

                $model = new Signatures;
                $model_class_name = $model->getClassName();

                if (isset($_POST[$model_class_name])) {
                   
                        $model->attributes = $_POST[$model_class_name];
                         if ($model->save()) {
                                Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                                $this->redirect(array('sms'));
                        }
 
                }

                $this->render('create', array(
                    'model' => $model,
                ));
        }

        /**
         * Updates a particular model.
         * If update is successful, the browser will be redirected to the 'view' page.
         * @param integer $id the ID of the model to be updated
         */
        public function actionUpdate($id)
        {
                $this->hasPrivilege(Acl::ACTION_UPDATE);
                $this->resourceLabel = "Notification template";
                $this->pageTitle = Lang::t('Edit ' . $this->resourceLabel);


                $model = Signatures::model()->loadModel($id);
                $model_class_name = $model->getClassName();

                if (isset($_POST[$model_class_name])) {
                        $model->attributes = $_POST[$model_class_name];
                        if ($model->save()) {
                                Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                                $this->redirect(array('signatures'));
                        }
                }

                $this->render('update', array(
                    'model' => $model,
                ));
        }

        /**
         * Deletes a particular model.
         * If deletion is successful, the browser will be redirected to the 'admin' page.
         * @param integer $id the ID of the model to be deleted
         */
        public function actionDelete($id)
        {
                Acl::hasPrivilege($this->privileges, $this->resource, Acl::ACTION_DELETE);
                Signatures::model()->loadModel($id)->delete();
                if (!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        
        public function actionActivate($id , $st = FALSE)
    {
        $this->hasPrivilege(Acl::ACTION_UPDATE);
         $model = Signatures::model()->loadModel($id);
        $model->Status = $st == FALSE ? Signatures::STATUS_INACTIVE : Signatures::STATUS_ACTIVE;
              $model->save(FALSE);

                if (!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('sms'));
                 echo CHtml::script("$('#preloader').hide()");
    }

        public function actionRuntime()
        {
                $this->resource = UserResources::RES_SETTINGS_RUNTIME;
                $this->hasPrivilege(Acl::ACTION_VIEW);
                $this->pageTitle = Lang::t('System runtime logs');
                $this->activeMenu = self::MENU_SETTINGS_RUNTIME;
                $this->showPageTitle = TRUE;

                $log_file = '';
                if (isset($_POST['log_file'])) {
                        $log_file = $_POST['log_file'];
                }

                $base_path = Yii::getPathOfAlias('application.runtime') . DS;
                if (empty($log_file))
                        $log_file = $base_path . 'application.log';

                if (isset($_POST['clear'])) {
                        $this->hasPrivilege(Acl::ACTION_DELETE);
                        if (file_exists($log_file)) {
                                @unlink($log_file);
                        }
                }

                $log_files = Common::getDirectoryFiles($base_path . 'application.log*');

                $this->render('runtime', array(
                    'log_files' => $log_files,
                    'log_file' => $log_file,
                ));
        }

        public function actionSignatures()
        {
                $this->resource = UserResources::RES_SETTINGS_SIGNATURES;
                $this->hasPrivilege(Acl::ACTION_VIEW);
                $this->showPageTitle = FALSE;
                $this->activeMenu = self::MENU_SETTINGS_SMS;
                $this->pageTitle = 'Sms ' . $this->resourceLabel;

                $model = new Signatures;
                $model_class_name = $model->getClassName();
                if (isset($_POST[$model_class_name])) {
                        $model->attributes = $_POST[$model_class_name];
                        if($model->save())
                        Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                        $this->refresh();
                }
                
                
                $this->render('sms', array(
                    'model' => $model,
                ));
        }
        public function actionSms()
        {
                $this->hasPrivilege(Acl::ACTION_VIEW);
                $this->resourceLabel = "Notification template";
                
                $this->pageTitle = Lang::t('Manage ' . $this->resourceLabel);

                $this->render('sms', array(
                    'model' => SettingsSmsTemplate::model()->searchModel(array(), $this->settings[Constants::KEY_PAGINATION],
                            'id' ,"sms_type = '". SettingsSmsTemplate::SMS_NOTIFY."'"),
                ));
        }

}

