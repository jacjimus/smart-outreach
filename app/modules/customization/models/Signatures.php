<?php

/**
 * This is the model class for table "settings_email_template".
 *
 * The followings are the available columns in table 'settings_email_template':
 * @property integer $id
 * @property string $key
 * @property string $description
 * @property string $body
 * @property string $frequency
 * @property string $repeat
 * @property string $time_to_send
 * @property string $days
 * @property string $comments
 * @property string $date_created
 * @property integer $created_by
 * @property integer $Status
 * @property integer $target
 * @property integer $sms_type
 */
class Signatures extends ActiveRecord implements IMyActiveSearch {
        //email template keys

        
        const STATUS_ACTIVE = "Active";
        const STATUS_INACTIVE = "Inactive";
       
        /**
         * Returns the static model of the specified AR class.
         * @param string $className active record class name.
         * @return SettingsEmailTemplate the static model class
         */
        
        
        public static function model($className = __CLASS__)
        {
                return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName()
        {
                return 'tbl_sms_signatures';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules()
        {

                return array(
                    array('name, body', 'required'),
                    array('created_by', 'numerical', 'integerOnly' => true),
                    array('name', 'length', 'max' => 25),
                    array('body', 'length', 'max' => 160),
                    array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
                );
        }


        /**
         * @return array relational rules.
         */
        public function relations()
        {
                return array(
                );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels()
        {
                return array(
                    'id' => Lang::t('ID'),
                    
                    'body' => Lang::t('Signature'),
                    'name' => Lang::t('Name'),
                    'Status' => Lang::t('Status'),
                   
                );
        }

        public function searchParams()
        {
                return array(
                    array('id', self::SEARCH_FIELD, true, 'OR'),
                    array('name', self::SEARCH_FIELD, true, 'OR'),
                    array('body', self::SEARCH_FIELD, true, 'OR'),
                   
                    array('Status', self::SEARCH_FIELD, true, 'OR'),
                    'id',
                );
        }

        /**
         * Find email template by key
         * @param type $key
         * @return type
         * @throws CHttpException
         */
        public function findByKey($key)
        {
                $model = $this->find('`key`=:t1', array(':t1' => $key));
                if ($model === null)
                        throw new CHttpException(500, 'No SMS template with the given key: ' . $key);
                return $model;
        }

}

