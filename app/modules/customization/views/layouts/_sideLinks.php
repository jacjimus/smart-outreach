<?php if ($this->showLink(UserResources::RES_SETTINGS_GENERAL)): ?>
<li class="<?php echo $this->getModuleName() === 'customization' ? 'active open' : '' ?>">
        <a href="<?php echo Yii::app()->createUrl('customization/default/index') ?>" class="dropdown-toggle">
                <i class="icon-wrench"></i>
                <span class="menu-text"><?php echo Lang::t('Customization Module') ?></span>
                <b class="arrow icon-angle-down"></b>
        </a>
        <ul class="submenu">
                <?php if ($this->showLink(UserResources::RES_SETTINGS_GENERAL)): ?>
            <li class="<?php echo $this->activeMenu === CustomizationModuleController::MENU_SETTINGS_GENERAL ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('customization/default/index') ?>"><i class="icon-double-angle-right"></i> <?php echo Lang::t('Main account customization') ?></a></li>
                <?php endif; ?>
            
                <?php if ($this->showLink(UserResources::RES_SETTINGS_SIGNATURES)): ?>
            <li class="<?php echo $this->activeMenu === CustomizationModuleController::MENU_SETTINGS_SIGNATURES ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('customization/default/signatures') ?>"><i class="icon-double-angle-right"></i> <?php echo Lang::t('Manage SMS Signatures') ?></a></li>
                <?php endif; ?>
                

                <?php if ($this->showLink(UserResources::RES_SETTINGS_RUNTIME)): ?>
                        <li class="<?php echo $this->activeMenu === CustomizationModuleController::MENU_SETTINGS_RUNTIME ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('customization/default/runtime') ?>"><i class="icon-double-angle-right"></i> <?php echo Lang::t('Runtime Logs') ?></a></li>
                <?php endif; ?>
                
                <?php if ($this->showLink(UserResources::RES_MODULES_ENABLED)): ?>
                        <li class="<?php echo $this->activeMenu === CustomizationModuleController::MENU_MODULES_ENABLED ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('customization/modules/index') ?>"><i class="icon-double-angle-right"></i> <?php echo Lang::t('Manage Modules') ?></a></li>
                        <?php endif; ?>
        </ul>
</li>
<?php endif; ?>