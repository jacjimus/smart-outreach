<h4 class="lighter">SMS Configuration</h4>
<div class="widget-toolbar no-border">
        <ul class="nav nav-tabs my-nav">
                 <li class="<?php echo $this->activeTab === 1 ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('settings/default/sms') ?>"><?php echo Lang::t('Notification SMS Templates') ?></a></li>
                 <li class="<?php echo $this->activeTab === 2 ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('settings/smsTemplate/index') ?>"><?php echo Lang::t('Custome SMS Templates') ?></a></li>
               
        </ul>
</div>
