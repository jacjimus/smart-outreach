
<?php

$grid_id = 'sms-templates-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Lang::t('Manage Custome SMS Templates'),
    'titleIcon' => null,
    'showExportButton' => false,
    'showSearch' => true,
    'createButton' => array('visible'=> $this->showlink( $this->resource , Acl::ACTION_CREATE )?true:false),
    'toolbarButtons' => array(),
    'showRefreshButton' => false,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        
        'columns' => array(
        array(
            'name' => 'key',
            'visible' => true 
        ),
        array(
            'name' => 'description',
            'type' => 'raw',
            'value' => 'CHtml::link(CHtml::encode($data->description),Yii::app()->controller->createUrl("update",array("id"=>$data->primaryKey)))',
        ),
             array(
            'header' => 'Send Date',
            'name' => 'frequency',
            'visible' => true 
        ), 
            
             array(
            'name' => 'target',
            'visible' => true 
        ), 
            array(
            'name' => 'time_to_send',
            'visible' => true 
        ),
            
       array(
            'name' => 'Status',
            'type' => 'raw',
            'value' => 'CHtml::tag("span", array("class"=>$data->Status=="Active"?"badge badge-success":"badge badge-danger"), $data->Status)',
        ),
            
        array(
            'class' => 'ButtonColumn',
            'template' => '{update}&nbsp;&nbsp;&nbsp;&nbsp;{delete}{deactivate}{activate}',
            'htmlOptions' => array('class' => 'text-center', 'style' => 'width: 100px;'),
            'buttons' => array(
                'activate' => array(
                            'imageUrl' => false,
                            'label' => '<i class="fa fa-ban fa-2x text-danger"></i>',
                            'url' => 'Yii::app()->controller->createUrl("activate",array("id"=>$data->id))',
                            'visible' => '$this->grid->owner->showlink("' . $this->resource . '","' . Acl::ACTION_UPDATE . '") AND $data->Status == "Active" ?true:false',
                            'url_attribute' => 'data-ajax-url',
                            'options' => array(
                                'data-grid_id' => $grid_id,
                                'data-confirm' => Lang::t('Do you want to Deativate this Sms Template?'),
                                'class' => 'my-update-grid',
                                'title' => Lang::t('Deactivate Sms Template?'),
                            ),
                        ),
                'deactivate' => array(
                            'imageUrl' => false,
                            'label' => '<i class="fa fa-check-circle fa-2x text-success"></i>',
                            'url' => 'Yii::app()->controller->createUrl("activate",array("id"=>$data->id , "st" => TRUE))',
                            'visible' => '$this->grid->owner->showlink("' . $this->resource . '","' . Acl::ACTION_UPDATE . '") AND $data->Status == "Inactive" ?true:false',
                            'url_attribute' => 'data-ajax-url',
                            'options' => array(
                                'data-grid_id' => $grid_id,
                                'data-confirm' => Lang::t('Do you want to Activate this Sms Template?'),
                                'class' => 'my-update-grid',
                                'title' => Lang::t('Activate Sms Template?'),
                            ),
                        ),
                'update' => array(
                    'imageUrl' => false,
                    'label' => '<i class="icon-pencil bigger-130"></i>',
                    'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->primaryKey))',
                    'visible' => '$this->grid->owner->showLink("' . UserResources::RES_SETTINGS_SMS . '", "' . Acl::ACTION_UPDATE . '")?true:false',
                    'options' => array(
                        'class' => 'green',
                        'title' => 'Edit',
                    ),
                ),
                'delete' => array(
                    'imageUrl' => false,
                    'label' => '<i class="icon-trash bigger-130"></i>',
                    'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->primaryKey))',
                    'visible' => '$this->grid->owner->showLink("' . UserResources::RES_SETTINGS_SMS . '", "' . Acl::ACTION_DELETE . '")?true:false',
                    'options' => array(
                        'class' => 'delete red',
                        'title' => 'Delete',
                    ),
                ),
            )
        ),
    ),
)
)

        );
?>