<?php
$this->breadcrumbs = array(
    $this->pageTitle,
);
?>
<?php echo CHtml::beginForm(Yii::app()->createUrl($this->route), 'POST', array('class' => 'form-horizontal', 'role' => 'form')) ?>
<div class="form-group">
        <label class="col-lg-3 control-label">System Name:  </label>
        <div class="col-lg-5">
                <?php echo CHtml::textField('settings[' . Constants::KEY_SITE_NAME . ']', $settings[Constants::KEY_SITE_NAME], array('class' => 'form-control')) ?>
        </div>
</div>
<div class="form-group">
        <label class="col-lg-3 control-label">Facility Name:</label>
        <div class="col-lg-5">
                <?php echo CHtml::textField('settings[' . Constants::KEY_COMPANY_NAME . ']', $settings[Constants::KEY_COMPANY_NAME], array('class' => 'form-control')) ?>
        </div>
</div>
<div class="form-group">
        <label class="col-lg-3 control-label">Facility Email:</label>
        <div class="col-lg-5">
                <?php echo CHtml::textField('settings[' . Constants::KEY_COMPANY_EMAIL . ']', $settings[Constants::KEY_COMPANY_EMAIL], array('class' => 'form-control', 'type' => 'email')) ?>
        </div>
</div>

<div class="form-group">
        <label class="col-lg-3 control-label">Pagination:  </label>
        <div class="col-lg-5">
                <?php echo CHtml::dropDownList('settings[' . Constants::KEY_PAGINATION . ']', $settings[Constants::KEY_PAGINATION], Common::generateIntergersList(10, 100, 10), array('class' => 'form-control')) ?>
        </div>
</div>
<div class="form-group">
        <label class="col-lg-3 control-label">Main Account Balance:  </label>
        <div class="col-lg-5">
                <?php echo CHtml::textField('settings[' . Constants::KEY_MAIN_ACC_BALANCE . ']', $settings[Constants::KEY_MAIN_ACC_BALANCE], array('class' => 'form-control', 'readonly' => true)) ?>
       </div>
</div>
<div class="form-group">
        <label class="col-lg-3 control-label">Minimum Credit Threshold:  </label>
        <div class="col-lg-5">
                <?php echo CHtml::numberField('settings[' . Constants::KEY_MIN_SMS_THREASHHOLD . ']', $settings[Constants::KEY_MIN_SMS_THREASHHOLD], array('class' => 'form-control', 'type' => 'number')) ?>
       </div>
</div>
<div class="form-group">
        <label class="col-lg-3 control-label">Vendor Email:  </label>
        <div class="col-lg-5">
                <?php echo CHtml::emailField('settings[' . Constants::KEY_VENDOR_EMAIL . ']', $settings[Constants::KEY_VENDOR_EMAIL], array('class' => 'form-control', 'type' => 'number')) ?>
       </div>
</div>
<div class="form-group">
        <label class="col-lg-3 control-label">Logo Image:  </label>
        <div class="col-lg-5">
                 <?php $this->renderPartial('_image_field', array('settings' => $settings, 'htmlOptions' => array('field_class' => 'col-md-9'))) ?>
       </div>
</div>

<?php if($this->showLink($this->resource, Acl::ACTION_UPDATE )):?>
<div class="clearfix form-actions">
        <div class="col-lg-offset-2 col-lg-9">
                <button class="btn btn-primary" <?php echo!$this->showLink($this->resource, Acl::ACTION_UPDATE) ? 'disabled="disabled"' : null ?> type="submit"><i class="icon-ok bigger-110"></i> Save</button>
                &nbsp; &nbsp; &nbsp;
                <a class="btn" href="<?php echo Yii::app()->createUrl('index') ?>"><i class="icon-remove bigger-110"></i>Cancel</a>
        </div>
</div>
<?php
endif;
echo CHtml::endForm() ?>