
<div class="alert alert-block alert-success">
    <button type="button" class="close" data-dismiss="alert">
        <i class="ace-icon fa fa-times"></i>
    </button>

    <i class="ace-icon fa fa-check green"></i>

    Welcome to
    <strong class="green">

        <small><?php echo CHtml::encode($this->settings[Constants::KEY_SITE_NAME]); ?></small>
    </strong>
    <?php
    $dept = Yii::app()->user->account;
    $folder = Accounts::model()->get($dept, 'AccountName');
    ?>
</div>

<div class="row">
    <div class="space-6"></div>

    <div class="col-sm-6 infobox-container">
        <div class="infobox infobox-green">
            <div class="infobox-icon">
                <i class="ace-icon fa fa-comments"></i>
            </div>

            <div class="infobox-data">
                <span class="infobox-data-number"><?= $this->smsSend("%%") ?></span>
                <div class="infobox-content">Total sms send</div>
            </div>


        </div>
        <div class="infobox infobox-red">
            <div class="infobox-icon">
                <i class="ace-icon fa fa-comments"></i>
            </div>

            <div class="infobox-data">
                <span class="infobox-data-number"><?= $this->senderAccount() ?></span>
                <div class="infobox-content"><?= Accounts::model()->get(Yii::app()->user->account, "AccountName") ?></div>
            </div>
        </div>
        <div class="col-md-12">

            <div class="panel-body" id="channels-container">

            </div>

        </div>

    </div>

    <div class="col-md-6">
        <div class="widget-box">
            <div class="widget-header widget-header-flat widget-header-small">
                <h5 class="widget-title">
                    <i class="ace-icon fa fa-credit-card"></i>
                    SMS Accounts Credit Reports <span id="timeslot"></span>
                </h5>
               
                <div class="widget-toolbar no-border">
                    <div class="inline dropdown-hover">
                        <button class="btn btn-minier btn-primary">
                            Select timespan
                            <i class="ace-icon fa fa-angle-down icon-on-right bigger-110"></i>
                        </button>

                        <ul id="timespan" class="dropdown-menu dropdown-menu-right dropdown-125 dropdown-lighter dropdown-close dropdown-caret">
                            <li class="active" id="this_month">
                                <a href="#" class="blue">
                                    <i class="ace-icon fa fa-caret-right bigger-110">&nbsp;</i>
                                    This Month
                                </a>
                            </li>

                            <li id="last_month">
                                <a href="#">
                                    <i class="ace-icon fa fa-caret-right bigger-110 invisible">&nbsp;</i>
                                    Last Month
                                </a>
                            </li>

                            <li id="this_week">
                                <a href="#">
                                    <i class="ace-icon fa fa-caret-right bigger-110 invisible">&nbsp;</i>
                                    This Week
                                </a>
                            </li>

                            <li id="last_week">
                                <a href="#">
                                    <i class="ace-icon fa fa-caret-right bigger-110 invisible">&nbsp;</i>
                                    Last Week
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>


            <div class="widget-box transparent">


                <div class="widget-body">
                    <div class="widget-main no-padding">
                        <table class="table table-bordered table-striped">
                            <thead class="thin-border-bottom">
                                <tr>
                                    <th>
                                        <i class="ace-icon fa fa-caret-right blue"></i>Account / Department
                                    </th>

                                    <th>
                                        <i class="ace-icon fa fa-caret-right blue"></i>Credit Purchased
                                    </th>

                                    <th class="hidden-480">
                                        <i class="ace-icon fa fa-caret-right blue"></i>Consumption
                                    </th>
                                    <th class="hidden-480">
                                        <i class="ace-icon fa fa-caret-right g"></i>Current Balance
                                    </th>
                                </tr>
                            </thead>


                            <tbody id="tabular">

                                <?php foreach ($depts As $dept) : ?>
                                    <tr>
                                        <td><?= $dept->AccountName ?></td>
                                        
                                        <td>
                                            <?php $amount = Accounts::model()->get($dept->id, "Credit") ?>
                                            <span class="label label-<?php
                                        $this->cssClass($amount);
                                            ?>arrowed-right arrowed-in">
                                                <b class="white"> 
                                            <?php echo number_format($amount, 0, '.', ',') ?>
                                                </b>
                                            </span>
                                        </td>

                                        <td class="hidden-480">
                                            <span class="label label-info arrowed-right arrowed-in">
    <?php
     $consumed = Smslogs::model()->getScaler("COUNT(SmsID)", "Sender_account = '$dept->id'");
     echo number_format($consumed, 0, '.', ',');
    $balance = $amount - $consumed;
    ?>
                                            </span>
                                        </td>

                                        <td class="hidden-480">
                                            <span class="label label-<?= $this->cssClass($balance) ?> arrowed-right arrowed-in">
    <?php echo number_format($amount, 0, '.', ',') ?>
                                            </span>
                                        </td>
                                    </tr>


<?php endforeach; ?>

                            </tbody>
                        </table>

                    </div><!-- /.widget-main -->
                </div><!-- /.widget-body -->
            </div><!-- /.widget-box -->
        </div>
    </div><!-- /.col -->
</div><!-- /.row -->

<div class="hr hr32 hr-dotted"></div>

<div class="row">
    <div class="col-sm-12">
        <div class="widget-box transparent">
            <div class="widget-header widget-header-flat">
                <h4 class="widget-title lighter">
                    <i class="ace-icon fa fa-star orange"></i>
                    Monthly Credit Usage Report
                </h4>

                <div class="widget-toolbar">
                    <a href="#" data-action="collapse">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div class="widget-body">
                <div class='row'>
                    <div class='col-md-12'>
                        <div class='col-md-12 border-right' id='monthly-sms-reports'>FusionCharts will load here.</div>

                    </div>
                </div>
                <hr  class="hr-14" />
                <div class='row' >
                    <div class='col-md-12'>
                        <div class='col-md-12 border-right' id='chart-container'>FusionCharts will load here.</div>
                    </div>
                </div>

            </div><!-- /.widget-body -->
        </div><!-- /.widget-box -->
    </div><!-- /.col -->


</div><!-- /.widget-main -->


<?php

function random_color_part() {
    return str_pad(dechex(mt_rand(0, 255)), 2, '0', STR_PAD_LEFT);
}

function random_color() {
    return random_color_part() . random_color_part() . random_color_part();
}

$colors = array("4e8310", "ffb0a1", "5c2c00", "7a5e85", "b3f0ff", "cf36ff", "5496ff", "5a9e68", "bf6900", "a17800", "72cc85", "becfbc", "005710" , "fac0e1" , "261e06")
?>
<script type="text/javascript">
    FusionCharts.ready(function () {

        var Data1 = new FusionCharts({
            "type": "stackedcolumn2d",
            "renderAt": "monthly-sms-reports",
            "width": "100%",
            "height": "400",
            "dataFormat": "json",
            "dataSource": {
                "chart": {
                    "caption": "<?php echo date('F , Y') ?> - Outgoing Sms Report",
                    "showvalues": "0",
                    "plotgradientcolor": "",
                    "formatnumberscale": "0",
                    "showplotborder": "0",
                    "palettecolors": "#EED17F,#97CBE7,#074868,#B0D67A,#2C560A,#DD9D82",
                    "canvaspadding": "0",
                    "bgcolor": "FFFFFF",
                    "showalternatehgridcolor": "0",
                    "divlinecolor": "CCCCCC",
                    "showcanvasborder": "0",
                    "legendborderalpha": "0",
                    "legendshadow": "0",
                    "interactivelegend": "0",
                    "showpercentvalues": "1",
                    "showsum": "1",
                    "canvasborderalpha": "0",
                    "showborder": "0"
                },
                "categories": [
                    {
                        "category": <?php echo $this->daysOfMonth() ?>
                    }
                ],
                "dataset": [
<?php $i = 0;
foreach (Accounts::model()->findAll() As $acc) : ?>
                        {
                            "seriesname": "<?php echo $acc->AccountName ?>",
                            "color": "<?php echo $colors[$i] ?>",
                            "data": <?php echo $this->getData($acc->id) ?>
                        },
    <?php $i++;
endforeach; ?>
                ],
                "styles": {
                    "definition": [
                        {
                            "name": "captionFont",
                            "type": "font",
                            "size": "15"
                        }
                    ],
                    "application": [
                        {
                            "toobject": "caption",
                            "styles": "captionfont"
                        }
                    ]
                }
            }
        }).render();

// Sms Channels Pie Chart

        var Data2 = new FusionCharts({
            "type": "doughnut2d",
            "renderAt": "channels-container",
            "width": "100%",
            "height": "400",
            "dataFormat": "json",
            "dataSource":
                    {
                        "chart": {
                            "caption": "SMS Channels consumption",
                            "numberPrefix": "",
                            "paletteColors": "#0075c2,#1aaf5d,#f2c500,#f45b00,#8e0000",
                            "bgColor": "#ffffff",
                            "showBorder": "0",
                            "use3DLighting": "0",
                            "showShadow": "0",
                            "enableSmartLabels": "0",
                            "showLabels": "1",
                            "showPercentValues": "0",
                            "showLegend": "1",
                            "legendShadow": "0",
                            "legendBorderAlpha": "0",
                            "defaultCenterLabel": "Total sms: <?php echo $this->smsSend("%%") ?>",
                            "centerLabel": "Sms send via $label: $value",
                            "centerLabelBold": "1",
                            "showTooltip": "0",
                            "decimals": "1",
                            "captionFontSize": "14",
                            "subcaptionFontSize": "14",
                            "subcaptionFontBold": "0"
                        },
                        "data": <?php echo $this->channelConsumption() ?>
                    }
        }).render();
    })


$("#timespan li").click(function() {
id =  $(this).attr('id'); 
   var values =  "id=" + id + '&cache=' + Math.random();
    //$('#tabular').show();
        $('#tabular').html("<img src= '<?php echo Yii::app()->request->baseUrl ?>/themes/default/images/ajax-loader.gif '/>");
        $.ajax
                ({
                    type: "POST",
                    url: "dataset",
                    data: values,
                    cache: false,
                    success: function(html)
                    {
                        $("#timeslot").html(myswitch(id))
                        $("#tabular").html(html);

                    }
                });

});

function myswitch(id)
{
    switch(id)
    {

        case 'this_week':
            title =  'for This Week';
            break;
            
        case 'last_month':
            title =   'for Last Month';
            break;
            
        case 'last_week':
            title =   'for Last Week';
            break;
        default:
            title =   'for This Month';
            break;
    }
    return title;
            
}


</script>

<?php
$theme = Yii::app()->theme->baseUrl;
Yii::app()->clientScript
        ->registerCssFile(Yii::app()->theme->baseUrl . '/dashboard/ace.min.css')
        ->registerScriptFile($theme . '/js/fs/fusioncharts.js')
        ->registerScriptFile($theme . '/js/fs/themes/fusioncharts.theme.ocean.js')
        ->registerScriptFile($theme . '/js/fs/themes/fusioncharts.theme.carbon.js')
        ->registerScriptFile($theme . '/js/fs/themes/fusioncharts.theme.fint.js')
        ->registerScriptFile($theme . '/js/fs/themes/fusioncharts.theme.zune.js');
//$js->registerCssFile($theme . '/css/style.css');
?>
