<?php

/**
 * Home controller
 * @author James Makau <jacjimus@gmail.com>
 */
class DefaultController extends AdminModuleController {

    public function init() {
        parent::init();

        $this->activeMenu = self::MENU_DASHBOARD;
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'data', 'dataset'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        $this->showPageTitle = false;
        $thisMonth = date('Y-m');
        $date = " DATE_FORMAT(`UpdateDate`,'%Y-%m') LIKE '$thisMonth' ";

        $depts = Accounts::model()->findAll();

        $this->render('dashboard', array('depts' => $depts, 'date' => $date));
    }

    public function smsSend($type) {
        return Smslogsarchive::model()->count("Status = '" . Smslogs::STATUS_DELIVERED . "' AND SmsType Like '$type'");
    }

    /*
     * Funtion to return all number of sms send from user department
     * return integer
     */

    public function senderAccount() {
        return Smslogsarchive::model()->count("Status = '" . Smslogs::STATUS_DELIVERED . "' AND Sender_account = " . Yii::app()->user->account);
    }

    /*
     * Funtion to return all number of sms send from user department in a month
     * return integer
     */

    public function getSmsVolumes() {
        return Smslogsarchive::model()->count("Status = '" . Smslogs::STATUS_DELIVERED . "' AND Sender_account = " . Accounts::model()->get(Yii::app()->user->account));
    }

    /*
     * Funtion to return all number of sms send from user department
     * return integer
     */

    public function userAccount() {
        return Smslogsarchive::model()->count("Status = '" . Smslogs::STATUS_DELIVERED . "' AND User_account = " . Accounts::model()->get(Yii::app()->user->id));
    }

    /*
     * Funtion to return all channel consumption
     * return json
     */

    public function channelConsumption() {
        $channels = array(Sms::QUICKSMS, Sms::CUSTOMESMS, Sms::FILE2SMS, Sms::APISMS);
        $data = array();
        foreach ($channels As $ch):
            $amt = $this->smsSend($ch);
            array_push($data, array('label' => $ch, "value" => $amt));
        endforeach;
        return CJSON::encode($data);
    }

    /*
     * Funtion to return all das of the current month
     * return json
     */

    public function daysOfMonth() {
        $days = Common::getDaysofMonth();
        $data = array();
        foreach ($days As $day):
            array_push($data, array('label' => date("dS", strtotime($day))));
        endforeach;
        return CJSON::encode($data);
    }

    /*
     * Funtion to return all das of the current month
     * return json
     */

    public function getData($acc) {
        $days = Common::getDaysofMonth();
        $return = array();
        foreach ($days As $day):
            $volume = Smslogsarchive::model()->count("Status = '" . Smslogs::STATUS_DELIVERED . "' AND DATE(Deliverytime) LIKE '$day'  AND Sender_account = '$acc'");
            $vol = (!$volume) ? 0 : $volume;
            array_push($return, array('value' => $vol));
        endforeach;


        return CJSON::encode($return);
    }

    public function cssClass($amount) {

        $min_threshhold = Yii::app()->settings->get(Constants::CATEGORY_GENERAL, Constants::KEY_MIN_SMS_THREASHHOLD);

        switch ($amount) {
            case $amount < $min_threshhold:
                echo "danger ";
                break;
            case $amount > $min_threshhold:
                echo "success ";
                break;
            default:
                echo "info ";
                break;
        }
    }

    public function actionDataset() {
        $id = Yii::app()->request->getParam('id');
        $depts = Accounts::model()->findAll();
        foreach ($depts As $dept) :

            $query = Yii::app()->db->createCommand()
                    ->select("SUM(Amount)")
                    ->from("tbl_acc_credits");
        
            $logs = Yii::app()->db->createCommand()
                    ->select("COUNT(SmsID)")
                    ->from("tbl_sms_logs");


            switch ($id) {
                case "last_month":
                    $query->where("Account_id = :id AND MONTH (`UpdateDate`) =  :premonth", array(":id" => $dept->id, ":premonth" => Common::getPreviousMonth()['month']));
                    $logs->where("SenderID = :id AND MONTH (`Datetime`) =  :premonth", array(":id" => $dept->id, ":premonth" => Common::getPreviousMonth()['month']));
                    break;

                case "this_week":
                    $query->where("Account_id = :id AND WEEK(`UpdateDate` , 3 ) = :curweek", array(":id" => $dept->id, ":curweek" => date("W")));
                    $logs->where("SenderID = :id AND WEEK(`Datetime` , 3 ) = :curweek", array(":id" => $dept->id, ":curweek" => date("W")));
                    break;

                case "last_week":
                    $query->where("Account_id = :id AND WEEK(`UpdateDate` , 3 ) = :thisweek", array(":id" => $dept->id, ":thisweek" => Common::getLastWeek()['week']));
                    $logs->where("SenderID = :id AND WEEK(`Datetime` , 3 ) = :thisweek", array(":id" => $dept->id, ":thisweek" => Common::getLastWeek()['week']));

                    break;

                default:
                    $query->where("Account_id = :id AND MONTH(`UpdateDate`) = :thismonth", array(":id" => $dept->id, ":thismonth" => date('m')));
                    $logs->where("SenderID = :id AND MONTH(`Datetime`) = :thismonth", array(":id" => $dept->id, ":thismonth" => date('m')));
                    break;
            }
            $amt = $query->queryScalar();
            $sms_logs = $logs->queryScalar();
            ?>
            <tr>
                <td><?= $dept->AccountName ?></td>

                <td>
                    <span class="label label-<?php
            $this->cssClass($query);
            ?>arrowed-right arrowed-in">
                        <b class="white"> 
            <?php echo number_format($amt, 0, '.', ',') ?>
                        </b>
                    </span>
                </td>

                <td class="hidden-480">
                    <span class="label label-info arrowed-right arrowed-in">
            <?php $amount = Accounts::model()->get($dept->id, "Credit") ?>
            <?php
            echo number_format($sms_logs, 0, '.', ',');
            $balance = $amount - $sms_logs;
            ?>
                    </span>
                </td>

                <td class="hidden-480">
                    <span class="label label-<?= $this->cssClass($balance) ?> arrowed-right arrowed-in">
                        <?php echo number_format($amount, 0, '.', ',') ?>
                    </span>
                </td>
            </tr>


                    <?php
                    endforeach;
                }

            }
            