<?php if ($this->showLink(UserResources::RES_TOOLS)): ?>
        <li  class="<?php echo $this->getModuleName() === 'tools' ? 'active open' : '' ?>">
                <a href="#" class="dropdown-toggle">
                        <i class="icon-gears"></i>
                        <span class="menu-text"> <?php echo Lang::t('System Tools Module') ?></span>
                         <b class="arrow icon-angle-down"></b>
                </a>
            <ul class="submenu">
                        <?php if ($this->showLink(UserResources::RES_TOOLS_GROUPS)): ?>
                             <li class="<?php echo $this->activeMenu === ToolsModuleController::MENU_GROUPS? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl(ModulesEnabled::MOD_TOOLS . '/default/create') ?>"><i class="icon-user-md"></i><?php echo Lang::t('Manage Sms Groups') ?></a></li>
                        <?php endif; ?>
                        <?php if ($this->showLink(UserResources::RES_TOOLS_ADDRESS_BOOK)): ?>
                             <li class="<?php echo $this->activeMenu === ToolsModuleController::MENU_ADDRESS? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl(ModulesEnabled::MOD_TOOLS . '/default/create') ?>"><i class="icon-user-md"></i><?php echo Lang::t('Manage Address Book') ?></a></li>
                        <?php endif; ?>
                        <?php if ($this->showLink(UserResources::RES_TOOLS_IMPORT_CONTACTS)): ?>
                             <li class="<?php echo $this->activeMenu === ToolsModuleController::MENU_IMPORT? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl(ModulesEnabled::MOD_TOOLS . '/default/create') ?>"><i class="icon-user-md"></i><?php echo Lang::t('Import Contacts') ?></a></li>
                        <?php endif; ?>
                        
                       
                </ul>
        </li>
<?php endif; ?>

