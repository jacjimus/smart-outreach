<?php

$form = $this->beginWidget('CActiveForm', array(
    'id' => 'patients-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
        'role' => 'form',
    )
        ));

?>
   
 

<div class="form-group">
                <?php echo $form->labelEx($model, 'Lname', array('class' => 'col-lg-4 control-label')); ?>
                <div class="col-lg-6">
            <?php echo $form->textField($model, 'Lname', array('class' => 'form-control', 'maxlength' => 25 , "readonly" => $model->isNewRecord? false:true)); ?>
                <?php echo CHtml::error($model, 'Lname') ?>
                </div>
        </div>
<div class="form-group">
                <?php echo $form->labelEx($model, 'Onames', array('class' => 'col-lg-4 control-label')); ?>
                <div class="col-lg-6">
            <?php echo $form->textField($model, 'Onames', array('class' => 'form-control', 'maxlength' => 40 , "readonly" => $model->isNewRecord? false:true)); ?>
                <?php echo CHtml::error($model, 'Onames') ?>
                </div>
        </div>
        <div class="form-group">
                <?php echo $form->labelEx($model, 'Idno', array('class' => 'col-lg-4 control-label')); ?>
                <div class="col-lg-6">
                        <?php echo $form->textField($model, 'Idno', array('class' => 'form-control', 'maxlength' => 15, "readonly" => $model->isNewRecord? false:true)); ?>
                        <?php echo CHtml::error($model, 'Idno') ?>
                </div>
        </div>
        <div class="form-group">
                <?php echo $form->labelEx($model, 'Dob', array('class' => 'col-lg-4 control-label')); ?>
                <div class="col-lg-6">
                        <?php echo $form->textField($model, 'Dob', array('class' => 'form-control', "id" => "datepicker", 'maxlength' => 15 , "readonly" => $model->isNewRecord? false:true)); ?>
                        <?php echo CHtml::error($model, 'Dob') ?>
                </div>
        </div>

    
    <div class="form-group">
                <?php echo $form->labelEx($model, 'Mstatus', array('class' => "col-lg-4 control-label", 'label' => Lang::t('Marital Status'))); ?>
                <div class="col-lg-6">
                        <?php echo CHtml::activeDropDownList($model, 'Mstatus', array(""=>"[Select one]", "Married"=>"Married" , "Single"=>"Single", "Divorced"=>"Divorced" , "Widow"=>"Widow"), array('class' => 'form-control')); ?>
                         <?php echo CHtml::error($model, 'Mstatus') ?>
                </div>
        </div>
        <div class="form-group">
        <?php echo $form->labelEx($model, 'Address', array('class' => 'col-lg-4 control-label')); ?>
        <div class="col-lg-6">
            <?php echo CHtml::error($model, 'Address') ?>
                <?php echo $form->textArea($model, 'Address', array('class' => 'form-control', 'rows' => 2 , 'cols' => 10)); ?>
                 </div>
        </div>
        <div class="form-group">
                <?php echo $form->labelEx($model, 'Mobileno', array('class' => 'col-lg-4 control-label')); ?>
                <div class="col-lg-6">
                        <?php echo $form->textField($model, 'Mobileno', array('class' => 'form-control', 'maxlength' => 15, "readonly" => $model->isNewRecord? false:true)); ?>
                        <?php echo CHtml::error($model, 'Mobileno') ?>
                </div>
        </div>
        <div class="form-group">
                <?php echo $form->labelEx($model, 'Nextofkin', array('class' => 'col-lg-4 control-label')); ?>
                <div class="col-lg-6">
                        <?php echo $form->textField($model, 'Nextofkin', array('class' => 'form-control', 'maxlength' => 40)); ?>
                        <?php echo CHtml::error($model, 'Nextofkin') ?>
                </div>
        </div>
        <div class="form-group">
                <?php echo $form->labelEx($model, 'Relationship', array('class' => 'col-lg-4 control-label')); ?>
                <div class="col-lg-6">
                        <?php echo $form->textField($model, 'Relationship', array('class' => 'form-control', 'maxlength' => 40)); ?>
                        <?php echo CHtml::error($model, 'Relationship') ?>
                </div>
        </div>
        <div class="form-group">
                <?php echo $form->labelEx($model, 'Kinphone', array('class' => 'col-lg-4 control-label')); ?>
                <div class="col-lg-6">
                        <?php echo $form->textField($model, 'Kinphone', array('class' => 'form-control', 'maxlength' => 15)); ?>
                        <?php echo CHtml::error($model, 'Kinphone') ?>
                </div>
        </div>
        


<div class="clearfix form-actions">
        <div class="col-lg-offset-2 col-lg-9">
                <button class="btn btn-primary" type="submit"><i class="icon-ok bigger-110"></i> <?php echo Lang::t('Create Mother') ?></button>
                &nbsp; &nbsp; &nbsp;
                <a class="btn" href="#" onclick="js:$('#dialogAccount').dialog('close')"><i class="icon-remove bigger-110"></i><?php echo Lang::t('Cancel') ?></a>
        </div>
</div>

<?php $this->endWidget(); 
?>

<script>
$(function() {
    $( ".datepick" ).datepicker({
    dateFormat: 'yy-mm-dd',
    onSelect: function(e) {
        e.preventDefault;
       valu = "val=" +  $("#LMPDate").val();
      
    $.ajax({
type: "POST",
url:"<?php echo Yii::app()->controller->createUrl('default/edd')?>",
data: valu,
success: function(data){
$("#EDDdate").val(data);
}

});
  }

})
  });
</script>