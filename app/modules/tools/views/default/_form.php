<?php

$form = $this->beginWidget('CActiveForm', array(
    'id' => 'child-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
        'role' => 'form',
    )
        ));
 
?>
   
<div class="form-group">
                <?php echo $form->labelEx($model, 'Motherid', array('class' => 'col-lg-2 control-label')); ?>
                <div class="col-lg-5">
             <?php
             $data = Patient::model()->findAll();
$return = array();

foreach ($data as $d)
    $return[$d->Motherid] = $d->Lname . ' '. $d->Onames. '-'. $d->Mobileno;     

        echo $form->dropDownList($model, 'Motherid', $return ,array('prompt' => '[Select one]' , 'class' => 'col-lg-6' ,
            "disabled" => $model->isNewRecord? false:true, "id"=>"Mother_id")); ?>
         <?php if ($this->showLink($this->resource, Acl::ACTION_CREATE)): ?>
                    <a class="" href="#" style = 'cursor: pointer;' onclick = "{addAcc(); $('#dialogAccount').dialog('open');}">
                        &nbsp;&nbsp;&nbsp;&nbsp;<i class="icon-plus-sign"></i>  <?php echo "&nbsp;". Lang::t('Register new Mother') ?></a><?php endif; ?>                
            <?php echo CHtml::error($model, 'Motherid') ?>
                </div>
        </div>  
<div class="form-group">
                <?php echo $form->labelEx($model, 'Birthnote', array('class' => 'col-lg-2 control-label')); ?>
                <div class="col-lg-5">
            <?php echo $form->textField($model, 'Birthnote', array('class' => 'form-control', 'maxlength' => 25 , "readonly" => $model->isNewRecord? false:true)); ?>
                <?php echo CHtml::error($model, 'Birthnote') ?>
                </div>
        </div>
<div class="form-group">
                <?php echo $form->labelEx($model, 'Lname', array('class' => 'col-lg-2 control-label')); ?>
                <div class="col-lg-5">
            <?php echo $form->textField($model, 'Lname', array('class' => 'form-control', 'maxlength' => 25 , "readonly" => $model->isNewRecord? false:true)); ?>
                <?php echo CHtml::error($model, 'Lname') ?>
                </div>
        </div>
<div class="form-group">
                <?php echo $form->labelEx($model, 'Onames', array('class' => 'col-lg-2 control-label')); ?>
                <div class="col-lg-5">
            <?php echo $form->textField($model, 'Onames', array('class' => 'form-control', "readonly" => $model->isNewRecord? false:true)); ?>
                      
            <?php echo CHtml::error($model, 'Onames') ?>
                </div>
        </div>
        
        <div class="form-group">
        <?php echo $form->labelEx($model, 'Gender', array('class' => 'col-lg-2 control-label')); ?>
        <div class="col-lg-5">
            <?php echo CHtml::activeDropDownList($model, 'Gender', array(""=>"[Select one]", "Male"=>"Male" , "Female"=>"Female"), array('class' => 'form-control' , "disabled" => $model->isNewRecord? false:true)); ?>
             <?php echo CHtml::error($model, 'Gender') ?>
                        
        </div>
        </div>
        
<div class="form-group">
        <?php echo $form->labelEx($model, 'Birthorder', array('class' => 'col-lg-2 control-label')); ?>
        <div class="col-lg-5">
           <?php echo $form->textField($model, 'Birthorder', array('class' => 'form-control')); ?>
             <?php echo CHtml::error($model, 'Birthorder') ?>
                    
        </div>
        </div>
<div class="form-group">
        <?php echo $form->labelEx($model, 'Dob', array('class' => 'col-lg-2 control-label')); ?>
        <div class="col-lg-5">
           <?php echo $form->textField($model, 'Dob', array('class' => 'form-control datepicker' , "disabled" => $model->isNewRecord? false:true)); ?>
             <?php echo CHtml::error($model, 'Dob') ?>
                    
        </div>
        </div>
<div class="form-group">
        <?php echo $form->labelEx($model, 'Dateseen', array('class' => 'col-lg-2 control-label')); ?>
        <div class="col-lg-5">
           <?php echo $form->textField($model, 'Dateseen', array('class' => 'form-control datepicker')); ?>
             <?php echo CHtml::error($model, 'Dateseen') ?>
                    
        </div>
        </div>
<div class="form-group">
        <?php echo $form->labelEx($model, 'Birthweight', array('class' => 'col-lg-2 control-label')); ?>
        <div class="col-lg-5">
           <?php echo $form->textField($model, 'Birthweight', array('class' => 'form-control')); ?>
             <?php echo CHtml::error($model, 'Birthweight') ?>
                    
        </div>
        </div>

<div class="form-group">
        <?php echo $form->labelEx($model, 'Birthheight', array('class' => 'col-lg-2 control-label')); ?>
        <div class="col-lg-5">
           <?php echo $form->textField($model, 'Birthheight', array('class' => 'form-control')); ?>
             <?php echo CHtml::error($model, 'Birthheight') ?>
                    
        </div>
        </div>
        
        


<div class="clearfix form-actions">
        <div class="col-lg-offset-2 col-lg-9">
                <button class="btn btn-primary" type="submit"><i class="icon-ok bigger-110"></i> <?php echo Lang::t($model->isNewRecord ? 'Create New Born' : 'Update New Born Details') ?></button>
                &nbsp; &nbsp; &nbsp;
                <a class="btn" href="<?php echo $this->createUrl('manage') ?>"><i class="icon-remove bigger-110"></i><?php echo Lang::t('Cancel') ?></a>
        </div>
</div>
<?php $this->endWidget(); ?>


<?php
// Diaogue for adding new custom report
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogAccount',
    'options' => array(
        'title' => 'Mother Registration Form',
        'autoOpen' => false,
        'modal' => false,
        'width' => 500,
        'height' => 600,
    ),
));
?>
<div class="divAcc"></div>

<?php $this->endWidget(); ?>
                       

<script type="text/javascript">
   

    function addAcc()
    {
<?php
echo CHtml::ajax(array(
    'url' => array("default/add"),
    'data' => "js:$(this).serialize()",
    'type' => 'post',
    'dataType' => 'json',
    'success' => "function(data)
            {
                if (data.status == 'failure')
                {
                    $('#dialogAccount div.divAcc').html(data.div);
                          // Here is the trick: on submit-> once again this function!
                    $('#dialogAccount div.divAcc form').submit(addAcc);
                }
                else
                {
                    $('#dialogAccount div.divAcc').html(data.div);
                    setTimeout(\"$('#dialogAccount').dialog('close') \",400);
                    $('#Mother_id').append(data.options);
                }
 
            } ",
))
?>;
        return false;

    }
    </script>