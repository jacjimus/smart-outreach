<?php
$this->breadcrumbs = array('Preventive care ' => 'manage',
    $this->pageTitle,
);
?>
<div class="widget-box transparent">
        <div class="widget-header">
        <?php echo $this->renderPartial('child.views.default._tab' , array("id" => $id)) ?>
    </div>  
        <div class="widget-body widget-body-style2">
                <div class="widget-main padding-12 no-padding-left no-padding-right">
                        <div class="tab-content padding-4">
                                <?php $this->renderPartial('_view', array('model' => $model)); ?>
                        </div>
                </div>
        </div>
