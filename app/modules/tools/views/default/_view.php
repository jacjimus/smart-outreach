<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <i class="fa fa-anchor"></i> <?php echo Lang::t('New Born Details') ?>
<?php if (Controller::showlink($this->resource , Acl::ACTION_UPDATE)): ?>
                                <span><a class="pull-right white" href="<?php echo $this->createUrl('update', array('id' => $model->Childid)) ?>"><i class="fa fa-edit"></i> <?php echo Lang::t('Edit New Born Details') ?></a></span>
                        <?php endif; ?>
        </h4>
    </div>
    <div id="exam_info" class="panel-collapse collapse in">
        <div class="panel-body">
            <div class="detail-view">
                <?php
                $this->widget('application.components.widgets.DetailView', array(
                    'data' => $model,
                    'attributes' => array(
                        array(
                            'name' => 'Lname',
                        ),
                        array(
                            'name' => 'Onames',
                        ),
                        array(
                            'name' => 'Gender',
                        ),
                        array(
                            'name' => 'Birthorder',
                        ),
                         array(
                            'name' => 'Dob',
                            'type' => 'raw',
                            'value' =>  MyYiiUtils::formatDate($model->Dob),
                        ),
                        array(
                            'name' => 'Age',
                        ),
                        
                        array(
                            'name' => 'Dateseen',
                        ),
                        
                        array(
                            'name' => 'Birthweight',
                        ),
                        
                        array(
                            'name' => 'Birthheight',
                        ),
                        array(
                            'name' => 'Motherid',
                            'type' => 'raw',
                            'value' => Patient::model()->get($model->Motherid , "CONCAT(Lname,' ',Onames )")
                        ),
                        
                )
                    )
                        );
                ?>
            </div>
        </div>
        </div>
     
</div>

