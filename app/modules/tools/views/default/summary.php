<?php
$this->breadcrumbs = array('Preventive Care ' => 'manage',
    $this->pageTitle,
);
?>
<div class="widget-box transparent">
  <div class="widget-header">
        <?php echo $this->renderPartial('child.views.default._tab' , array("id" => $id)) ?>
    </div>      
        <div class="widget-body widget-body-style2">
                <div class="widget-main padding-12 no-padding-left no-padding-right">
                        <div class="tab-content padding-4">
<table class="table table-striped table-bordered table-hover">
    <?php if(!empty($model)):?>
                                                <thead>
                                                    <tr>
                                                        <th>Antenatal Care</th>
                                                        <th>Date to Visit</th>
                                                        <th>Date Administered</th>
                                                        <th>SMS Date</th>
                                                        <th>SMS Message</th>
                                                        <th>SMS Delivery Status</th>
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    foreach ($model As $mod):
                                                    ?>
                                                    <tr>

                                                        <td><?php echo Preventive::model()->get($mod['Careid'] , "Prevname") ?></td>
                                                        <td ><?php echo Antenatal::model()->getScaler('Prevdate', "Previd = '$mod[Careid]' AND Motherid = '$id'") ?></td>
                                                        <td ><?php echo Antenatal::model()->getScaler('Dategiven', "Previd = '$mod[Careid]' AND Motherid = '$id'") ?></td>
                                                         <td> <?php echo $mod['Smsdate'] ?></td>
                                                         <td> <?php echo MyYiiUtils::myShortenedString($mod['Smsmessage'] , 200 , "..." , 70 ) ?></td>
                                                         <td> <?php echo $mod['Deliverystatus'] ?></td>
                                                            
                                                        </tr>
                                                        <?php
                                                    endforeach;
            
                                                      ?>
                                                        
                                                </tbody>
        <?php
        else: 
            echo "<div class=\"alert alert-block alert-warning\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><i class=\"icon-remove\"></i></button>
                        <div>No sms Logs found!</div>
                </div>";
        endif; ?>
                                            </table>
                            </div>
                </div>
        </div>
 </div>
