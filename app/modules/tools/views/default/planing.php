<?php
$this->breadcrumbs = array('Children Module' => 'manage',
    $this->pageTitle,
);
?>
<div class="widget-box transparent">
  <div class="widget-header">
        <?php echo $this->renderPartial('child.views.default._tab' , array("id" => $id)) ?>
    </div>      
        <div class="widget-body widget-body-style2">
                <div class="widget-main padding-12 no-padding-left no-padding-right">
                        <div class="tab-content padding-4">
<table class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Immunization</th>

                                                        <th>Date to be Given </th>
                                                        <th >Actual date Given</th>
                                                        <th>Facility</th>
                                                        <th>Status</th>
                                                        <th>SMS Notification</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    foreach ($model As $mod):
                                                    ?>
                                                    <tr>

                                                        <td><?php echo Immunization::model()->get($mod['Immunid'] , "Immunname") ?></td>
                                                            <td><?php echo $mod['Immundate'] ?> </td>
                                                            <td><?php echo $mod['Dategiven'] != "0000-00-00" ? $mod['Dategiven'] : "Not Taken"?> </td>
                                                            <td><?php echo $mod['Facility'] ?> </td>
                                                            <td><?php echo $mod['Status'] ?> </td>
                                                            <td><?php echo $mod['Smsnotifystatus'] ?> </td>
                                                            
                                                        </tr>
                                                      <?php
                                                    endforeach;
                                                      ?>  
                                                </tbody>
                                            </table>
                            </div>
                </div>
        </div>
 </div>
