
<div class="widget-toolbar no-border" style="float:left !important;">
    <ul class="nav nav-tabs my-nav">
        <li class="<?php echo $this->activeTab === 0 ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('child/default/view?id='.$id) ?>"><?php echo Lang::t('Child Details') ?></a></li>
        <li class="<?php echo $this->activeTab === 1 ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('child/default/planing?id='.$id) ?>"><?php echo Lang::t('Postnatal Care Planing') ?></a></li>
        <li class="<?php echo $this->activeTab === 2 ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('child/default/summary?id='.$id) ?>"><?php echo Lang::t('Postnatal Care SMS Summary Report') ?></a></li>
    </ul>
</div>
