<?php

/**
 * This is the model class for table "tbl_children".
 * 
 * The followings are the available columns in table 'tbl_children':
 * @property integer $Childid
 * @property string $Birthnote
 * @property string $Lname
 * @property string $Onames
 * @property string $Gender
 * @property string $Birthorder
 * @property string $Dob
 * @property string $Dateseen
 * @property string $Comments
 * @property string $Birthweight
 * @property string $Birthheight
 * @property string $Motherid
 * @property string $Age

 */
class Child extends ActiveRecord implements IMyActiveSearch {
        //email template keys

        // SMS Notification Status
       const STATUS_SMS_NOTIFY_NO = "Pending";
       const STATUS_SMS_NOTIFY_YES = "Delivered";
       // Antenatal Care Status
       const STATUS_ANTENATAL_PENDING = "Pending";
       const STATUS_ANTENATAL_GIVEN = "Given";
       const SEARCH_FIELD = '_search';
      
       public $Age;
       public $Dob_start;
       public $Dob_end;
       public $Dob_weekly;
       public $Dob_monthly;
       public $Gender_weekly;
       public $Gender_monthly;
       public $Year;
       public $Month;

        /**
         * Returns the static model of the specified AR class.
         * @param string $className active record class name.
         * @return SettingsEmailTemplate the static model class
         */
        public static function model($className = __CLASS__)
        {
                return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName()
        {
                return 'tbl_children';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules()
        {

                return array(
                     array('Lname, Onames, Birthnote, Gender, Dob,Dateseen,Birthweight,Birthheight', 'required'), 
                     array('Birthnote', 'unique'), 
                     array('Lname', 'length', 'max' => 25),
                     array('Onames', 'length', 'max' => 50),
                     array('Motherid', 'numerical', 'integerOnly' => true),
                     array('Message,Comments, Createdate, IntAfter, Birthorder', 'safe'),
                     array('Childid,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
                    );
        }
        /*
         * After saving new born, Generate all the immunizations to be administered depending on all the immunizations available.
         */
        public function afterSave()
        {
            if($this->isNewRecord):
                $immunizations = Immunization::model()->findAll();
                $weeks = 0;
                foreach ($immunizations As $immune):
                    $model = new Postnatal();
                    $model->Immunid = $immune->Immunid;
                    $model->Childid = $this->Childid;
                    $model->Status = Postnatal::STATUS_POSTNATAL_PENDING;
                    $model->Smsnotifystatus = Postnatal::STATUS_SMS_NOTIFY_NO;
                    
                    if($immune->IntAfter == "Birth"):
                        $weeks = $immune->Interval;
                        $model->Immundate = $this->getDate($weeks , $this->Dob);
                       
                    else:
                        $weeks = $this->recurseWeeks($immune->Immunid , $weeks);
                         $model->Immundate = $this->getDate($weeks , $this->Dob);
                        
                        
                    endif;
                        $model->save(FALSE);
                            
                endforeach;
            endif;
            return parent::afterSave(); 
        }
        
        /**
         * function to add weeks to a date and return the resulting date
         * @return date
         */
        private function getDate($weeks , $date)
        {
            $date_n = date("d-m-Y" , strtotime($date));
            $aDate = strtotime("$date_n + $weeks week");
            return date('Y-m-d', $aDate);
        }
       
        private function recurseWeeks($immunid , $weeks)
        {
            
            if(Immunization::model()->get($immunid , "IntAfter") != "L.M.P"):
                        
                        $weeks += Immunization::model()->get($immunid, "Interval");
                        $immunname = Immunization::model()->get($immunid, "IntAfter");
                        $new_id = Immunization::model()->findByAttributes(array("Immunname" => $immunname));
                       $immun =  $new_id['Immunid'];
                       $this->recurseWeeks($immun , $weeks);
                        
                       
            endif;
                        return $weeks;
        }
        
        public function afterFind()
        {
            $this->Age = Patient::dateDiff($this->Dob, date("y-m-d"));
                return parent::afterFind(); 
        }
        
        
        /**
         * @return array relational rules.
         */
        public function relations()
        {
                return array(
                );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels()
        {
            
           return array(
                    'Birthnote' => Lang::t('Birth notification No'),
                    'Lname' => Lang::t('Last Name'),
                    'Onames' => Lang::t('Other Names'),
                    'Gender' => Lang::t('Gender'),
                    'Birthorder' => Lang::t('Birth Order'),
                    'Dob' => Lang::t('Date of Birth'),
                    'Dateseen' => Lang::t('Date Seen'),
                    'Birthweight' => Lang::t('Birth Weight(Kgs)'),
                    'Birthheight' => Lang::t('Birth Height(cm)'),
                    'Motherid' => Lang::t('Child Mother'),
                    
                );
        }

        public function searchParams()
        {
                return array(
                    array('Birthnote', self::SEARCH_FIELD, true, 'OR'),
                    array('Lname', self::SEARCH_FIELD, true, 'OR'),
                    array('Onames', self::SEARCH_FIELD, true, 'OR'),
                    array('Gender', self::SEARCH_FIELD, true, 'OR'),
                    array('Dob', self::SEARCH_FIELD, true, 'OR'),
                    array('Birthorder', self::SEARCH_FIELD, true, 'OR'),
                    array('Dateseen', self::SEARCH_FIELD, true, 'OR'),
                    
                    
                );
        }

        
        
//        public function getName($id)
//        {
//                if (empty($id))
//                        return NULL;
//                $name = SmsGroup::model()->getScaler('group_name', '`id`=:t1 ', array(':t1' => $id));
//                return !empty($name) ? $name : NULL;
//        }

}

