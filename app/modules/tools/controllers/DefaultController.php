<?php

class DefaultController extends ChildModuleController {

        public function init()
        {
                $this->resource = UserResources::RES_CHILD;
                $this->activeMenu = self::MENU_CREATE;
                $this->resourceLabel = 'New Born';
                $this->showPageTitle = false;
                parent::init();
        }

        /**
         * @return array action filters
         */
        public function filters()
        {
                return array(
                    'accessControl', // perform access control for CRUD operations
                    'postOnly + delete',
                );
        }

        /**
         * Specifies the access control rules.
         * This method is used by the 'accessControl' filter.
         * @return array access control rules
         */
        public function accessRules()
        {
                return array(
                    array('allow',
                        'actions' => array('index', 'view', 'create', 'add' , 'manage', 'update',
                            'planing', 'summary', 'reports' ),
                        'users' => array('@'),
                    ),
                    array('deny', // deny all users
                        'users' => array('*'),
                    ),
                );
        }

         public function actionCreate()
        {
                $this->hasPrivilege(Acl::ACTION_CREATE);

                $this->pageTitle = Lang::t('Add ' . $this->resourceLabel);

                $model = new Child();
                $model_class_name = $model->getClassName();

                if (isset($_POST[$model_class_name])) {
                        $model->attributes = $_POST[$model_class_name];
                         if ($model->save()) {
                             $mother = Patient::model()->loadModel($model->Motherid);
                             // Enroll the mother to postnatal care
                             $mother->Postnatal = 1;
                             $mother->save(FALSE);
                                Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                                $this->redirect(array('view' , "id" => $model->Childid));
                        }
                }

                $this->render('create', array(
                    'model' => $model,
                ));
        }
        
        public function actionAdd() {
        $model = new Patient();
        $model_class_name = get_class($model);
        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $model->Postnatal = 1;
            if ($model->save()) {
                if (Yii::app()->request->isAjaxRequest) {
                    echo CJSON::encode(array(
                        'status' => 'success',
                        'div' => "Mother added",
                        'options' => "<option value=' $model->Motherid ' selected='selected'>". $model->Lname. " ". $model->Onames . "-" . "$model->Mobileno</option>"
                    ));
                    exit;
                } else
                    $this->actionManage();
            }
            else {

                //echo 'could not insert';
            }
        }

        if (Yii::app()->request->isAjaxRequest) {
            echo CJSON::encode(array(
                'status' => 'failure',
                'div' => $this->renderPartial('add', array('model' => $model), true)));
            exit;
        } else
            $this->render('add', array('model' => $model,));
    }
        
         public function actionView($id) {
        
        $this->hasPrivilege(Acl::ACTION_VIEW);
        $this->activeTab = 0;
        $this->showPageTitle = false;
        $this->pageTitle = Lang::t('View New Born details');

        $this->render('view', array(
            'id' => $id,
            'model' => Child::model()->model()->loadModel($id),
        ));
        }
        
         public function actionUpdate($id)
        {
                $this->hasPrivilege(Acl::ACTION_UPDATE);

                $this->pageTitle = Lang::t('Update ' . $this->resourceLabel . ' Details');


                $model = Child::model()->loadModel($id);
                $model_class_name = $model->getClassName();

                if (isset($_POST[$model_class_name])) {
                        $model->attributes = $_POST[$model_class_name];
                        //var_dump( $model->attributes);die;
                       
                        if ($model->save()) {
                                Yii::app()->user->setFlash('success', $this->resourceLabel. " details ". Lang::t('SUCCESS_MESSAGE'));
                                $this->redirect(array('view' , "id" => $id));
                        }
                }

                $this->render('update', array(
                    'model' => $model,
                ));
        }
        
        public function actionPlaning($id) {
        
        $this->hasPrivilege(Acl::ACTION_VIEW);
        $this->activeTab = 1;
        $this->showPageTitle = false;
        $this->pageTitle = Lang::t('View Post care Planing Details');

        $this->render('planing', array(
            "id"=>$id,
            'model' => Postnatal::model()->findAll("Childid = '$id' order by Immundate ASC")
                ,
        ));
    }
        public function actionSummary($id) {
        
        $this->hasPrivilege(Acl::ACTION_VIEW);
        $this->activeTab = 2;
        $this->showPageTitle = false;
        $this->pageTitle = Lang::t('View Postnatal Care SMS Summary Details');

        $this->render('summary', array(
            "id"=>$id,
           'model' => Smslogs::model()->findAll("Motherid = '$id' AND Caretype LIKE '".Smslogs::STATUS_SMS_TYPE_POSTNATAL."' order by Smsdate Desc")
        ));
    }
        
        
    
    public function actionManage()
        {
                $this->hasPrivilege(Acl::ACTION_VIEW);
                $this->activeMenu = self::MENU_MANAGE;
                $this->pageTitle = Lang::t("New Born Details");
                

                $searchModel = Child::model()->searchModel(array(), $this->settings[Constants::KEY_PAGINATION], 'Dob DESC');
                $this->render('manage', array(
                    'model' => $searchModel,
                ));
        }
        
        
}
