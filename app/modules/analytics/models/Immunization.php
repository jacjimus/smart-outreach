<?php

/**
 * This is the model class for table "settings_email_template".
 *
 * The followings are the available columns in table 'settings_email_template':
 * @property integer $Immunid
 * @property string $Immunname
 * @property string $Interval
 * @property string $Message
 * @property string $Createdate
 * @property string $Comments
 * @property string $IntDuration
 * @property string $IntAfter
 
 */
class Immunization extends ActiveRecord implements IMyActiveSearch {
        //email template keys

       
        const SEARCH_FIELD = '_search';
       
       public $Createdate;

        /**
         * Returns the static model of the specified AR class.
         * @param string $className active record class name.
         * @return SettingsEmailTemplate the static model class
         */
        public static function model($className = __CLASS__)
        {
                return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName()
        {
                return 'tbl_immunizations';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules()
        {

                return array(
                     array('Immunname, Interval, IntDuration,IntAfter', 'required'), 
                     array('Immunname', 'unique'), 
                     array('Message, Immunname, Comments', 'length', 'max' => 100),
                     array('Interval', 'numerical', 'integerOnly' => true),
                     array('Message,Comments, Createdate, IntAfter,IntDuration', 'safe'),
                     array('Immunid,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
                    );
        }
        
        public function beforeSave()
        {
            $this->Createdate = date("Y-m-d");
                return parent::beforeSave(); 
        }
        
        
        /**
         * @return array relational rules.
         */
        public function relations()
        {
                return array(
                );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels()
        {
            
          
                return array(
                    'Immunid' => Lang::t('ID'),
                    'Immunname' => Lang::t('Immunization'),
                    'Interval' => Lang::t('Time Interval'),
                    'IntDuration' => Lang::t('Interval Duration'),
                    'IntAfter' => Lang::t('Interval From'),
                    'Message' => Lang::t('Notes'),
                    'Comments' => Lang::t('Comments'),
                    'Createdate' => Lang::t('Create date'),
                    
                );
        }

        public function searchParams()
        {
                return array(
                    array('Immunname', self::SEARCH_FIELD, true, 'OR'),
                    array('Interval', self::SEARCH_FIELD, true, 'OR'),
                    array('IntDuration', self::SEARCH_FIELD, true, 'OR'),
                    array('IntAfter', self::SEARCH_FIELD, true, 'OR'),
                    
                );
        }

        
}

