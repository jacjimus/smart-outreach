<div class="nav-search">
            <div id="the_loader" class="hide">
                <?php
                echo CHtml::imageButton(Yii::app()->request->baseUrl."/themes/default/images/indicator.gif", array("style"=>"width: 30px; height: 30px;"))
                ?>
            </div>
        </div>
<?php

$grid_id = 'preventive-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Lang::t('Manage Preventive care'),
    'titleIcon' => null,
    'showExportButton' => false,
    'showSearch' => true,
    'createButton' => array('visible'=> $this->showlink( $this->resource , Acl::ACTION_CREATE )?true:false),
    'toolbarButtons' => array(),
    'showRefreshButton' => false,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        
        'columns' => array(
        array(
            'header' => "Immunname",
            'name' => 'row',
            'value' => '$data->Immunname',
        ),
         
        array(
            'name' => 'Interval',
            'type' => 'raw',
            'value' => '$data->Interval',
        ),
        
        array(
            'name' => 'IntDuration',
            'type' => 'raw',
            'value' => '$data->IntDuration',
        ),
        array(
            'name' => 'IntAfter',
            'type' => 'raw',
            'value' => '$data->IntAfter',
        ),
        array(
            'name' => 'Message',
            'type' => 'raw',
            'value' => '$data->Message',
        ),
        
        
        array(
            'class' => 'ButtonColumn',
            'template' => '{update}&nbsp;&nbsp;&nbsp;&nbsp;{view}',
            'htmlOptions' => array('class' => 'text-center', 'style' => 'width: 100px;'),
            'buttons' => array(
                'update' => array(
                    'imageUrl' => false,
                    'label' => '<i class="icon-pencil bigger-130"></i>',
                    'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->primaryKey))',
                    'visible' => '$this->grid->owner->showLink("' . UserResources::RES_MANAGE_IMMUNIZATION . '", "' . Acl::ACTION_UPDATE . '")?true:false',
                    'options' => array(
                        'class' => 'green',
                        'title' => 'Edit',
                    ),
                ),
                'view' => array(
                    'imageUrl' => false,
                    'label' => '<i class="icon-list-alt bigger-130"></i>',
                    'url' => 'Yii::app()->controller->createUrl("view",array("id"=>$data->primaryKey))',
                    'visible' => '$this->grid->owner->showLink("' . UserResources::RES_MANAGE_IMMUNIZATION . '", "' . Acl::ACTION_VIEW . '")?true:false',
                    'options' => array(
                        'class' => 'blue',
                        'title' => 'View details',
                    ),
                ),
            )
        ),
    ),
)
)
)
;
?>