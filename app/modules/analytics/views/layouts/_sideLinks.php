<?php if ($this->showLink(UserResources::RES_ANALYTICS)): ?>
        <li  class="<?php echo $this->getModuleName() === 'analytics' ? 'active open' : '' ?>">
                <a href="#" class="dropdown-toggle">
                        <i class="icon-dashboard"></i>
                        <span class="menu-text"> <?php echo Lang::t('Analytics Module') ?></span>
                         <b class="arrow icon-angle-down"></b>
                </a>
            <ul class="submenu">
                        <?php if ($this->showLink(UserResources::RES_INBOUND_ANALYTICS)): ?>
                <li class="<?php echo $this->activeMenu === AnalyticsModuleController::MENU_CREATE ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl(ModulesEnabled::MOD_ANALYTICS . '/default/create') ?>"><i class="icon-double-angle-right"></i><?php echo Lang::t('Inbound Reports') ?></a></li>
                        <?php endif; ?>
                        
                                
                        <?php if ($this->showLink(UserResources::RES_OUTBOUND_ANALYTICS)): ?>
                                <li class="<?php echo $this->activeMenu === AnalyticsModuleController::MENU_MANAGE ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl(ModulesEnabled::MOD_ANALYTICS . '/default/manage') ?>"><i class="icon-briefcase"></i><?php echo Lang::t('Outbound Reports') ?></a></li>
                        <?php endif; ?>
                        
                       
                        
                </ul>
        </li>
<?php endif; ?>

