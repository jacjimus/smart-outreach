<?php

class DefaultController extends ImmunizationModuleController {

        public function init()
        {
                $this->resource = UserResources::RES_IMMUNIZATION;
                $this->activeMenu = self::MENU_CREATE;
                $this->resourceLabel = 'Immunization';
                $this->showPageTitle = false;
                parent::init();
        }

        /**
         * @return array action filters
         */
        public function filters()
        {
                return array(
                    'accessControl', // perform access control for CRUD operations
                    'postOnly + delete',
                );
        }

        /**
         * Specifies the access control rules.
         * This method is used by the 'accessControl' filter.
         * @return array access control rules
         */
        public function accessRules()
        {
                return array(
                    array('allow',
                        'actions' => array('index', 'view', 'create', 'manage', 'update', 'reports' ),
                        'users' => array('@'),
                    ),
                    array('deny', // deny all users
                        'users' => array('*'),
                    ),
                );
        }

         public function actionCreate()
        {
                $this->hasPrivilege(Acl::ACTION_CREATE);

                $this->pageTitle = Lang::t('Add ' . $this->resourceLabel);

                $model = new Immunization();
                $model_class_name = $model->getClassName();

                if (isset($_POST[$model_class_name])) {
                        $model->attributes = $_POST[$model_class_name];
                         if ($model->save()) {
                                Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                                $this->redirect(array('view' , "id" => $model->Immunid));
                        }
                }

                $this->render('create', array(
                    'model' => $model,
                ));
        }
        
         public function actionView($id) {
        
        $this->hasPrivilege(Acl::ACTION_VIEW);
        $this->showPageTitle = false;
        $this->pageTitle = Lang::t('View Immunization details');

        $this->render('view', array(
            'model' => Immunization::model()->model()->loadModel($id),
        ));
        }
        
         public function actionUpdate($id)
        {
                $this->hasPrivilege(Acl::ACTION_UPDATE);

                $this->pageTitle = Lang::t('Update ' . $this->resourceLabel . ' Details');


                $model = Immunization::model()->loadModel($id);
                $model_class_name = $model->getClassName();

                if (isset($_POST[$model_class_name])) {
                        $model->attributes = $_POST[$model_class_name];
                        
                        if ($model->save()) {
                                Yii::app()->user->setFlash('success', $this->resourceLabel. " details ". Lang::t('SUCCESS_MESSAGE'));
                                $this->redirect(array('view' , "id" => $id));
                        }
                }

                $this->render('update', array(
                    'model' => $model,
                ));
        }
         
        public function actionReports() {
		ini_set('max_execution_time', 3000);
         $this->hasPrivilege(Acl::ACTION_VIEW);
        
         $condition =   (Yii::app()->user->client != 0)? "client = ". (int) Yii::app()->user->client . " AND " : "";
                
         $model =  new Bulkparking();
         $model->setScenario(Bulkparking::SCENARIO_SEARCH);
         $model_class_name = $model->getClassName();
         $show = false;
         $this->pageTitle = Lang::t('Filter Bulk Sms by Date / Month');
         $text = '';
         if (isset($_POST[$model_class_name]['date']) && !empty($_POST[$model_class_name]['date'])) {
             $model->validate();
             $date = ($_POST[$model_class_name]['date']);
             $show = true;
             if($_POST[$model_class_name]['month'] == 1)
             {
                 $date = date('Y-m' , strtotime($date));
                $condition .= ' DATE(FROM_UNIXTIME(timestamp)) LIKE ' . "'$date%'";
                $text =  date('M Y' , strtotime($date));
             }
            else 
            {
                 $condition .= ' DATE(FROM_UNIXTIME(timestamp)) LIKE ' . "'$date%'";
                  $text =  date(' D, M Y' , strtotime($date));
                  
            }
            
            //$condition .=empty($condition) ? '' : ' AND ';
            //var_dump($condition);die;
        }
        
        $this->render('reports', array(
            'model' => Bulkparking::model()->searchModel(array(), 150000, 'timestamp', $condition),
            'date' => $text,
            'show' => $show
           
        ));
    }
    
    public function actionManage()
        {
                $this->hasPrivilege(Acl::ACTION_VIEW);
                $this->activeMenu = self::MENU_MANAGE;
                $this->pageTitle = Lang::t($this->resourceLabel);
                

                $searchModel = Immunization::model()->searchModel(array(), $this->settings[Constants::KEY_PAGINATION], 'Immunname ASC');
                $this->render('manage', array(
                    'model' => $searchModel,
                ));
        }
        
        
}
