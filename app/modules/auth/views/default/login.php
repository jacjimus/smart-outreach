<div class="login-container">
    
    <div class="space-12"></div>
    <div class="position-relative">
        <div id="login-box" class="login-box visible widget-box no-border">
            <div class="widget-body">
                <div class="widget-main">
                    <a href="<?php echo $this->home_url ?>" class="left">
                                <?php
                                
                                 $path =  Yii::app()->baseUrl . DS . "themes" . DS . "default" . DS . "images" . DS . "mombasa_front.png";
               
                                echo CHtml::image($path, '', array('height' => "70px" , 'width' => '300px')). '   ' ?>
                        </a><!-- /.brand -->
                    <h4 class="header blue lighter bigger">
                        
<!--                        <div class="center">
                            <h4><span class="blue"><?php //echo $this->settings[Constants::KEY_SITE_NAME] ?></span></h4>
                        </div>-->
                    </h4>
                    <div class="space-6"></div>
                    <?php $this->renderPartial('application.views.widgets._alert') ?>
                    <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'login-form',
                        'enableClientValidation' => false,
                        'focus' => array($model, 'username'),
                        'clientOptions' => array(
                            'validateOnSubmit' => false,
                        ),
                        'htmlOptions' => array(
                            'class' => '',
                        )
                    ));
                    ?>
                    <?php echo $form->errorSummary($model, ''); ?>
                    <fieldset>
                        <label class="block clearfix">
                            <span class="block input-icon input-icon-right">
                                <?php echo $form->textField($model, 'username', array('class' => 'form-control', 'placeholder' => $model->getAttributeLabel('username'))); ?>
                                <?php echo $form->error($model , 'username')?>
                                <i class="icon-user"></i>
                            </span>
                        </label>
                        <label class="block clearfix">
                            <span class="block input-icon input-icon-right">
                                <?php echo $form->passwordField($model, 'password', array('class' => 'form-control', 'placeholder' => $model->getAttributeLabel('password'))); ?>
                                <i class="icon-lock"></i>
                            </span>
                        </label>
                        <div class="clearfix"> <a href="<?php echo $this->createUrl('forgotPassword') ?>" class="forgot-box">I forgot my password</a></div>
                        <div class="space"></div>
                            
                        <section style="padding-bottom: 20px;">
                    <div class="text-center" style="padding-left: 0px;">
                        <?php if (CCaptcha::checkRequirements()): ?>
                            <?php
                            $this->widget('CCaptcha', array(
                                'buttonLabel' => 'New',
                                'imageOptions' => array('style' => 'width:200px; height:60px; margin-bottom:3px;'),
                            ));
                            ?>
                            <br/>
                            <?php echo CHtml::activeTextField($model, 'verifyCode', array('style' => 'width:200px; padding : 5px; margin-left:-50px; background: #000; color: #fff;' , 'placeholder' => 'Enter the code above')); ?>
                            
                        <?php endif; ?>
                    </div>
                </section>
                        
                        <div class="clearfix">
                            <label class="inline">
                                <?php echo $form->checkBox($model, 'rememberMe', array('class' => 'ace')) ?>
                                <span class="lbl"> Remember Me</span>
                            </label>
                            <button type="submit" class="width-35 pull-right btn btn-sm btn-primary"><i class="icon-key"></i>Login</button>
                        </div>
                        <div class="space-4"></div>
                    </fieldset>
                    <?php $this->endWidget(); ?>
                </div><!-- /widget-main -->
                <div class="toolbar clearfix">
                   
                </div>
            </div><!-- /widget-body -->
        </div><!-- /login-box -->
    </div><!-- /position-relative -->
</div>