<?php

/**
 * @author James Makau <jacjimus@gmail.com>
 * Parent controller for the users module
 */
class UsersModuleController extends AdminModuleController {

        const MENU_USER_ACCOUNTS = 'USER_ACCOUNTS';
        const MENU_USER_ADMIN = 'ADMINUSERS';
        const MENU_USER_ROLES = 'USER_ROLES';
        const MENU_USER_LEVELS = 'USER_LEVELS';
        const MENU_USER_RESOURCES = 'USER_RESOURCES';
        const MENU_USER_PARTNERS = 'USER_PARTNERS';

        public function init()
        {
                parent::init();
        }

}
