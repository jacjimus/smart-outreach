<?php

/**
 * The user/staff controller
 * @author James Makau <jacjimus@gmail.com>
 */
class DefaultController extends UsersModuleController {

        const LOG_ACTIVITY = 'activity_log';
        const LOG_LOGIN = 'login_log';

        /**
         * Executed before every action
         */
        public function init()
        {
                $this->resource = UserResources::RES_USER_ADMIN;
                $this->showPageTitle = false;
                parent::init();
        }

        /**
         * @return array action filters
         */
        public function filters()
        {
                return array(
                    'accessControl', // perform access control for CRUD operations
                    'postOnly + delete,changeStatus,changeLevel,deleteLog', // we only allow deletion via POST request
                    'ajaxOnly + resetPassword,loginLog,activityLog',
                );
        }

        /**
         * Specifies the access control rules.
         * This method is used by the 'accessControl' filter.
         * @return array access control rules
         */
        public function accessRules()
        {
                return array(
                    array('allow',
                        'actions' => array('index', 'partners', 'view', 'create','add', 'delete', 'changeStatus', 'changeLevel', 'deleteLog', 'loginLog', 'activitylog'),
                        'users' => array('@'),
                    ),
                    array('deny', // deny all users
                        'users' => array('*'),
                    ),
                );
        }

        /**
         * Displays a particular model.
         * @param integer $id the ID of the model to be displayed
         */
        public function actionView($id = NULL, $action = NULL)
        {
             $this->resourceLabel = 'User';
                if (NULL === $id)
                        $id = Yii::app()->user->id;
                $user_model = Users::model()->loadModel($id);
                
                if (Users::isMyAccount($id))
                   $this->pageTitle =  ($user_model->password_change ==1) ? "Change Password for the first Login!" : Lang::t("View User: "). $user_model->username;
                $this->showPageTitle = TRUE;
                $first = $user_model->password_change == 1 ? true : false;
                

                if (!empty($action)) {
                        if (!Users::isMyAccount($id)) {
                                $this->checkPrivilege($user_model, Acl::ACTION_UPDATE);
                        }
                        switch ($action) {
                                case Users::ACTION_UPDATE_PERSONAL:
                                        $this->update($person_model);
                                        break;
                                case Users::ACTION_UPDATE_ACCOUNT:
                                        $this->update($user_model);
                                        break;
                                case Users::ACTION_UPDATE_ADDRESS:
                                        $this->update($address_model);
                                        break;
                                case Users::ACTION_RESET_PASSWORD:
                                        $this->resetPassword($user_model);
                                        break;
                                case Users::ACTION_CHANGE_PASSWORD:
                                        $this->changePassword($user_model , $first);
                                        break;
                                default :
                                        $action = NULL;
                        }
                }

                $this->render('view', array(
                    'user_model' => $user_model,
                    'action' => $action
                ));
        }

        /**
         * Creates a new model.
         * If creation is successful, the browser will be redirected to the 'view' page.
         */
        public function actionCreate()
        {
                $this->hasPrivilege(Acl::ACTION_CREATE);
                $this->resourceLabel = 'New User';
                $this->activeMenu = self::MENU_USER_ADMIN;
                $this->pageTitle = Lang::t(Common::pluralize($this->resourceLabel));
              
                //account information
                $user_model = new Users(ActiveRecord::SCENARIO_CREATE);
                $user_model->status = Users::STATUS_ACTIVE;
                $user_model_class_name = $user_model->getClassName();
                //personal information
                
                if (Yii::app()->request->isPostRequest) {
                        $user_model->attributes = $_POST[$user_model_class_name];
                        //var_dump($_POST[$user_model_class_name]);die;
                        $user_model->validate();
                        if (!$user_model->hasErrors()) {
                                if ($user_model->save(FALSE)) {
                                         Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                                        $this->redirect(Controller::getReturnUrl($this->createUrl('view', array('id' => $user_model->id))));
                                }
                        }
                }

                
                $this->render('create', array(
                    'user_model' => $user_model,
                   
                ));
        }
        
        
        /**
         * Update user records
         * @param type $model
         */
        protected function update($model)
        {
                $model_class_name = $model->getClassName();

                if (isset($_POST[$model_class_name])) {
                        $model->attributes = $_POST[$model_class_name];
                        if ($model->save()) {
                                Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                                $this->refresh();
                        }
                }
        }

        protected function resetPassword($model)
        {
                $model->setScenario(Users::SCENARIO_RESET_PASSWORD);
                $model->password = NULL;
                $model_class_name = $model->getClassName();

                if (isset($_POST[$model_class_name])) {
                        $model->attributes = $_POST[$model_class_name];
                        if ($model->save()) {
                                Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                                $this->refresh();
                        }
                }
        }

        protected function changePassword($model , $first = false)
        {
                if (!Users::isMyAccount($model->id))
                        throw new CHttpException(403, Lang::t('403_error'));

                $model->setScenario(Users::SCENARIO_CHANGE_PASSWORD);
                $model->pass = $model->password;
                $model->password = null;
                $model_class_name = $model->getClassName();

                if (isset($_POST[$model_class_name])) {
                        $model->attributes = $_POST[$model_class_name];
                        $currentPass = $model->currentPassword;
                        if ($model->save()) {
                                Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                                $first==true ? $this->redirect(Yii::app()->homeUrl) : $this->refresh();
                        }

                        $model->currentPassword = $currentPass;
                }
        }

        /**
         * Deletes a particular model.
         * If deletion is successful, the browser will be redirected to the 'admin' page.
         * @param integer $id the ID of the model to be deleted
         */
        public function actionDelete($id)
        {
                $model = Users::model()->loadModel($id);
                $this->checkPrivilege($model, Acl::ACTION_DELETE);

                $model->delete();

                if (!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }

        /**
         * Lists all models.
         */
        public function actionIndex()
        {
           //   
            $this->hasPrivilege(Acl::ACTION_VIEW);
                $this->resourceLabel = 'Users';
                $this->activeMenu = self::MENU_USER_ADMIN;
                $this->pageTitle = Lang::t(Common::pluralize($this->resourceLabel));

                //var_dump(Users::model()->getFetchCondition());die;
                $searchModel = UsersView::model()->searchModel(array(), $this->settings[Constants::KEY_PAGINATION], 'username', Users::model()->getFetchCondition());
              
                $this->render('index', array(
                    'model' => $searchModel,
                    'usr' => '',
                    
                ));
        }
        
        public function actionChangeStatus($id, $status)
        {
                $model = Users::model()->loadModel($id);
                $this->checkPrivilege($model);

                $this->hasPrivilege(Acl::ACTION_UPDATE);

                $model->status = $status;
                $model->save(FALSE);

                Yii::app()->user->setFlash('success', 'Success.');

                echo CJSON::encode(array
                    ('redirectLink' => $this->createUrl('view', array('id' => $model->id))));
        }

        public function actionActivitylog($id)
        {
                $this->resource = UserResources::RES_USER_ACTIVITY;
                $model = Users::model()->loadModel($id);
                var_dump($model);die;
                $this->hasPrivilege(Acl::ACTION_VIEW);

                $this->pageTitle = Lang::t('Audit Trail');

                $this->renderPartial('log/_activity', array(
                    'model' => UserActivityLog::model()->searchModel(array('user_id' => $model->id), $this->settings[Constants::KEY_PAGINATION], 'date_created asc'),
                        ), FALSE, TRUE);
        }

        public function actionLoginLog($id)
        {
                $this->resource = UserResources::RES_USER_ACTIVITY;
                $this->pageTitle = Lang::t('Login History');

                $model = Users::model()->loadModel($id);
                $this->hasPrivilege(Acl::ACTION_VIEW);

                $this->renderPartial('log/_login', array(
                    'model' => UserLoginLog::model()->searchModel(array('user_id' => $model->id), $this->settings[Constants::KEY_PAGINATION], 'date_created asc'),
                        ), FALSE, TRUE);
        }

        public function actionDeleteLog($t)
        {
                $this->resource = UserResources::RES_USER_ACTIVITY;
                $this->hasPrivilege(Acl::ACTION_DELETE);
                if ($ids = filter_input(INPUT_POST, 'ids')) {
                        if ($t === self::LOG_ACTIVITY) {
                                UserActivityLog::model()->deleteMany($ids);
                        } else {
                                UserLoginLog::model()->deleteMany($ids);
                        }
                }
        }

        /**
         *
         * @param type $model
         * @param type $action
         * @throws CHttpException
         */
        protected function checkPrivilege($model, $action = Acl::ACTION_UPDATE)
        {
                if (!$model->canBeModified($this, $action))
                        throw new CHttpException(403, Lang::t('403_error'));
        }

}
