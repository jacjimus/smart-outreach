<?php

$grid_id = 'roles-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Lang::t('Manage User Roles'),
    'titleIcon' => null,
    'showExportButton' =>  $this->showlink($this->resource, Acl::ACTION_EXPORT),
    'showSearch' => true,
    'createButton' => array('visible' => $this->showlink($this->resource, Acl::ACTION_CREATE),'modal' => true),
    'toolbarButtons' => array(),
   // 'rowCssClass' => '$data->status==="0"?"bg-danger":""',
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
    'columns' => array(
        array(
            'name' => 'description',
            'type' => 'raw',
            'value' => 'CHtml::link(CHtml::encode($data->description),Yii::app()->controller->createUrl("view",array("id"=>$data->primaryKey)))',
        ),
        array(
            'name' => 'alias',
            'type' => 'raw',
            'value' => 'CHtml::link(CHtml::encode($data->alias),Yii::app()->controller->createUrl("view",array("id"=>$data->primaryKey)))',
        ),
       array(
            'name' => 'status',
            'type' => 'raw',
            'value' => 'CHtml::tag("span", array("class"=>$data->status=="Active"?"badge badge-success":"badge badge-danger"), $data->status)',
        ),
        array(
            'class' => 'ButtonColumn',
            'template' => '{update}&nbsp;&nbsp;&nbsp;&nbsp;{delete}',
            'htmlOptions' => array('class' => 'text-center', 'style' => 'width: 100px;'),
            'buttons' => array(
                'update' => array(
                    'imageUrl' => false,
                    'label' => '<i class="icon-pencil bigger-130"></i>',
                    'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->primaryKey))',
                    'visible' => '$this->grid->owner->showLink("' . UserResources::RES_USER_ROLES . '","' . Acl::ACTION_UPDATE . '")?true:false',
                    'options' => array(
                        'class' => 'green',
                        'title' => 'Edit',
                    ),
                ),
                'delete' => array(
                    'imageUrl' => false,
                    'label' => '<i class="icon-trash bigger-130"></i>',
                    'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->primaryKey))',
                    'visible' => '$this->grid->owner->showLink("' . UserResources::RES_USER_ROLES . '", "' . Acl::ACTION_DELETE . '")?true:false',
                    'options' => array(
                        'class' => 'delete red',
                        'title' => 'Delete',
                    ),
                ),
            )
        ),
    
))));
?>