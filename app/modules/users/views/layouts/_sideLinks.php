<?php if ($this->showLink(UserResources::RES_USER_ACL)): ?>
        <li class="<?php echo $this->getModuleName() === 'users' ? 'active open' : '' ?>">
                <a href="<?php echo Yii::app()->createUrl('users/default/index') ?>" class="dropdown-toggle">
                        <i class="icon-group"></i>
                        <span class="menu-text"> <?php echo Lang::t('Users Management Module') ?></span>
                        <b class="arrow icon-angle-down"></b>
                </a>
                <ul class="submenu">
                        
                                
                        <?php if ($this->showLink(UserResources::RES_USER_ADMIN)): ?>
                                <li class="<?php echo $this->activeMenu === UsersModuleController::MENU_USER_ADMIN ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('users/default/index') ?>"><i class="icon-double-angle-right"></i><?php echo Lang::t('Manage Users') ?></a></li>
                        <?php endif; ?>
                       
                        <?php if ($this->showLink(UserResources::RES_USER_ROLES)): ?>
                                <li class="<?php echo $this->activeMenu === UsersModuleController::MENU_USER_ROLES ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('users/roles/index') ?>"><i class="icon-double-angle-right"></i><?php echo Lang::t('Manage Roles') ?></a></li>
                        <?php endif; ?>
                        <?php if ($this->showLink(UserResources::RES_USER_RESOURCES)): ?>
                                <li class="<?php echo $this->activeMenu === UsersModuleController::MENU_USER_RESOURCES ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('users/resources/index') ?>"><i class="icon-double-angle-right"></i><?php echo Lang::t('Manage Resources') ?></a></li>
                        <?php endif; ?>
                        
                </ul>
        </li>
<?php endif; ?>

