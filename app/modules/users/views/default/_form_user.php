<?php
if (!isset($label_size)):
        $label_size = 4;
endif;
if (!isset($input_size)):
        $input_size = 6;
endif;
$label_class = "col-md-{$label_size} control-label";
$input_class = "col-md-{$input_size}";
if (!$model->getIsNewRecord()) {
        $can_update = $this->showLink($this->resource, Acl::ACTION_UPDATE) || !Users::isMyAccount($model->id);
}
?>

<?php if ($model->getIsNewRecord() || $can_update) : ?>

       <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'account_id', array('class' => $label_class)); ?>
                <div class="<?php echo $input_class ?>">
                        
                        <?php echo CHtml::activeDropDownList($model, 'account_id', Accounts::model()->getListData('id', 'AccountName' , true, Accounts::model()->getFetchCondition(), array() , "description ASC"), array('class' => 'form-control')); ?>
                </div>
        </div>
        
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'role_id', array('class' => $label_class)); ?>
                <div class="<?php echo $input_class ?>">
                        
                        <?php echo CHtml::activeDropDownList($model, 'role_id', UserRoles::model()->getListData('id', 'description' , true , UserRoles::model()->getFetchCondition(), array() , "description ASC"), array('class' => 'form-control chosen-select')); ?>
                </div>
       
        </div>
        
        
<?php endif; ?>
<div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'email', array('class' => $label_class)); ?>
        <div class="<?php echo $input_class ?>">
                <?php echo CHtml::activeEmailField($model, 'email', array('class' => 'form-control', 'maxlength' => 128, 'type' => 'email')); ?>
                <?php echo CHtml::error($model, 'email') ?>
        </div>
</div>
<div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'phone', array('class' => $label_class)); ?>
        <div class="<?php echo $input_class ?>">
                <?php echo CHtml::activeTextField($model, 'phone', array('class' => 'form-control', 'maxlength' => 12)); ?>
                <?php echo CHtml::error($model, 'phone') ?>
        </div>
</div>
<?php if ($model->isNewRecord): ?>
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'username', array('class' => $label_class)); ?>
                <div class="<?php echo $input_class ?>">
                        <?php echo CHtml::activeTextField($model, 'username', array('class' => 'form-control', 'maxlength' => 30)); ?>
                        <?php echo CHtml::error($model, 'username') ?>
                </div>
        </div>
<?php endif; ?>
<?php if ($model->isNewRecord): ?>
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'password', array('class' => $label_class)); ?>
                <div class="<?php echo $input_class; ?>">
                        <?php echo CHtml::activePasswordField($model, 'password', array('class' => 'form-control')); ?>
                        <?php echo CHtml::error($model, 'password') ?>
                </div>
        </div>
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'confirm', array('class' => $label_class)); ?>
                <div class="<?php echo $input_class; ?>">
                        <?php echo CHtml::activePasswordField($model, 'confirm', array('class' => 'form-control')); ?>
                        <?php echo CHtml::error($model, 'confirm') ?>
                </div>
        </div>

        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'password_change', array('class' => "col-md-6")); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeCheckBox($model, 'password_change' , array('class' => 'col-md-6')); ?>
                        <?php echo CHtml::error($model, 'password_change') ?>
                </div>
        </div>
        <div class="form-group">
                <?php echo CHtml::Label(Lang::t('Email the login details to the user.'), 'send_email', array('class' => "col-md-6")); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeCheckBox($model, 'send_email' , array('class' => 'col-md-6')); ?>
                        <?php echo CHtml::error($model, 'send_email') ?>
                </div>
        </div>



<?php endif; ?>


<?php
Yii::app()->clientScript
        ->registerCssFile(Yii::app()->theme->baseUrl . '/plugins/chosen/chosen.min.css')
        ->registerScriptFile(Yii::app()->theme->baseUrl . '/plugins/chosen/chosen.jquery.min.js', CClientScript::POS_END)
        ->registerScript('recepient_id', "$('.chosen-select').chosen();");
?>