<?php

$form = $this->beginWidget('CActiveForm', array(
    'id' => 'account-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => $model->isNewRecord == true ? "form-horizontal" : "form-vertical",
        'role' => 'form',
    )
        ));

?>  
<Br />
<div class="form-group">
                <?php echo $form->labelEx($model, 'client_name', array('class' => 'col-lg-3 control-label')); ?>
                <div class="col-lg-9">
            <?php echo $form->textField($model, 'client_name', array('class' => 'form-control', 'maxlength' => 100)); ?>
                <?php echo CHtml::error($model, 'client_name') ?>
                </div>
        </div>
        <div class="form-group">
                <?php echo $form->labelEx($model, 'client_email', array('class' => 'col-lg-3 control-label')); ?>
                <div class="col-lg-9">
                        <?php echo $form->textField($model, 'client_email', array('class' => 'form-control', 'maxlength' => 100)); ?>
                        <?php echo CHtml::error($model, 'client_email') ?>
                </div>
        </div>
        <div class="form-group">
                <?php echo $form->labelEx($model, 'phone_1', array('class' => 'col-lg-3 control-label')); ?>
                <div class="col-lg-9">
                        <?php echo $form->textField($model, 'phone_1', array('class' => 'form-control', 'maxlength' => 100)); ?>
                        <?php echo CHtml::error($model, 'phone_1') ?>
                </div>
        </div>
        <div class="form-group">
                <?php echo $form->labelEx($model, 'phone_2', array('class' => 'col-lg-3 control-label')); ?>
                <div class="col-lg-9">
                        <?php echo $form->textField($model, 'phone_2', array('class' => 'form-control', 'maxlength' => 100)); ?>
                        <?php echo CHtml::error($model, 'phone_2') ?>
                </div>
        </div>



<div class="clearfix form-actions">
        <div class="col-lg-offset-2 col-lg-9">
                <button class="btn btn-primary" type="submit"><i class="icon-ok bigger-110"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
                &nbsp; &nbsp; &nbsp;
                <a class="btn" href="#" onclick="$('#<?=$model->isNewRecord == true ? "dialogAccount" : "groups-dialog" ?>').dialog('close');"><i class="icon-remove bigger-110"></i><?php echo Lang::t('Cancel') ?></a>
        </div>
</div>
<?php $this->endWidget(); ?>
