<h4 class="lighter">Bulk Sms Type</h4>
<div class="widget-toolbar no-border">
        <ul class="nav nav-tabs my-nav">
                <li class="<?php echo $this->activeTab === 0 ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('bulk/default/dashboard') ?>"><?php echo Lang::t('Bulk Dashboard') ?></a></li>
                <li class="<?php echo $this->activeTab === 1 ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('bulk/default/send') ?>"><?php echo Lang::t('Normal Bulk') ?></a></li>
                <li class="<?php echo $this->activeTab === 2 ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('bulk/default/custome') ?>"><?php echo Lang::t('Custome Bulk') ?></a></li>
        </ul>
</div>
