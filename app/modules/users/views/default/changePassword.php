<?php
$this->breadcrumbs = array(
    CHtml::encode($model->name) => array('view', 'id' => $model->id),
    $this->pageTitle,
);
?>
<div class="row">
    <div class="col-sm-10">
        <h1 class="txt-color-blueDark">
            <?php echo CHtml::encode($model->name); ?>
        </h1>
    </div>
</div>
<hr/>
<div class="row">
    <div class="col-xs-12 col-sm-2">
        <?php $this->renderPartial('_userMenu', array('model' => $model)) ?>
    </div>
    <div class="col-xs-12 col-sm-10">
        <?php $this->renderPartial('forms/_changePassword', array('model' => $model)); ?>
    </div>
</div>
