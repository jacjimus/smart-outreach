<?php

$grid_id = 'users-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Lang::t('Manage Users'),
    'titleIcon' => null,
    'showExportButton' => false,
    'showSearch' => true,
    'createButton' => array('visible'=> $this->showlink( $this->resource , Acl::ACTION_CREATE )?true:false),
    'toolbarButtons' => array(),
    'showRefreshButton' => false,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        
        'columns' => array(
        
        array(
            'name' => 'username',
            'type' => 'raw',
            'value' => 'CHtml::link(CHtml::encode($data->username),Yii::app()->controller->createUrl("view",array("id"=>$data->id)))',
        ),
        array(
            'name' => 'name',
        ),
        array(
            'name' => 'email',
        ),
        array(
            'name' => 'date_joined',
            'value' => 'MyYiiUtils::formatDate($data->date_joined)',
        ),
        array(
            'name' => 'status',
            'type' => 'raw',
            'value' => 'CHtml::tag("span", array("class"=>$data->status==="' . Users::STATUS_ACTIVE . '"?"badge badge-success":"badge badge-danger"), $data->status)',
        ),
       
        array(
            'name' => 'role',
            'visible' => empty($model->role_id),
        ),
        array(
            'name' => 'account',
            'type' => 'raw',
            'value' => 'Accounts::model()->get($data->account , "AccountName")'
        ),
        array(
            'class' => 'ButtonColumn',
            'template' => '{view}{update}{delete}',
            'buttons' => array(
                'view' => array(
                    'imageUrl' => false,
                    'label' => '<i class="icon-eye-open bigger-130"></i>',
                    'url' => 'Yii::app()->controller->createUrl("view",array("id"=>$data->primaryKey))',
                    'options' => array(
                        'class' => 'blue',
                        'title' => Lang::t('View details'),
                    ),
                ),
                'update' => array(
                    'imageUrl' => false,
                    'label' => '<i class="icon-pencil bigger-130"></i>',
                    'url' => 'Yii::app()->controller->createUrl("view",array("id"=>$data->primaryKey,"action" => "u_account"))',
                    'visible' => '$data->canBeModified($this->grid->owner,"' . Acl::ACTION_UPDATE . '")',
                    'options' => array(
                        'class' => 'green',
                        'title' => Lang::t('Edit'),
                    ),
                ),
                'delete' => array(
                    'imageUrl' => false,
                    'label' => '<i class="icon-trash bigger-130"></i>',
                    'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->primaryKey))',
                    'visible' => '$data->canBeModified($this->grid->owner,"' . Acl::ACTION_DELETE . '")',
                    'options' => array(
                        'class' => 'delete red',
                        'title' => Lang::t('Delete'),
                    ),
                ),
            )
        ),
    ),
)
)    
);
?>
