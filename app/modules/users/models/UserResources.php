<?php

/**
 * This is the model class for table "user_resources".
 *
 * The followings are the available columns in table 'user_resources':
 * @property string $id
 * @property string $description
 * @property integer $viewable
 * @property integer $createable
 * @property integer $updateable
 * @property integer $deleteable
 * @property integer $exportable
 *
 * The followings are the available model relations:
 * @property UserRolesOnResources[] $usersRolesOnResources
 */
class UserResources extends ActiveRecord {

        //system resources should correspond to those saved in the db:

        const RES_SETTINGS_GENERAL = 'SETTINGS_GENERAL';
        const RES_SETTINGS_SIGNATURES = 'SETTINGS_SIGNATURES';
        const RES_SETTINGS_SMS = 'SETTINGS_SMS';
        const RES_SETTINGS_RUNTIME = 'SETTINGS_RUNTIME';
        const RES_SETTINGS_CRON = 'SETTINGS_CRON';
        const RES_MODULES_ENABLED = 'MODULES_ENABLED';
        const RES_USER_ROLES = 'USER_ROLES';
        const RES_USER_RESOURCES = 'USER_RESOURCES';
        const RES_USER_ENGINEER = 'USER_ENGINEER';
        const RES_USER_SUPERADMIN = 'USER_SUPERADMIN';
        const RES_USER_ADMIN = 'USER_ADMIN';
        const RES_USER_DEFAULT = 'USER_DEFAULT';
        const RES_USER_ACL = 'USER_ACL';
        const RES_USER_ACTIVITY = 'USER_ACTIVITY';
        const RES_DOCUMENTATION = 'DOCUMENTATION';
        const RES_MESSAGE = 'MESSAGE';
        
        
        // Account Module Resources
         const RES_ACCOUNTS = "ACCOUNT";
         const RES_ACCOUNTS_USERS = "ACCOUNTS_USERS";
         const RES_ACCOUNT_CREDITS = "ACCOUNT_CREDITS";
         const RES_ACCOUNT_BORROW = "ACCOUNT_BORROW";
         const RES_ACCOUNT_PURCHASE = "ACCOUNT_PURCHASE";
        
        
        // Messaging Module Resources
         const RES_MESSAGING = "MESSAGING";
         const RES_QUICK_MESSAGING = "QUICK_MESSAGING";
         const RES_PROFESSIONAL_MESSAGING = "PROFESSIONAL_MESSAGING";
         const RES_SMS2FILE_MESSAGING = "SMS2FILE_MESSAGING";
         const RES_SMS_INBOX = "SMS_INBOX";
         const RES_SMS_VERIFY = "SMS_VERIFY";
         const RES_SMS_APPROVALS = "SMS_APPROVALS";
         const RES_SMS_LOGS = "SMS_LOGS";
         const RES_SMS_PHONEBOOK = "SMS_PHONEBOOK";
         const RES_SMS_E_VERIFY = "SMS_E_VERIFY";
         const RES_E_VERIFY_INCOMING = "E_VERIFY_INCOMING";
         const RES_E_VERIFY_REPORTING = "E_VERIFY_REPORTING";
         //const RES_SMS_LOGS = "SMS_LOGS";
        
        // Tools Module Resources
         const RES_TOOLS = "TOOLS";
         const RES_TOOLS_GROUPS = "TOOLS_GROUPS";
         const RES_TOOLS_ADDRESS_BOOK = "TOOLS_ADDRESS_BOOK";
         const RES_TOOLS_IMPORT_CONTACTS = "TOOLS_IMPORT_CONTACTS";
         
        
        // Postnatal Module Resources
         const RES_POSTNATAL = "POSTNATAL";
         const RES_CREATE_POSTNATAL = "CREATE_POSTNATAL";
         const RES_MANAGE_POSTNATAL = "MANAGE_POSTNATAL";
        
        
        // SMS Analytics Resources
        
        const RES_ANALYTICS = "ANALYTICS";
        const RES_INBOUND_ANALYTICS = "INBOUND_ANALYTICS";
        const RES_OUTBOUND_ANALYTICS = "OUTBOUND_ANALYTICS";
       
        
        // Reports Resources
        
        const RES_REPORTS = "REPORTS";
        const RES_REPORT_PATIENT = "REPORT_PATIENT";
        const RES_REPORT_PREVENTIVE = "REPORT_PREVENTIVE";
        const RES_REPORT_ANTENATAL = "REPORT_ANTENATAL";
        const RES_REPORT_CHILD = "REPORT_CHILD";
        const RES_REPORT_POSTNATAL = "REPORT_POSTNATAL";
        const RES_REPORT_IMMUNIZATION = "REPORT_IMMUNIZATION";
        const RES_SYSTEM_LOGS = "SYSTEM_LOGS";
        
        /**
         * Returns the static model of the specified AR class.
         * @param string $className active record class name.
         * @return UserResources the static model class
         */
        public static function model($className = __CLASS__)
        {
                return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName()
        {
                return 'tbl_user_resources';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules()
        {

                return array(
                    array('id,description', 'required'),
                    array('viewable, createable, updateable, deleteable , exportable', 'numerical', 'integerOnly' => true),
                    array('id', 'length', 'max' => 128),
                    array('id', 'unique'),
                    array('description', 'length', 'max' => 255),
                    array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
                );
        }

        /**
         * @return array relational rules.
         */
        public function relations()
        {
                return array(
                    'usersRolesOnResources' => array(self::HAS_MANY, 'UserRolesOnResources', 'resource_id'),
                );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels()
        {
                return array(
                    'id' => Lang::t('ID'),
                    'description' => Lang::t('Description'),
                    'viewable' => Lang::t('Viewable'),
                    'createable' => Lang::t('Createable'),
                    'updateable' => Lang::t('Updateable'),
                    'deleteable' => Lang::t('Deleteable'),
                    'exportable' => Lang::t('Exportable'),
                );
        }

        public function searchParams()
        {
                return array(
                    array('description', self::SEARCH_FIELD, true, 'OR'),
                    array('id', self::SEARCH_FIELD, true, 'OR'),
                    'id',
                );
        }

        /**
         * Get resources
         * @param type $exluded_resources. Resources not to be included
         * @return type
         */
        public function getResources($exluded_resources = null)
        {
                $command = Yii::app()->db->createCommand()
                        ->select()
                        ->from($this->tableName());
                if (!empty($exluded_resources))
                        $command->where(array('NOT IN', 'id', $exluded_resources));
                        $command->order = "description ASC";
                return $command->queryAll();
        }

}
