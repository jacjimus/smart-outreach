<?php

/**
 * This is the model class for table "user_roles".
 *
 * The followings are the available columns in table 'user_roles':
 * @property integer $id
 * @property string $status
 * @property string $description
 *
 * The followings are the available model relations:
 * @property UserRolesOnResources[] $userRolesOnResources
 */
class UserRoles extends ActiveRecord implements IMyActiveSearch {

        /**
         * Returns the static model of the specified AR class.
         * @param string $className active record class name.
         * @return UserRoles the static model class
         */
        public static function model($className = __CLASS__)
        {
                return parent::model($className);
        }
        
        
        const USER_VENDOR_ENGINEER = "ENGINEER";
        const USER_COUNTY_ADMIN = "SUPERADMIN";
        const USER_DEPARTMENTAL_ADMIN = "ADMIN";
        const USER_LEVEL_NORMAL = "NORMAL";

        /**
         * @return string the associated database table name
         */
        public function tableName()
        {
                return 'tbl_admin_roles';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules()
        {
                return array(
                    array('description, alias', 'required'),
                    array('status, alias', 'length', 'max' => 25),
                    array('description, alias', 'unique', 'message' => "{attribute} already exists"),
                    array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
                );
        }

        /**
         * @return array relational rules.
         */
        public function relations()
        {
                return array(
                    'userRolesOnResources' => array(self::HAS_MANY, 'UserRolesOnResources', 'role_id'),
                );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels()
        {
                return array(
                    'id' => Lang::t('ID'),
                    'status' => Lang::t('Status'),
                    'description' => Lang::t('Description'),
                    'alias' => Lang::t('Alias'),
                );
        }
        
        
        public function getFetchCondition()
        {
                $condition = '';
                switch (UserRoles::model()->get(Yii::app()->user->role , "alias")) {
                        case self::USER_VENDOR_ENGINEER:
                                $condition .= "";
                                break;
                        case self::USER_COUNTY_ADMIN:
                                $condition .= '  (`id`!="' . $this->getRoleID(self::USER_VENDOR_ENGINEER) . '")';
                                break;
                        case self::USER_DEPARTMENTAL_ADMIN:
                                $condition .= '  (`id`!="' . $this->getRoleID(self::USER_VENDOR_ENGINEER) . '" AND `id`!="' . $this->getRoleID(self::USER_COUNTY_ADMIN) . '")';
                                break;
                        default :
                                throw new CHttpException(403, Lang::t('403_error'));
                }

                return $condition;
        }
        
        protected function getRoleID($alias)
        {
             return UserRoles::model()->getScaler('id' , "alias LIKE '". $alias ."'");
        }

        public function searchParams()
        {
                return array(
                    array('description', self::SEARCH_FIELD, true),
                    array('alias', self::SEARCH_FIELD, true),
                    'id',
                );
        }

}
