<?php

class CreditController extends AccountModuleController {

        public function init()
        {
                $this->resource = UserResources::RES_ACCOUNT_CREDITS;
                $this->activeMenu = self::MENU_CREDIT;
                $this->resourceLabel = 'Credit';
                $this->showPageTitle = false;
                parent::init();
        }

        /**
         * @return array action filters
         */
        public function filters()
        {
                return array(
                    'accessControl', // perform access control for CRUD operations
                    'postOnly + delete',
                );
        }

        /**
         * Specifies the access control rules.
         * This method is used by the 'accessControl' filter.
         * @return array access control rules
         */
        public function accessRules()
        {
                return array(
                    array('allow',
                        'actions' => array('index', 'view', 'add', 'reduce', 'manage', 'update', 'reports' ),
                        'users' => array('@'),
                    ),
                    array('deny', // deny all users
                        'users' => array('*'),
                    ),
                );
        }

         public function actionAdd($id)
        {
        $model = new Credit();
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
                $model->attributes = $_POST[$model_class_name];
                $model->UpdateDate = date('Y-m-d h:i:s');
                if ($model->save())
                //----- begin new code --------------------
                    if (!empty($_GET['asDialog'])) {
                        //Close the dialog, reset the iframe and update the grid
                        echo CHtml::script("window.parent.$('#addCredit').dialog('close');window.parent.$('#add-frame').attr('src','');window.parent.$.fn.yiiGridView.update('{$_GET['gridId']}');");
                        Yii::app()->end();
                    } 
            }

            //----- begin new code --------------------
            if (!empty($_GET['asDialog']))
                //$this->layout = '//layouts/iframe';
            //----- end new code --------------------

            $this->renderPartial('add', array(
                'model' => $model, 'id' => $id
            ), false, true);
        }
        
        
         public function actionReduce($id)
        {
        $model = new Accounts;
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $add = Accounts::model()->loadModel($_POST[$model_class_name]['id']);
                $add->Credit = $add->Credit + $_POST[$model_class_name]['Amount'];
                if ($add->save()) {
                    $reduce = Accounts::model()->loadModel($id);
                    $reduce->Credit -= $_POST[$model_class_name]['Amount'];
                    $reduce->save(False);
                //----- begin new code --------------------
                    if (!empty($_GET['asDialog'])) {
                        //Close the dialog, reset the iframe and update the grid
                        echo CHtml::script("window.parent.$('#reduceCredit').dialog('close');window.parent.$('#reduce-frame').attr('src','');window.parent.$.fn.yiiGridView.update('{$_GET['gridId']}');");
                        Yii::app()->end();
                    } 
                }
            }

            //----- begin new code --------------------
            if (!empty($_GET['asDialog']))
                //$this->layout = '//layouts/iframe';
            //----- end new code --------------------

            $this->renderPartial('reduce', array(
                'model' => $model, 'id' => $id
            ), false, true);
        }
    

    public function actionView($id) {
        
        $this->hasPrivilege(Acl::ACTION_VIEW);
        $this->showPageTitle = false;
        $this->pageTitle = Lang::t('View Immunization details');

        $this->render('view', array(
            'model' => Immunization::model()->model()->loadModel($id),
        ));
        }
        
         public function actionUpdate($id)
        {
                $this->hasPrivilege(Acl::ACTION_UPDATE);

                $this->pageTitle = Lang::t('Update ' . $this->resourceLabel . ' Details');


                $model = Immunization::model()->loadModel($id);
                $model_class_name = $model->getClassName();

                if (isset($_POST[$model_class_name])) {
                        $model->attributes = $_POST[$model_class_name];
                        
                        if ($model->save()) {
                                Yii::app()->user->setFlash('success', $this->resourceLabel. " details ". Lang::t('SUCCESS_MESSAGE'));
                                $this->redirect(array('view' , "id" => $id));
                        }
                }

                $this->render('update', array(
                    'model' => $model,
                ));
        }
         
        public function actionReports() {
		ini_set('max_execution_time', 3000);
         $this->hasPrivilege(Acl::ACTION_VIEW);
        
         $condition =   (Yii::app()->user->client != 0)? "client = ". (int) Yii::app()->user->client . " AND " : "";
                
         $model =  new Bulkparking();
         $model->setScenario(Bulkparking::SCENARIO_SEARCH);
         $model_class_name = $model->getClassName();
         $show = false;
         $this->pageTitle = Lang::t('Filter Bulk Sms by Date / Month');
         $text = '';
         if (isset($_POST[$model_class_name]['date']) && !empty($_POST[$model_class_name]['date'])) {
             $model->validate();
             $date = ($_POST[$model_class_name]['date']);
             $show = true;
             if($_POST[$model_class_name]['month'] == 1)
             {
                 $date = date('Y-m' , strtotime($date));
                $condition .= ' DATE(FROM_UNIXTIME(timestamp)) LIKE ' . "'$date%'";
                $text =  date('M Y' , strtotime($date));
             }
            else 
            {
                 $condition .= ' DATE(FROM_UNIXTIME(timestamp)) LIKE ' . "'$date%'";
                  $text =  date(' D, M Y' , strtotime($date));
                  
            }
            
            //$condition .=empty($condition) ? '' : ' AND ';
            //var_dump($condition);die;
        }
        
        $this->render('reports', array(
            'model' => Bulkparking::model()->searchModel(array(), 150000, 'timestamp', $condition),
            'date' => $text,
            'show' => $show
           
        ));
    }
    
    public function actionIndex()
        {
                $this->hasPrivilege(Acl::ACTION_VIEW);
                $this->activeMenu = self::MENU_CREDIT;
                $this->pageTitle = Lang::t($this->resourceLabel);
                

                $searchModel = Accounts::model()->searchModel(array(), $this->settings[Constants::KEY_PAGINATION], 'AccountName ASC');
                $this->render('manage', array(
                    'model' => $searchModel,
                ));
        }
        
        
}
