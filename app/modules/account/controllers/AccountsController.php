<?php

/**
 * User Levels management controller
 * @author Fred <fred@btimillman.com>
 */
class AccountsController extends AccountModuleController {

        public function init()
        {
                $this->resourceLabel = 'Client Accounts';
                $this->resource = UserResources::RES_ACCOUNTS_USERS;
                $this->activeMenu = self::MENU_ACCOUNTS;
                 $this->showPageTitle = false;
                parent::init();
        }

        /**
         * @return array action filters
         */
        public function filters()
        {
                return array(
                    'accessControl', // perform access control for CRUD operations
                    'postOnly + delete', // we only allow deletion via POST request
                );
        }

        /**
         * Specifies the access control rules.
         * This method is used by the 'accessControl' filter.
         * @return array access control rules
         */
        public function accessRules()
        {
                return array(
                    array('allow',
                        'actions' => array('index', 'create', 'update', 'delete' , 'borrow', 'purchase', 'sender' , 'credit'),
                        'users' => array('@'),
                    ),
                    array('deny', // deny all users
                        'users' => array('*'),
                    ),
                );
        }

        /**
         * Creates a new model.
         * If creation is successful, the browser will be redirected to the 'view' page.
         */
        public function actionCreate()
        {
                $this->hasPrivilege(Acl::ACTION_CREATE);

                $this->pageTitle = 'Add ' . $this->resourceLabel;

                $model = new Accounts;
                $model_class_name = $model->getClassName();

                if (isset($_POST[$model_class_name])) {
                        $model->attributes = $_POST[$model_class_name];
                       
                        if ($model->save())
                                $this->redirect(array('sender' , 'id' => $model->id));
                }
                $this->render('create', array(
                    'model' => $model,
                    'id' => null
                ));
        }
        /*
         * 
         */
        
         public function actionCredit($id) {
        
        $this->hasPrivilege(Acl::ACTION_VIEW);
       
        $this->activeTab = 2;
        $this->pageTitle = Lang::t('View Account Credit Details');

        $this->render('view', array(
            'model' => Accounts::model()->loadModel($id),
            'id' => $id
        ));
    }
        
        

        /**
         * Updates a particular model.
         * If update is successful, the browser will be redirected to the 'view' page.
         * @param integer $id the ID of the model to be updated
         */
        public function actionUpdate($id)
        {
                $this->hasPrivilege(Acl::ACTION_UPDATE);

                $this->pageTitle = 'Edit ' . $this->resourceLabel;
                $this->activeTab = 0;
                
                $model = Accounts::model()->loadModel($id);
                $model_class_name = $model->getClassName();

                if (isset($_POST[$model_class_name])) {
                        $model->attributes = $_POST[$model_class_name];
                        //var_dump($model->Hod);die;
                              if ($model->save())
                                $this->redirect(array('sender' , 'id' => $id));
                }

                $this->render('update', array(
                    'model' => $model,
                    'id' => $id
                ));
        }
        /**
         * Updates a particular model.
         * If update is successful, the browser will be redirected to the 'view' page.
         * @param integer $id the ID of the model to be updated
         */
        public function actionSender($id)
        {
                $this->hasPrivilege(Acl::ACTION_UPDATE);

                $this->pageTitle = 'Update Sender ID details :  [ ' . Accounts::model()->get($id , 'AccountName'). ' ]';
                $this->activeTab = 1;
                $model = Accounts::model()->loadModel($id);
                $model_class_name = $model->getClassName();

                if (isset($_POST[$model_class_name])) {
                        $model->attributes = $_POST[$model_class_name];
                                                if ($model->save())
                                $this->redirect(array('credit' , 'id' => $id));
                }

                $this->render('sender', array(
                    'model' => $model,
                    'id' => $id
                ));
        }
        /**
         * Updates a particular model.
         * If update is successful, the browser will be redirected to the 'view' page.
         * @param integer $id the ID of the model to be updated
         */
        public function actionBorrow()
        {
                $this->hasPrivilege(Acl::ACTION_CREATE);

                $this->pageTitle = 'Borrow credit from other Departments';
                $this->activeTab = 1;
                $model = Accounts::model()->searchModel(array(), $this->settings[Constants::KEY_PAGINATION], 'AccountName');
                $this->renderPartial('borrow', array(
                    'model' => $model
                ));
        }
        
         /**
         * Purchase credit.
         * If update is successful, the browser will be redirected to the 'view' page.
         * @param integer $id the ID of the model to be updated
         */
        public function actionPurchase($_vw = false)
        {
                $this->hasPrivilege(Acl::ACTION_CREATE);

                $this->pageTitle = 'Purchase credit from Vendor';
                $this->activeTab = 1;
                $model = new MsgEmailOutbox;
                $model_class_name = $model->getClassName();

                if (isset($_POST[$model_class_name])) {
                        $model->attributes = $_POST[$model_class_name];
                        if ($model->save(false)){
                        $account = Accounts::model()->loadModel(Yii::app()->user->account);
                        $account->purchase_status = Accounts::PURCHASE_OK;
                        $account->save(false);
                        Yii::app()->user->setFlash('success', Lang::t("Request has been send"));
                        if (!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
                        $this->refresh();
                }
                }
                $this->renderPartial('purchase', array(
                    'model' => $model,
                    "_vw" => $_vw
                ));
        }

        /**
         * Deletes a particular model.
         * If deletion is successful, the browser will be redirected to the 'admin' page.
         * @param integer $level the ID of the model to be deleted
         */
        public function actionDelete($level)
        {
                $this->hasPrivilege(Acl::ACTION_DELETE);

                UserLevels::model()->loadModel($level)->delete();

                if (!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }

        /**
         * Lists all user levels
         */
        public function actionIndex()
        {
                $this->hasPrivilege(Acl::ACTION_VIEW);
                $this->showPageTitle = false;
                $this->pageTitle = Lang::t($this->resourceLabel);
                $model = Accounts::model()->searchModel(array(), $this->settings[Constants::KEY_PAGINATION], 'AccountName');
                $this->render('index', array(
                    'model' => $model
                ));
        }

}
