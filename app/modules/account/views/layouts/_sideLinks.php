<?php if ($this->showLink(UserResources::RES_ACCOUNTS)): ?>
        <li  class="<?php echo $this->getModuleName() === 'account' ? 'active open' : '' ?>">
                <a href="#" class="dropdown-toggle">
                        <i class="icon-bitbucket-sign"></i>
                        <span class="menu-text"> <?php echo Lang::t('Account Management Module') ?></span>
                         <b class="arrow icon-angle-down"></b>
                </a>
            <ul class="submenu">
                        <?php if ($this->showLink(UserResources::RES_ACCOUNTS_USERS)): ?>
                                <li class="<?php echo $this->activeMenu === AccountModuleController::MENU_ACCOUNTS ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl(ModulesEnabled::MOD_ACCOUNT . '/accounts/') ?>"><i class="icon-double-angle-right"></i><?php echo Lang::t('Manage Accounts') ?></a></li>
                        <?php endif; ?>
                              
                        <?php if ($this->showLink(UserResources::RES_ACCOUNT_CREDITS)): ?>
                                <li class="<?php echo $this->activeMenu === AccountModuleController::MENU_CREDIT ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl(ModulesEnabled::MOD_ACCOUNT . '/credit/') ?>"><i class="icon-double-angle-right"></i><?php echo Lang::t('Account Credits') ?></a></li>
                        <?php endif; ?>
                        <?php if ($this->showLink(UserResources::RES_ACCOUNT_BORROW)): ?>
                                <li class="<?php echo $this->activeMenu === AccountModuleController::MENU_BORROW ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl(ModulesEnabled::MOD_ACCOUNT . '/accounts/borrow') ?>" class="show-colorbox"><i class="icon-double-angle-right"></i><?php echo Lang::t('Borrow Credits') ?></a></li>
                        <?php endif; ?>
                        <?php if ($this->showLink(UserResources::RES_ACCOUNT_PURCHASE)): ?>
                                <li class="<?php echo $this->activeMenu === AccountModuleController::MENU_PURCHASE ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl(ModulesEnabled::MOD_ACCOUNT . '/accounts/purchase/') ?>" class="show-colorbox"><i class="icon-double-angle-right"></i><?php echo Lang::t('Purchase Credits') ?></a></li>
                        <?php endif; ?>
                                               
                       
                        
                </ul>
        </li>
<?php endif; ?>

