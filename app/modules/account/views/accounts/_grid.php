<?php

$grid_id = 'accounts-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Lang::t('Manage sub Accounts'),
    'titleIcon' => null,
    'showExportButton' => false,
    'showSearch' => true,
    'createButton' => array('visible'=> $this->showlink( $this->resource , Acl::ACTION_CREATE )?true:false),
    'toolbarButtons' => array(),
    'showRefreshButton' => false,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        
        'columns' => array(
        
        'AccountName',
        array(
            'name' => 'Description',
            'value' => 'Common::smartWordwrap($data->Description, 70,"<br/>")',
            'type' => 'raw',
        ),
        array(
            'name' => 'Hod',
            'value' => 'UsersView::model()->get($data->Hod, "name")',
            'type' => 'raw',
        ),
        array(
            'name' => 'Status',
            'type' => 'raw',
            'value' => 'CHtml::tag("span", array("class"=>$data->Status=="Active"?"badge badge-success":"badge badge-danger"), $data->Status)',
        ),
        array(
            'class' => 'ButtonColumn',
            'template' => '{update}&nbsp;&nbsp;&nbsp;&nbsp;{delete}',
            'htmlOptions' => array('class' => 'text-center', 'style' => 'width: 100px;'),
            'buttons' => array(
                'update' => array(
                    'imageUrl' => false,
                    'label' => '<i class="icon-pencil bigger-130"></i>',
                    'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
                    'visible' => '$this->grid->owner->showLink("' . UserResources::RES_ACCOUNTS_USERS. '","' . Acl::ACTION_UPDATE . '")?true:false',
                    'options' => array(
                        'class' => 'green',
                        'title' => 'Edit',
                    ),
                ),
                'delete' => array(
                    'imageUrl' => false,
                    'label' => '<i class="icon-trash bigger-130"></i>',
                    'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
                    'visible' => '$this->grid->owner->showLink("' . UserResources::RES_ACCOUNTS_USERS . '", "' . Acl::ACTION_DELETE . '")?true:false',
                    'options' => array(
                        'class' => 'delete red',
                        'title' => 'Delete',
                    ),
                ),
            )
        ),
    ),
))
);
?>