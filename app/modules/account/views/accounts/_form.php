<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'user-levels-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data','role' => 'form')
        ));
?>
<div class="panel-group" id="accordion">
    <div class="row">
    <div class="col-md-<?php echo Yii::app()->controller->action->id == 'sender'? 9 : 7?>">
 <div class="panel panel-default">
                <div class="panel-heading">
                        <h4 class="panel-title">
                                <i class="fa fa-lock"></i> <?php echo Lang::t('Department Details') ?>
                                
                        </h4>
                </div>
          <div class="panel-body">
<?php
if(Yii::app()->controller->action->id == 'sender'):?>

<div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'Sender_id', array('class' => 'col-lg-4 control-label')); ?>
                <div class="col-lg-6">
                        
                        <?php echo CHtml::activeDropDownList($model, 'Sender_id', Sender::model()->getListData('id', 'SenderID'), array('class' => 'form-control')); ?>
                </div>
        </div>
<?php else: ?>
<div class="form-group">
        <?php echo $form->labelEx($model, 'AccountName', array('class' => 'col-lg-4 control-label')); ?>
        <div class="col-lg-6">
                <?php echo $form->textField($model, 'AccountName', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'AccountName'); ?>
        </div>
</div>

<div class="form-group">
        <?php echo $form->labelEx($model, 'Description', array('class' => 'col-lg-4 control-label')); ?>
        <div class="col-lg-6">
                <?php echo $form->textArea($model, 'Description', array('class' => 'form-control', 'cols' => 60 , 'rows' => 2)); ?>
            <?php echo $form->error($model, 'Description'); ?>
        </div>
</div>

<div class="form-group">
        <?php echo $form->labelEx($model, 'Hod', array('class' => 'col-lg-4 control-label')); ?>
        <div class="col-lg-6">
                <?php echo $form->dropDownList($model, 'Hod', UsersView::model()->getListData('id', 'name'), array('class' => 'form-control chosen-select')); ?>
                <?php echo $form->error($model, 'Hod'); ?>
        </div>
</div>
<div class="form-group">
        <?php echo $form->labelEx($model, 'Status', array('class' => 'col-lg-4 control-label')); ?>
        <div class="col-lg-6">
                <?php echo $form->dropDownList($model, 'Status', array('Active' => 'Active', 'Inactive' => 'Inactive'), array( 'prompt' => '[Select ]' ,'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'Status'); ?>
        </div>
</div>
<?php endif; ?>

 </div> 
    </div>
    </div>
     <div class="col-md-5">
         <?php
if(Yii::app()->controller->action->id != 'sender'):?>
 <div class="panel panel-default">
                <div class="panel-heading">
                        <h4 class="panel-title">
                                   <i class="fa fa-columns"></i> <?php echo Lang::t('Department customization') ?>
                               
                        </h4>
                </div>
     <div id="person" class="panel-collapse collapse in">
                        <div class="panel-body">
                                 <div class="form-group">
                                        <div class="col-sm-10">
                                                <?php $this->renderPartial('_image_field', array('model' => $model, 'htmlOptions' => array('field_class' => 'col-md-9'))) ?>
                                        </div>
                                </div>
                        </div>
    </div>
<!--     <div id="person" class="panel-collapse collapse in">
                        <div class="panel-body">
                                <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                                <?php //$this->renderPartial('application.views.person._form_fields', array('model' => $user_model)) ?>
                                        </div>
                                </div>
                        </div>
                </div>-->
 </div>
         <?php endif;?>
   
<br />
<br />
<br />


<div class="form-group">
        <div class="col-md-12">
                <button class="btn btn-sm btn-success" type="submit"><i class="icon-ok bigger-110"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
                &nbsp; &nbsp; &nbsp;
                <a class="btn btn-sm" href="<?php echo Controller::getReturnUrl($this->createUrl('index')) ?>"><i class="icon-remove bigger-110"></i><?php echo Lang::t('Cancel') ?></a>
        </div>
 </div>
    </div>
 </div>
     
     
<?php $this->endWidget(); ?>
<?php
Yii::app()->clientScript
        ->registerCssFile(Yii::app()->theme->baseUrl . '/plugins/chosen/chosen.min.css')
        ->registerScriptFile(Yii::app()->theme->baseUrl . '/plugins/chosen/chosen.jquery.min.js', CClientScript::POS_END)
        ->registerScript('Accounts[Hod]', "$('.chosen-select').chosen();");
?>