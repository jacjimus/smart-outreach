<?php

$this->breadcrumbs = array('Accounts Module' => 'index',
    $this->pageTitle,
);
$dept = Accounts::model()->get(Yii::app()->user->account , "AccountName");
$sender = UsersView::model()->get(Yii::app()->user->id , "name");

$form = $this->beginWidget('CActiveForm', array(
    'id' => 'purchase-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array('class' => 'form-horizontal','role' => 'form')
        ));
?>
<div class="panel-group" id="accordion">
    <div class="row">
    <div class="col-md-12">
 <div class="panel panel-default">
                <div class="panel-heading">
                        <h4 class="panel-title">
                                <i class="fa fa-lock"></i> <?php echo Lang::t($this->pageTitle) ?>
                                
                        </h4>
                </div>
          <div class="panel-body">

<div class="form-group">
        <?php echo $form->labelEx($model, 'Credit Amount', array('class' => 'col-lg-4 control-label')); ?>
        <div class="col-lg-6">
                <?php echo CHtml::numberField('Credit' , '', array("required" => true)); ?>
                <?php echo $form->hiddenField($model, 'from_email' , array("value" => $dept)); ?>
                <?php echo $form->hiddenField($model, 'subject' , array("value" => "Request for Credit Topup for $dept")); ?>
                <?php echo $form->hiddenField($model, 'to_email' , array("value" => Yii::app()->settings->get(Constants::CATEGORY_GENERAL, Constants::KEY_VENDOR_EMAIL))); ?>
                <?php echo $form->hiddenField($model, 'sent_by' , array("value" => Yii::app()->user->id)); ?>
                <?php echo $form->hiddenField($model, 'from_name' ,array("value" => $sender)); ?>
                <?php echo $form->hiddenField($model, 'date_queued' ,array("value" => date("Y-m-d h:i:s"))); ?>
               
            <?php echo $form->error($model, 'Credit'); ?>
        </div>
</div>

<div class="form-group">
        <?php echo $form->labelEx($model, 'message', array('class' => 'col-lg-4 control-label')); ?>
        <div class="col-lg-6">
                <?php echo $form->textArea($model, 'message', array('class' => 'form-control', 'cols' => 60 , 'rows' => 2)); ?>
            <?php echo $form->error($model, 'message'); ?>
        </div>
</div>


 </div> 
     

                 
  
<div class="clearfix form-actions">
        <div class="col-lg-offset-2 col-lg-9">
                <button class="btn btn-success" type="submit"><i class="icon-ok bigger-110"></i> <?php echo Lang::t('Submit request') ?></button>
                &nbsp; &nbsp; &nbsp;
                <a class="btn btn-sm" href="<?php echo Controller::getReturnUrl($this->createUrl('index')) ?>"><i class="icon-remove bigger-110"></i><?php echo Lang::t('Cancel') ?></a>
       </div>
</div>

    </div>
    </div>
     
 </div>
     
     
<?php $this->endWidget(); ?>
<?php
Yii::app()->clientScript
        ->registerCssFile(Yii::app()->theme->baseUrl . '/plugins/chosen/chosen.min.css')
        ->registerScriptFile(Yii::app()->theme->baseUrl . '/plugins/chosen/chosen.jquery.min.js', CClientScript::POS_END)
        ->registerScript('Accounts[Hod]', "$('.chosen-select').chosen();");
?>