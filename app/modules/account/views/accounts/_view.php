<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <i class="fa fa-anchor"></i> <?php echo Lang::t('Account Credit Details [' .Accounts::model()->get($id , "AccountName"). ']') ?>

        </h4>
    </div>
    <div id="exam_info" class="panel-collapse collapse in">
        <div class="panel-body">
            <div class="detail-view">
                <?php
                $this->widget('application.components.widgets.DetailView', array(
                    'data' => $model,
                    'attributes' => array(
                        
                         array(
                            'name' => 'AccountName',
                        ),
                        
                        array(
                            'name' => 'Sender_id',
                            'type' => 'raw',
                            'value' => Sender::model()->get($model->Sender_id , "SenderID"),
                        ),
                        array(
                            'name' => 'Hod',
                            'type' => 'raw',
                            'value' => UsersView::model()->get($model->Hod , "name"),
                        ),
                        array(
                            'name' => 'HOD Email',
                            'type' => 'raw',
                            'value' => UsersView::model()->get($model->Hod , "email"),
                        ),
                        array(
                            'name' => 'Credit',
                            'type' => 'raw',
                            'value' => number_format($model->Credit , '0','.', ','),
                       
                            ),
                        
                        
                )
                    )
                        );
                ?>
            </div>
        </div>
        </div>
     
</div>

