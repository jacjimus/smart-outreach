<?php
$this->breadcrumbs = array(' Manage Departments' => '../accounts',
    $this->pageTitle,
);
?>
<div class="widget-box transparent">
  <div class="widget-header">
        <?php echo $this->renderPartial('account.views.accounts._tab', array('id' => $id)) ?>
    </div>  
    <div class="widget-header">
                <h4><?php echo CHtml::encode($this->pageTitle); ?></h4>
                <div class="widget-toolbar">
                </div>
        </div>
        <div class="widget-body widget-body-style2">
                <div class="widget-main padding-12 no-padding-left no-padding-right">
                        <div class="tab-content padding-4">
                                <?php $this->renderPartial('_view', array('model' => $model , 'id' => $id)); ?>
                        </div>
                </div>
        </div>
</div>
