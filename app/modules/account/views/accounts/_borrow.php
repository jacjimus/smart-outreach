<?php

$grid_id = 'accounts-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Lang::t('Borrow Credit from Departments'),
    'titleIcon' => null,
    'showExportButton' => false,
    'showSearch' => false,
    'createButton' => array('visible'=> false),
    'toolbarButtons' => array(),
    'showRefreshButton' => false,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        
        'columns' => array(
        
        'AccountName',
        array(
            'name' => 'Description',
            'value' => 'Common::smartWordwrap($data->Description, 70,"<br/>")',
            'type' => 'raw',
        ),
        array(
            'name' => 'Hod',
            'value' => 'UsersView::model()->get($data->Hod, "name")',
            'type' => 'raw',
        ),
        array(
            'name' => 'Credit',
             ),
        array(
            'name' => 'Status',
            'type' => 'raw',
            'value' => 'CHtml::tag("span", array("class"=>$data->Status=="Active"?"badge badge-success":"badge badge-danger"), $data->Status)',
        ),
        array(
            'class' => 'ButtonColumn',
            'template' => '{borrow}',
            'htmlOptions' => array('class' => 'text-center', 'style' => 'width: 100px;'),
            'buttons' => array(
                'borrow' => array(
                    'imageUrl' => false,
                    'label' => '<i class="icon-exchange bigger-130"></i>',
                    'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
                    'click'=>'function(){$("#previewframe").attr("src",$(this).attr("href")); $("#previewDialogue").dialog("open");  return false;}',
                    'visible' => '$this->grid->owner->showLink("' . UserResources::RES_ACCOUNT_BORROW. '","' . Acl::ACTION_UPDATE . '") AND ( $data->Credit >  Yii::app()->settings->get(Constants::CATEGORY_GENERAL, Constants::KEY_MIN_SMS_THREASHHOLD)) ?true:false',
                    'options' => array(
                        'class' => 'orange',
                        'title' => 'Borrow Credit'
                    ),
                ),
                
            )
        ),
    ),
))
);
