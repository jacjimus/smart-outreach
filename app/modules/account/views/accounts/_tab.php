
<div class="widget-toolbar no-border" style="float:left !important;">
    <ul class="nav nav-tabs my-nav">
        <li class="<?php echo $this->activeTab === 0 ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('account/accounts/update?id=' . $id ) ?>"><i class="icon-edit"></i> <?php echo Lang::t('Edit Account Details') ?></a></li>
        <li class="<?php echo $this->activeTab === 1 ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('account/accounts/sender?id=' . $id ) ?>"><i class="icon-wrench"></i> <?php echo Lang::t('Account Sender ID') ?></a></li>
        <li class="<?php echo $this->activeTab === 2 ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('account/accounts/credit?id=' . $id ) ?>"><i class="icon-folder-open"></i> <?php echo Lang::t('Account Credit Details') ?></a></li>
          </ul>
</div>
