<?php

$form = $this->beginWidget('CActiveForm', array(
    'id' => 'credit-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-vertical',
        'role' => 'form',
    )
        ));

?>
   
  
        <div class="form-group">
        <?php echo $form->labelEx($model, 'Amount', array('class' => 'col-lg-8 control-label')); ?>
        <div class="col-lg-8">
            <?php echo CHtml::error($model, 'Amount') ?>
                <?php echo $form->hiddenField($model, 'Account_id', array('value' => $id)); ?>
                <?php echo $form->numberField($model, 'Amount', array('class' => 'form-control')); ?>
                 </div>
        </div>
        
        


<div class="clearfix form-actions">
        <div class="col-lg-offset-2 col-lg-9">
                <button class="btn btn-primary" type="submit"><i class="icon-ok bigger-110"></i> <?php echo Lang::t( 'Add Credits') ?></button>
        </div>
</div>
<?php $this->endWidget(); ?>
