<?php

$form = $this->beginWidget('CActiveForm', array(
    'id' => 'immunization-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
        'role' => 'form',
    )
        ));

?>
   
  
<div class="form-group">
                <?php echo $form->labelEx($model, 'Immunname', array('class' => 'col-lg-2 control-label')); ?>
                <div class="col-lg-5">
            <?php echo $form->textField($model, 'Immunname', array('class' => 'form-control', 'maxlength' => 100 , "readonly" => $model->isNewRecord? false:true)); ?>
                <?php echo CHtml::error($model, 'Immunname') ?>
                </div>
        </div>
<div class="form-group">
                <?php echo $form->labelEx($model, 'Interval', array('class' => 'col-lg-2 control-label')); ?>
                <div class="col-lg-5">
            <?php echo $form->textField($model, 'Interval', array('style' => 'width:70px')); ?>
             <?php echo CHtml::activeDropDownList($model, 'IntDuration', array(""=>"[Select one]", "Weeks"=>"Weeks"), array('style' => 'width:150px;')); ?>
              <?php echo "FROM ". $form->dropDownList($model, 'IntAfter', Immunization::model()->getListData('Immunname' , 'Immunname' , array(""=>"[Select one]", "Birth" => "Birth" , "Today" => "Today") , "Immunname !='" . $model->Immunname ."'"), array('style' => 'width:150px;')); ?>
                        
            <?php echo CHtml::error($model, 'Interval') ?>
            <?php echo CHtml::error($model, 'IntDuration') ?>
            <?php echo CHtml::error($model, 'IntAfter') ?>
                </div>
        </div>
        
        <div class="form-group">
        <?php echo $form->labelEx($model, 'Message', array('class' => 'col-lg-2 control-label')); ?>
        <div class="col-lg-5">
            <?php echo CHtml::error($model, 'Message') ?>
                <?php echo $form->textArea($model, 'Message', array('class' => 'form-control', 'rows' => 2 , 'cols' => 10)); ?>
                 </div>
        </div>
        <div class="form-group">
        <?php echo $form->labelEx($model, 'Comments', array('class' => 'col-lg-2 control-label')); ?>
        <div class="col-lg-5">
            <?php echo CHtml::error($model, 'Comments') ?>
                <?php echo $form->textArea($model, 'Comments', array('class' => 'form-control', 'rows' => 2 , 'cols' => 10)); ?>
                 </div>
        </div>
        
        


<div class="clearfix form-actions">
        <div class="col-lg-offset-2 col-lg-9">
                <button class="btn btn-primary" type="submit"><i class="icon-ok bigger-110"></i> <?php echo Lang::t($model->isNewRecord ? 'Create Immunization' : 'Update Immunization Details') ?></button>
                &nbsp; &nbsp; &nbsp;
                <a class="btn" href="<?php echo $this->createUrl('manage') ?>"><i class="icon-remove bigger-110"></i><?php echo Lang::t('Cancel') ?></a>
        </div>
</div>
<?php $this->endWidget(); ?>
