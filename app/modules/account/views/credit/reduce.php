<?php

$form = $this->beginWidget('CActiveForm', array(
    'id' => 'credit-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-vertical',
        'role' => 'form',
    )
        ));

?>
   <div class="form-group">
                <?php echo $form->labelEx($model, 'id', array('class' => 'col-lg-8 control-label' , "label" => Lang::t("Target Account"))); ?>
              <div class="col-lg-8">
                    <?php echo $form->dropDownList($model, 'id', Accounts::model()->getListData('id' , 'AccountName' , true, "id != '$id'") , array(""=> "[Select one]" , "class" => "form-control" )) ?>
                    <?php echo CHtml::error($model, 'id') ?>
             </div>
</div>
  
        <div class="form-group">
        <?php echo $form->labelEx($model, 'Amount', array('class' => 'col-lg-8 control-label')); ?>
        <div class="col-lg-8">
            <?php echo CHtml::error($model, 'Amount') ?>
               <?php echo $form->numberField($model, 'Amount', array('class' => 'form-control')); ?>
                 </div>
        </div>
        
        


<div class="clearfix form-actions">
        <div class="col-lg-offset-2 col-lg-9">
                <button class="btn btn-primary" type="submit"><i class="icon-ok bigger-110"></i> <?php echo Lang::t( 'Transfer Credits') ?></button>
        </div>
</div>
<?php $this->endWidget(); ?>
