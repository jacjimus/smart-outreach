<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <i class="fa fa-anchor"></i> <?php echo Lang::t('Patient Details') ?>
<?php if (Controller::showlink($this->resource , Acl::ACTION_UPDATE)): ?>
                                <span><a class="pull-right white" href="<?php echo $this->createUrl('update', array('id' => $model->Immunid)) ?>"><i class="fa fa-edit"></i> <?php echo Lang::t('Edit Immunization Details') ?></a></span>
                        <?php endif; ?>
        </h4>
    </div>
    <div id="exam_info" class="panel-collapse collapse in">
        <div class="panel-body">
            <div class="detail-view">
                <?php
                $this->widget('application.components.widgets.DetailView', array(
                    'data' => $model,
                    'attributes' => array(
                        array(
                            'name' => 'Immunname',
                        ),
                        array(
                            'name' => 'Interval',
                        ),
                        array(
                            'name' => 'IntDuration',
                        ),
                        array(
                            'name' => 'IntAfter',
                        ),
                        array(
                            'name' => 'Message',
                        ),
                        array(
                            'name' => 'Comments',
                        ),
                        
                )
                    )
                        );
                ?>
            </div>
        </div>
        </div>
     
</div>

