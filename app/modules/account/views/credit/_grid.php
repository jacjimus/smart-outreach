
<?php

$grid_id = 'credit-account-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Lang::t('Manage Accounts Credits'),
    'titleIcon' => null,
    'showExportButton' => false,
    'showSearch' => true,
    'createButton' => array('visible'=> false),
    'toolbarButtons' => array(),
    'showRefreshButton' => false,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        
        'columns' => array(
        array(
            'name' => "AccountName" ),
         
        array(
            'name' => 'Description',
            ),
        
        array(
            'name' => 'Credit',
            'type' => 'raw',
            'value' => 'number_format($data->Credit, 0, "." , ",")'
            ),
        
       array(
            'name' => 'Status',
            'type' => 'raw',
            'value' => 'CHtml::tag("span", array("class"=>$data->Status=="Active"?"badge badge-success":"badge badge-danger"), $data->Status)',
        ),
        
        array(
            'class' => 'ButtonColumn',
            'template' => '{add}&nbsp;&nbsp;{reduce}&nbsp;&nbsp;',
            'htmlOptions' => array('class' => 'text-center', 'style' => 'width: 100px;'),
            'buttons' => array(
                'add' => array(
                    'imageUrl' => false,
                    'label' => '<i class="icon-plus-sign bigger-130"></i>',
                    'url' => 'Yii::app()->controller->createUrl("add",array("id"=>$data->primaryKey,"asDialog"=>1,"gridId"=>$this->grid->id))',
                    'click'=>'function(){$("#add-frame").attr("src",$(this).attr("href")); $("#addCredit").dialog("open");  return false;}',
                    'visible' => '$this->grid->owner->showLink("' . UserResources::RES_ACCOUNTS_USERS. '","' . Acl::ACTION_UPDATE . '")?true:false',
                    'options' => array(
                        'class' => 'green',
                        'title' => 'Add Credits',
                    ),
                ),
                'reduce' => array(
                    'imageUrl' => false,
                    'label' => '<i class="icon-minus-sign bigger-130"></i>',
                    'url' => 'Yii::app()->controller->createUrl("reduce",array("id"=>$data->primaryKey,"asDialog"=>1,"gridId"=>$this->grid->id))',
                    'click'=>'function(){$("#reduce-frame").attr("src",$(this).attr("href")); $("#reduceCredit").dialog("open");  return false;}',
                    'visible' => '$this->grid->owner->showLink("' . UserResources::RES_ACCOUNTS_USERS. '","' . Acl::ACTION_UPDATE . '")?true:false',
                    'options' => array(
                        'class' => 'red',
                        'title' => 'Transfer Credits',
                    ),
                ),
            )
        ),
    ),
)
)
)
;
?>


<?php
// Diaogue for adding new custom report
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'addCredit',
    'options' => array(
        'title' => 'Add Credits',
        'autoOpen' => false,
        'modal' => false,
        'width' => 350,
        'height' => 300,
    ),
));
?>
<iframe id="add-frame" width="100%" height="100%"></iframe>
<?php $this->endWidget(); ?>

<?php
// Diaogue for adding new custom report
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'reduceCredit',
    'options' => array(
        'title' => 'Reduce Credits',
        'autoOpen' => false,
        'modal' => false,
        'width' => 450,
        'height' => 500,
    ),
));
?>
<iframe id="reduce-frame" width="100%" height="100%"></iframe>
<?php $this->endWidget(); ?>
         