<?php

/**
 * This is the model class for table "user_roles".
 *
 * The followings are the available columns in table 'user_roles':
 * @property integer $id
 * @property integer $Account_id
 * @property string $Amount
 * @property string $UpdateDate
 *
 * The followings are the available model relations:
 * @property UserRolesOnResources[] $userRolesOnResources
 */
class Credit extends ActiveRecord implements IMyActiveSearch {

    
    public $id;
        /**
         * Returns the static model of the specified AR class.
         * @param string $className active record class name.
         * @return UserRoles the static model class
         */
        public static function model($className = __CLASS__)
        {
                return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName()
        {
                return 'tbl_acc_credits';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules()
        {
                return array(
                    array('Amount, Account_id', 'required', 'message' => '{attribute} is required'),
                    array('Amount', 'numerical', 'integerOnly' => true),
                   // array('Amount', 'exceeds', 'message' => '{attribute} exceed the Total amount asigned'),
                    array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
                );
        }

        /**
         * @return array relational rules.
         */
        public function relations()
        {
                return array(
                   // 'userRolesOnResources' => array(self::HAS_MANY, 'UserRolesOnResources', 'role_id'),
                );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels()
        {
                return array(
                    'id' => Lang::t('ID'),
                    'Amount' => Lang::t('Credit Amount'),
                    'Account_id' => Lang::t('Account'),
                    'UpdateDate' => Lang::t('Update Date'),
                    
                );
        }

        public function searchParams()
        {
                return array(
                    array('Amount', self::SEARCH_FIELD, true),
                    );
        }

}
