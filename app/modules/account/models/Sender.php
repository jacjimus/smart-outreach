<?php

/**
 * This is the model class for table "user_roles".
 *
 * The followings are the available columns in table 'user_roles':
 * @property integer $id
 * @property integer $SenderID
 * @property string $Username
 * @property string $Password
 * @property string $ApiKey
 * @property string $Status
 *
 * The followings are the available model relations:
 * @property UserRolesOnResources[] $userRolesOnResources
 */
class Sender extends ActiveRecord implements IMyActiveSearch {

    
    const STATUS_INACTIVE = "Inactive";
    const STATUS_ACTIVE = "Active";
     /**
         * Returns the static model of the specified AR class.
         * @param string $className active record class name.
         * @return UserRoles the static model class
         */
        public static function model($className = __CLASS__)
        {
                return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName()
        {
                return 'tbl_acc_senders';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules()
        {
                return array(
                    array('SenderID , Status , Username, Password, ApiKey', 'required', 'message' => '{attribute} is required'),
                    array('SenderID', 'length', 'max' => 11),
                    array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
                );
        }

        /**
         * @return array relational rules.
         */
        public function relations()
        {
                return array(
                   // 'userRolesOnResources' => array(self::HAS_MANY, 'UserRolesOnResources', 'role_id'),
                );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels()
        {
                return array(
                    'id' => Lang::t('ID'),
                    'SenderID' => Lang::t('Sender ID'),
                    'Username' => Lang::t('Username'),
                    'Password' => Lang::t('Password'),
                    'ApiKey' => Lang::t('Api Key'),
                    'Status' => Lang::t('Status'),
                    );
        }

        public function searchParams()
        {
                return array(
                    array('SenderID', self::SEARCH_FIELD, true),
                    array('Username', self::SEARCH_FIELD, true),
                    array('ApiKey', self::SEARCH_FIELD, true),
                    array('Status', self::SEARCH_FIELD, true),
                       
                );
        }

}
