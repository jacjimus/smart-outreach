<?php

/**
 * This is the model class for table "user_roles".
 *
 * The followings are the available columns in table 'user_roles':
 * @property integer $id
 * @property integer $AccountName
 * @property string $Status
 * @property string $Description
 * @property string $Sender_id
 * @property string $Credit
 * @property string $Hod
 *
 * The followings are the available model relations:
 * @property UserRolesOnResources[] $userRolesOnResources
 */
class Accounts extends ActiveRecord implements IMyActiveSearch {

    
    
        const USER_VENDOR_ENGINEER = "ENGINEER";
        const USER_COUNTY_ADMIN = "SUPERADMIN";
        const USER_DEPARTMENTAL_ADMIN = "ADMIN";
        const USER_LEVEL_NORMAL = "NORMAL";
        const PURCHASE_OK = "Active";
        const PURCHASE_INACTIVE = "Inactive";
        
    /**
         * Holds the account id.
         * @var type
         */
    public $id;
    
     const BASE_DIR = '';
    /**
         * Holds the Amount
         * @var type
         */
    public $Amount;
    public $Account_logo;
    public $purchase_status;
    
    /**
         * Holds the temp file for profile image if file upload via ajax.
         * @var type
         */
        public $temp_profile_image;
        
        /**
         * Returns the static model of the specified AR class.
         * @param string $className active record class name.
         * @return UserRoles the static model class
         */
        public static function model($className = __CLASS__)
        {
                return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName()
        {
                return 'tbl_accounts';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules()
        {
                return array(
                    array('AccountName, Status, Sender_id, Hod', 'required', 'message' => '{attribute} is required'),
                    array('AccountName', 'length', 'max' => 50),
                    array('Description, Account_logo', 'length', 'max' => 100),
                    array('Hod', 'unique', 'message' => "{attribute} is in charge of another Department"),
                    array('Sender_id, Credit', 'numerical', 'integerOnly' => true),
                    array('AccountName', 'unique', 'message' => '{attribute} has already been registered'),
                    array('AccountName, last_modified,temp_profile_image,Credit , purchase_status', 'safe'),
                    array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
                );
        }

        /**
         * @return array relational rules.
         */
        public function relations()
        {
                return array(
                   // 'userRolesOnResources' => array(self::HAS_MANY, 'UserRolesOnResources', 'role_id'),
                );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels()
        {
                return array(
                    'id' => Lang::t('ID'),
                    'AccountName' => Lang::t('Department Name'),
                    'status' => Lang::t('Status'),
                    'description' => Lang::t('Description'),
                    'Sender_id' => Lang::t('Sender ID'),
                    'Credit' => Lang::t('Remaining Credit'),
                    'Account_logo' => Lang::t('Department Logo'),
                    'Hod' => Lang::t('Department Head'),
                    'purchase_status' => Lang::t('Purchase Status'),
                    
                );
        }

        public function searchParams()
        {
                return array(
                    array('AccountName', self::SEARCH_FIELD, true),
                    array('Description', self::SEARCH_FIELD, true),
                   
                );
        }
        
         public function beforeSave()
        {
             //var_dump($this->temp_profile_image);die;
             $this->setProfileImage($this->temp_profile_image);
                return parent::beforeSave();
        }
        
        /**
         * Get the dir of a account
         * @param string $id
         */
        public function getDir($id = null)
        {
                if (empty($id))
                        $id = $this->id;
                return Common::createDir($this->getBaseDir() . DS . "accounts". DS.  $id);
        }

        public function getBaseDir()
        {
                return Common::createDir(PUBLIC_DIR);
        }

        public function getProfileImagePath($id = null)
        {
                if (!empty($this->id))
                        $id = $this->AccountName;
                $logo = !empty($this->id) ? $this->Account_logo : $this->get($id, 'Account_logo');
                if (!empty($logo)) {
                        $file_path = $this->getDir($id) . DS . $logo;

                        if (file_exists($file_path))
                                return $file_path;
                }
                return $this->getDefaultProfileImagePath();
        }

        protected function getDefaultProfileImagePath()
        {
                return   Yii::app()->baseUrl . DS . "public". DS . "accounts" . DS . "Main" . DS . Yii::app()->settings->get(Constants::CATEGORY_GENERAL, Constants::KEY_LOGO);
                              
        }

        public function setProfileImage($field , $here = true)
        {
                //using fineuploader
           
                if (!empty($field)) {
                        $temp_file = Common::parseFilePath($field);
                        //var_dump($temp_file);die;
                        $file_name = $temp_file['name'];
                        $temp_dir = $temp_file['dir'];
                        $name = $here == true ? $this->AccountName : "Main";
                         if (@copy($field, $this->getDir($name) . DS . $file_name)) {
                                if (!empty($temp_dir))
                                        Common::deleteDir($temp_dir);
                                if($here)
                                $this->Account_logo = $file_name;
                            else {
                            return $file_name;    
                            }
                                
                                $field = null;
                        }
                }
        }

       
public function getFetchCondition()
        {
                $condition = '';
                switch (UserRoles::model()->get(Yii::app()->user->role , "alias")) {
                        case self::USER_VENDOR_ENGINEER:
                                $condition .= "";
                                break;
                        case self::USER_COUNTY_ADMIN:
                                $condition .= "";
                            break;
                        case self::USER_DEPARTMENTAL_ADMIN:
                                $condition .= '  (`id`="' . Yii::app()->user->account . '")';
                                break;
                        default :
                               $condition .= '  (`id`="' . Yii::app()->user->account . '")';
                }

                return $condition;
        }
        
        protected function getRoleID($alias)
        {
             return UserRoles::model()->getScaler('id' , "alias LIKE '". $alias ."'");
        }
}
