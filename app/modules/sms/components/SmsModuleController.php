<?php

/**
 *
 * @author James Makau <jacjimus@gmail.com>
 */
class SmsModuleController extends Controller {

        const MENU_QUICK = 'Quick';
        const MENU_CUSTOME = 'Custome';
        const MENU_FILE2SMS = 'File2sms';
        const MENU_INBOX = 'Inbox';
        const MENU_VERIFY = 'Verify';
        const MENU_APPROVE = 'Approve';
        const MENU_LOGS = 'Logs';
        const MENU_PHONEBOOK = 'Phonebook';
        const MENU_E_VERIFY = 'E-Verify';
        
        public function init()
        {
                parent::init();
        }
        

}
