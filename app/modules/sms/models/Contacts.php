<?php

/**
 * This is the model class for table "tbl_phone_numbers".
 *
 * The followings are the available columns in table 'tbl_phone_numbers':
 * @property integer $PhoneID
 * @property integer $PhoneNumber
 * @property string $PhoneBookID
 * @property string $FirstName
 * @property integer $IdNo
 * @property string $Surname
 * @property string $Datetime
 
 */
class Contacts extends ActiveRecord implements IMyActiveSearch {
        // SMS Notification Status
       const STATUS_ACTIVE = "Active";
       const STATUS_INACTIVE = "Inactive";
       
       /**
         * Returns the static model of the specified AR class.
         * @param string $className active record class name.
         * @return SettingsEmailTemplate the static model class
         */
        public static function model($className = __CLASS__)
        {
                return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName()
        {
                return 'tbl_phone_numbers';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules()
        {

                return array(
                     array('PhoneNumber, csv_file', 'required', 'message' => "{attribute} is required"), 
                     array('PhoneNumber', 'length', 'max' => 12),
                     array('FirstName, Surname', 'length', 'max' => 30),
                     array('PhoneBookID, IdNo ', 'safe'),
                     array('PhoneID,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
                    );
        }
        
        /**
         * Whether the logged in user can update a given user
         * @param type $controller
         * @return type
         */
        public function canBeModified($controller, $type = Acl::ACTION_UPDATE)
        {
                 return true;
        }
        /**
         * @return array relational rules.
         */
        public function relations()
        {
                return array(
                );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels()
        {
              return array(
                    'PhoneID' => Lang::t('ID'),
                    'PhoneNumber' => Lang::t('Phone number'),
                    'FirstName' => Lang::t('First name'),
                    'Surname' => Lang::t('Surname'),
                    'IdNo' => Lang::t('ID number'),
                    'PhoneBookID' => Lang::t('Group'),
                    'Status' => Lang::t('Status'),
                    
                );
        }

        public function searchParams()
        {
                return array(
                    array('PhoneNumber', self::SEARCH_FIELD, true, 'OR'),
                    array('FirstName', self::SEARCH_FIELD, true, 'OR'),
                    array('Surname', self::SEARCH_FIELD, true, 'OR'),
                    
                );
        }
        
        public function getName($phone)
        {
            $name = self::model()->getScaler("CONCAT(FirstName , ' ', Surname)" , "PhoneNumber LIKE '" . $phone. "'");
            if($name)
                return $name;
            return $phone;
        }

     

}

