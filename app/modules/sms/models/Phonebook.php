<?php

/**
 * This is the model class for table "tbl_phonebook".
 *
 * The followings are the available columns in table 'tbl_phonebook':
 * @property integer $PbID
 * @property string $GroupName
 * @property string $Datetime
 * @property string $UserCreated
 * @property string $csv_file
 * @property string $Status
 
 */
class Phonebook extends ActiveRecord implements IMyActiveSearch {
        // SMS Notification Status
       const STATUS_ACTIVE = "Active";
       const STATUS_INACTIVE = "Inactive";
       
      
       //Reports variables
      
       public $csv_file;
       public $contacts;
      
       /**
         * Returns the static model of the specified AR class.
         * @param string $className active record class name.
         * @return SettingsEmailTemplate the static model class
         */
        public static function model($className = __CLASS__)
        {
                return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName()
        {
                return 'tbl_phonebook';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules()
        {

                return array(
                     array('GroupName', 'required', 'message' => "{attribute} is required"), 
                     array('GroupName', 'length', 'max' => 30),
                     array('csv_file', 'file', 'types' => 'csv', 'maxSize'=>5242880, 'allowEmpty' => false, 'wrongType'=>'Only csv allowed.', 'tooLarge'=>'File too large! 5MB is the limit'),
                     array('Datetime, UserCreated', 'safe'),
                     array('PbID,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
                    
                    );
        }
        
        public function afterFind() {
            $this->contacts = Contacts::model()->count("PhoneBookID = '$this->PbID'");
            parent::afterFind();
        }


        
        /**
         * @return array relational rules.
         */
        public function relations()
        {
                return array(
                );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels()
        {
              return array(
                    'PbID' => Lang::t('ID'),
                    'GroupName' => Lang::t('Group Name'),
                    'Datetime' => Lang::t('Timestamp'),
                    'UserCreated' => Lang::t('User Created'),
                    'csv_file' => Lang::t('CSV file'),
                    'Status' => Lang::t('Status'),
                    
                );
        }

        public function searchParams()
        {
                return array(
                    array('GroupName', self::SEARCH_FIELD, true, 'OR'),
                    array('UserCreated', self::SEARCH_FIELD, true, 'OR'),
                    array('Datetime', self::SEARCH_FIELD, true, 'OR'),
                    array('Status', self::SEARCH_FIELD, true, 'OR'),
                    
                );
        }

     

}

