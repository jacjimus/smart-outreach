<?php

/**
 * This is the model class for table "tbl_antenatal".
 *
 * The followings are the available columns in table 'tbl_antenatal':
 * @property integer $SmsID
 * @property string $Recepient
 * @property string $Body
 * @property string $SenderID
 * @property string $Status
 * @property string $Datetime
 * @property string $Smsnotifystatus
 * @property string $Status
 * @property string $Batch
 * @property string $Sender_account
 * @property string $SmsType
 * @property string $Schedule
 * @property string $Time
 
 */
class File2sms extends ActiveRecord implements IMyActiveSearch {
        // SMS Notification Status
       const STATUS_PENDING = "Pending";
       const STATUS_APPROVED = "Approved";
       const STATUS_REVIEWED = "Reviewed";
       const STATUS_RETURNED = "Returned";
       const STATUS_SEND = "Send";
       
       
       /**
         * Returns the static model of the specified AR class.
         * @param string $className active record class name.
         * @return SettingsEmailTemplate the static model class
         */
        public static function model($className = __CLASS__)
        {
                return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName()
        {
                return 'tbl_file2sms';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules()
        {

                return array(
                     array('Data, Smsid ', 'required', 'message' => "{attribute} is required"), 
                     array('Data', 'filter', 'filter'=>'trim'),
                     array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
                    );
        }
        
        
        
        
        /**
         * @return array relational rules.
         */
        public function relations()
        {
                return array(
                );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels()
        {
              return array(
                    'id' => Lang::t('ID'),
                    'Data' => Lang::t('Data'),
                    'Smsid' => Lang::t('Sms ID'),
                    'Status' => Lang::t('Status'),
                    
                    
                );
        }

        public function searchParams()
        {
                return array(
                    array('Smsid', self::SEARCH_FIELD, true, 'OR'),
                    
                    
                );
        }

        public static function body($sms , $grpid)
        {
            $return['to'] = '';
            $return['sms'] = '';
             $phones = Yii::app()->db->createCommand("SELECT * From tbl_phone_numbers WHERE PhoneBookID = $grpid LIMIT 1")->queryAll();
             if($phones):
             foreach ($phones As $p){
                        $body = $sms;
                        $columns = Yii::app()->db->schema->getTable('tbl_phone_numbers')->columns;
                        foreach ($columns as $col){
                            $body = preg_replace('/' . preg_quote('{'.$col->name.'}') .  '/', $p[$col->name], $body);
                        }
                         $return['to'] = $p['PhoneNumber'];
                         $return['sms'] = $body;
                        
        }
                        endif;
                       
                        return $return;
        }
        public static function processSms($sms , $id)
        {
            $return['to'] = '';
            $return['sms'] = '';
             $phones = Yii::app()->db->createCommand("SELECT * From tbl_phone_numbers WHERE PhoneNumber = $id LIMIT 1")->queryAll();
             if($phones):
             foreach ($phones As $p){
                        $body = $sms;
                        $columns = Yii::app()->db->schema->getTable('tbl_phone_numbers')->columns;
                        foreach ($columns as $col){
                            $body = preg_replace('/' . preg_quote('{'.$col->name.'}') .  '/', $p[$col->name], $body);
                        }
                         $return['to'] = $p['PhoneNumber'];
                         $return['sms'] = $body;
                        
        }
                        endif;
                       
                        return $return;
        }


//        public function getName($id)
//        {
//                if (empty($id))
//                        return NULL;
//                $name = SmsGroup::model()->getScaler('group_name', '`id`=:t1 ', array(':t1' => $id));
//                return !empty($name) ? $name : NULL;
//        }

}

