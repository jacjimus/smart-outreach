<?php

/**
 * This is the model class for table "tbl_antenatal".
 * 
 * The followings are the available columns in table 'tbl_antenatal':
 * @property integer $Smsid
 * @property string $Smsmessage
 * @property string $Datetime
 * @property string $Deliverystatus
 * @property string $Motherid
 * @property string $Caretype
 * @property string $Careid

 */
class Smslogs extends ActiveRecord implements IMyActiveSearch {
        // SMS Notification Status
       const STATUS_PENDING = "Pending";
       const STATUS_DELIVERED = "Success";
       
       // Other Message Status
       const  SEND_ERROR = "Send Error";
       const  NOT_ENOUGH_CREDITS = "Insufficient Balance";
       const  NETWORK_NOT_COVERED = "Network Unknown";
       const  INVALID_DETAILS = "Invalid Logins";
       const  CUMMUNICATION_ERROR = "Communication Error";
       const  INSUFFICIENT_CREDIT = "Insufficient Credit";
       const  ERROR_PROCESSING = "General Error";
       
       // Antenatal Care Status
       
       const SEARCH_FIELD = '_search';
      

        /**
         * Returns the static model of the specified AR class.
         * @param string $className active record class name.
         * @return SettingsEmailTemplate the static model class
         */
        public static function model($className = __CLASS__)
        {
                return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName()
        {
                return 'tbl_sms_logs';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules()
        {

               return array(
                     array('Recepient, Body ', 'required', 'message' => "{attribute} is required"), 
                     array('Body', 'length', 'max' => 160),
                     array('Body, Body', 'filter', 'filter'=>'trim'),
                    // array('Recepient', 'unique', 'message' => "Please remove duplicates from {attribute}"),
                     //array('Recepient','match','pattern'=>'/^[2547][1-9]\d{0,3}[\.]{1}[\d\s]+$/' , 'message' => '{attribute} should be in the format 2547*******,2547*******'),
                     array('Recepient, SenderID, Batch, Sender_account, User_account SmsType, Datetime, Deliverytime', 'safe'),
                     array('SmsID,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
                    );
        }
        
        public function beforeSave()
        {
            if($this->isNewRecord AND !is_numeric($this->SenderID))
            $this->SenderID = Sender::model()->getScaler("id" , "SenderID LIKE '$this->SenderID'");
                return parent::beforeSave(); 
        }
        
        
        /**
         * @return array relational rules.
         */
        public function relations()
        {
                return array(
                );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels()
        {
              return array(
                    'SmsID' => Lang::t('ID'),
                    'Recepient' => Lang::t('Recepient(s)'),
                    'SenderID' => Lang::t('Sender'),
                    'Body' => Lang::t('Body'),
                    'Datetime' => Lang::t('Timestamp'),
                    'Status' => Lang::t('Status'),
                    'Batch' => Lang::t('Batch No'),
                    'No' => Lang::t('SMS Count'),
                    'SmsType' => Lang::t('SMS Type'),
                    'User_account' => Lang::t('User Account'),
                    'Deliverytime' => Lang::t('Delivery time'),
                   
                );
        }

        public function searchParams()
        {
                return array(
                    array('Recepient', self::SEARCH_FIELD, true, 'OR'),
                    array('SenderID', self::SEARCH_FIELD, true, 'OR'),
                    array('Body', self::SEARCH_FIELD, true, 'OR'),
                    array('Datetime', self::SEARCH_FIELD, true, 'OR'),
                    array('Status', self::SEARCH_FIELD, true, 'OR'),
                    array('Batch', self::SEARCH_FIELD, true, 'OR'),
                    array('Deliverytime', self::SEARCH_FIELD, true, 'OR'),
                    
                    
                );
        }
        
         public function updateProgress($id, $data)
        {
                
                
                $model = Smslogs::model()->loadModel($id);
                $model->Status = $data['Status'];
                $model->Deliverytime = $data['Deliverytime'];
                $model->save(false);
//                        $credit_update = Accounts::model()->loadModel($sms->Sender_account);
//
//                            $credit_update->Credit-= 1;
//                            $credit_update->save(false);
        }

  

}

