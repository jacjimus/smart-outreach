<?php

/**
 * This is the model class for table "tbl_antenatal".
 *
 * The followings are the available columns in table 'tbl_antenatal':
 * @property integer $SmsID
 * @property string $Recepient
 * @property string $Body
 * @property string $SenderID
 * @property string $Status
 * @property string $Datetime
 * @property string $Smsnotifystatus
 * @property string $Status
 * @property string $Batch
 * @property string $Sender_account
 * @property string $SmsType
 * @property string $Schedule
 * @property string $Time
 * @property string $Time
 * @property string $Peer_reviewer
 
 */
class Eaccounts extends ActiveRecord implements IMyActiveSearch {
       
       const E_VERIFY = 'E_VERIFY';
       const INBOUND = 'INBOUND';
      
       public $duration;

      
       /**
         * Returns the static model of the specified AR class.
         * @param string $className active record class name.
         * @return SettingsEmailTemplate the static model class
         */
        public static function model($className = __CLASS__)
        {
                return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName()
        {
                return 'tbl_e_verify_accounts';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules()
        {

                return array(
                     array('account_name, keyword', 'required', 'message' => "{attribute} is required"), 
                     array('keyword', 'filter', 'filter'=>'trim'),
                     array('status, data_table ', 'safe'),
                     array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
                    );
        }
        
        
        /**
         * @return array relational rules.
         */
        public function relations()
        {
                return array(
                );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels()
        {
              return array(
                    'id' => Lang::t('ID'),
                    'account_name' => Lang::t('Account Name'),
                    'data_table' => Lang::t('Data table'),
                    'keyword' => Lang::t('Keyword'),
                    'date_created' => Lang::t('Timestamp'),
                    'status' => Lang::t('Status'),
                    
                );
        }

        public function searchParams()
        {
                return array(
                    array('account_name', self::SEARCH_FIELD, true, 'OR'),
                    array('keyword', self::SEARCH_FIELD, true, 'OR'),
                   
                );
        }

       


//        public function getName($id)
//        {
//                if (empty($id))
//                        return NULL;
//                $name = SmsGroup::model()->getScaler('group_name', '`id`=:t1 ', array(':t1' => $id));
//                return !empty($name) ? $name : NULL;
//        }

}

