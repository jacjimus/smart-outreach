<?php

/**
 * This is the model class for table "tbl_custome_sms".
 *
 * The followings are the available columns in table 'tbl_custome_sms':
 * @property integer $id
 * @property string $Body
 * @property string $Datetime
 * @property string $SmsCount
 * @property string $Batch
 * @property string $Status
 * @property string $SenderID
 * @property interger $Recepient
 * @property interger $Sender_account
 * @property interger $User_account
 
 */
class Comments extends ActiveRecord implements IMyActiveSearch {
       
    
    public $Action;
    
    public static function model($className = __CLASS__)
        {
                return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName()
        {
                return 'tbl_sms_comments';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules()
        {

               return array(
                     array('Comments', 'required', "on"=>"Reject", 'message' => " Please enter {attribute} for rejecting SMS"), 
                     array('Comments', 'length', 'max' => 255),
                     array('Stage, SmsID, Stage, User_account', 'safe'),
                     array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
                    );
                    
        }
        
        
        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels()
        {
              return array(
                    'SmsID' => Lang::t('ID'),
                    'Comments' => Lang::t('Comments'),
                    'Datetime' => Lang::t('Timestamp'),
                    'User_account' => Lang::t('User Account'),
                   
                   
                );
        }

        public function searchParams()
        {
                return array(
                    array('SmsCount', self::SEARCH_FIELD, true, 'OR'),
                    array('Datetime', self::SEARCH_FIELD, true, 'OR'),
                    array('Batch', self::SEARCH_FIELD, true, 'OR'),
                    array('Body', self::SEARCH_FIELD, true, 'OR'),
                    array('Status', self::SEARCH_FIELD, true, 'OR'),
                    array('SenderID', self::SEARCH_FIELD, true, 'OR'),
                    array('Recepient', self::SEARCH_FIELD, true, 'OR'),
                    array('User_account', self::SEARCH_FIELD, true, 'OR'),
                    
                    
                );
        }

       

}

