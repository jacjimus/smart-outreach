<?php

/**
 * This is the model class for table "tbl_custome_sms".
 *
 * The followings are the available columns in table 'tbl_custome_sms':
 * @property integer $id
 * @property string $Body
 * @property string $SendDate
 * @property string $SmsCount
 * @property string $Batch
 * @property string $Status
 * @property string $SenderID
 * @property interger $Recepient
 * @property interger $Sender_account
 
 */
class Notification extends ActiveRecord implements IMyActiveSearch {
     
    
    const STATUS_UNREAD = "Unread";
    const STATUS_READ = "Read";
    
    public $Action;
    public $Period;
    
    public static function model($className = __CLASS__)
        {
                return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName()
        {
                return 'tbl_notifications';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules()
        {

               return array(
                     array('Message, Destination', 'required', 'message' => "{attribute} is required"), 
                     array('Message', 'length', 'max' => 255),
                     array('Status, User_account, Commentid', 'safe'),
                     array('Id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
                    );
                    
        }
        
        
        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels()
        {
              return array(
                    'Id' => Lang::t('ID'),
                    'Message' => Lang::t('Message'),
                    'User_account' => Lang::t('From'),
                    'Status' => Lang::t('Status'),
                    'Destination' => Lang::t('Recepient'),
                    'Commentid' => Lang::t('Review Comments'),
                    'Period' => Lang::t('Time'),
                    'Datetime' => Lang::t('Time'),
                   
                   
                );
        }
        
        public function afterFind() {
            $this->Period = self::dateDiff($this->Datetime , date("Y-m-d h:i:s"));
            parent::afterFind();
        }

        
        public static function dateDiff($time1, $time2, $precision = 6) {
    // If not numeric then convert texts to unix timestamps
           date_default_timezone_set("Africa/Nairobi");
           if($time2 === null)
               $time2 = date('Y-m-d h:i:s');
    if (!is_int($time1)) {
      $time1 = strtotime($time1);
    }
    if (!is_int($time2)) {
      $time2 = strtotime($time2);
    }
 
    // If time1 is bigger than time2
    // Then swap time1 and time2
    if ($time1 > $time2) {
      $ttime = $time1;
      $time1 = $time2;
      $time2 = $ttime;
    }
 
    // Set up intervals and diffs arrays
    $intervals = array('day','hour','minute','second');
    $diffs = array();
 
    // Loop thru all intervals
    foreach ($intervals as $interval) {
      // Create temp time from time1 and interval
      $ttime = strtotime('+1 ' . $interval, $time1);
      // Set initial values
      $add = 1;
      $looped = 0;
      // Loop until temp time is smaller than time2
      while ($time2 >= $ttime) {
        // Create new temp time from time1 and interval
        $add++;
        $ttime = strtotime("+" . $add . " " . $interval, $time1);
        $looped++;
      }
      $time1 = strtotime("+" . $looped . " " . $interval, $time1);
      $diffs[$interval] = $looped;
    }
    
    $count = 0;
    $times = array();
    // Loop thru all diffs
    foreach ($diffs as $interval => $value) {
      // Break if we have needed precission
      if ($count >= $precision) {
break;
      }
      // Add value and interval
      // if value is bigger than 0
      if ($value > 0) {
// Add s if value is not 1
if ($value != 1) {
  $interval .= "s";
}
// Add value and interval to times array
$times[] = $value . " " . $interval;
$count++;
      }
    }
 
    // Return string with times
    return implode(", ", $times);
  }
        public function searchParams()
        {
                return array(
                    array('Message', self::SEARCH_FIELD, true, 'OR'),
                    array('User_account', self::SEARCH_FIELD, true, 'OR'),
                    array('Status', self::SEARCH_FIELD, true, 'OR'),
                    array('Destination', self::SEARCH_FIELD, true, 'OR'),
                   
                    
                );
        }

       

}

