<?php

/**
 * This is the model class for table "tbl_antenatal".
 *
 * The followings are the available columns in table 'tbl_antenatal':
 * @property integer $SmsID
 * @property string $Recepient
 * @property string $Body
 * @property string $SenderID
 * @property string $Status
 * @property string $Datetime
 * @property string $Smsnotifystatus
 * @property string $Status
 * @property string $Batch
 * @property string $Sender_account
 * @property string $SmsType
 * @property string $Schedule
 * @property string $Time
 * @property string $Time
 * @property string $Peer_reviewer
 
 */
class Sms extends ActiveRecord implements IMyActiveSearch {
        // SMS Notification Status
       const STATUS_PENDING = "Pending";
       const STATUS_APPROVED = "Approved";
       const STATUS_REVIEWED = "Reviewed";
       const STATUS_RETURNED = "Returned";
       const STATUS_SEND = "Send";
       
       //Sms Types
       const  QUICKSMS = "QUICK";
       const  CUSTOMESMS = "CUSTOME";
       const  FILE2SMS = "FILE2SMS";
       const  APISMS = "API";


       
       public $No;
       public $grp;
       public $class;
       public $preview;
       public $Time;
       public $Signature;
       public $Comments;
      
       /**
         * Returns the static model of the specified AR class.
         * @param string $className active record class name.
         * @return SettingsEmailTemplate the static model class
         */
        public static function model($className = __CLASS__)
        {
                return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName()
        {
                return 'tbl_raw_sms';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules()
        {

                return array(
                     array('Recepient, Body ', 'required', 'message' => "{attribute} is required"), 
                     array('Body, Comments', 'filter', 'filter'=>'trim'),
                     array('Body', 'length', 'max' => 170),
                     array('Recepient, SenderID, Batch, Sender_account, SmsType, Datetime,Schedule, Comments', 'safe'),
                     array('SmsID,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
                    );
        }
        
        public function afterfind()
        {
            $this->No = Sms::model()->get($this->Batch , "Count(SmsID) As No");
            $this->grp = $this->slpitGrp($this->Recepient);
            if($this->SmsType == self::CUSTOMESMS)
                $return = self::body($this->Body, explode(',', $this->Recepient)[0]);
            $this->preview = $this->SmsType == self::CUSTOMESMS ? $return ["sms"] : $this->Body;
            $this->class = $this->cssclass($this->Status);
           //var_dump( $return );die;
            return parent::afterfind(); 
        }
        
       
        protected function cssclass($status)
        {
            switch ($status)
            {
                case self::STATUS_PENDING:
                    return "badge-warning";
                    break;
                
                case self::STATUS_REVIEWED:
                    return "badge-success";
                    break;
                
                case self::STATUS_RETURNED:
                    return "badge-error";
                    break;
                
                case self::STATUS_APPROVED:
                    return "badge-success";
                    break;
            }
        }
        protected function slpitGrp($param) {
            
            $grp = explode(',', $param);
            $str = "";
            foreach ($grp As $r)
            {
                $str .=  Phonebook::model()->get( $r , "GroupName") . " , " ;
            }
            
            
             return rtrim($str , " , ");
        }
        
        public function beforeSave()
        {
            $this->Sender_account = Yii::app()->user->account;
            if(!is_numeric($this->SenderID)):
            $this->SenderID = Sender::model()->getScaler("id" , "SenderID LIKE '$this->SenderID'");
            
            $this->Schedule = date("Y-m-d h:i:s" , strtotime($this->Schedule. " " .substr($this->Time, 0, -2)));
            endif;
            
            return parent::beforeSave(); 
        }
        
        
        /**
         * @return array relational rules.
         */
        public function relations()
        {
                return array(
                );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels()
        {
              return array(
                    'SmsID' => Lang::t('ID'),
                    'Recepient' => Lang::t('Recepient(s)'),
                    'SenderID' => Lang::t('Sender'),
                    'Body' => Lang::t('Body'),
                    'Datetime' => Lang::t('Timestamp'),
                    'Status' => Lang::t('Status'),
                    'Batch' => Lang::t('Batch No'),
                    'No' => Lang::t('SMS Count'),
                    'SmsType' => Lang::t('SMS Type'),
                    'Schedule' => Lang::t('Day to Send'),
                    'Time' => Lang::t('Time to Send'),
                    'User_account' => Lang::t('Sms Origin'),
                    'Peer_reviewer' => Lang::t('Reviewed by'),
                    'Approver' => Lang::t('Approved by'),
                   
                );
        }

        public function searchParams()
        {
                return array(
                    array('Recepient', self::SEARCH_FIELD, true, 'OR'),
                    array('SenderID', self::SEARCH_FIELD, true, 'OR'),
                    array('Status', self::SEARCH_FIELD, true, 'OR'),
                    array('Datetime', self::SEARCH_FIELD, true, 'OR'),
                    array('Batch', self::SEARCH_FIELD, true, 'OR'),
                    array('Schedule', self::SEARCH_FIELD, true, 'OR'),
                    
                    
                );
        }

        public static function body($sms , $grpid)
        {
            $return['to'] = '';
            $return['sms'] = '';
             $phones = Yii::app()->db->createCommand("SELECT * From tbl_phone_numbers WHERE PhoneBookID = $grpid LIMIT 1")->queryAll();
             if($phones):
             foreach ($phones As $p){
                        $body = $sms;
                        $columns = Yii::app()->db->schema->getTable('tbl_phone_numbers')->columns;
                        foreach ($columns as $col){
                            $body = preg_replace('/' . preg_quote('{'.$col->name.'}') .  '/', $p[$col->name], $body);
                        }
                         $return['to'] = $p['PhoneNumber'];
                         $return['sms'] = $body;
                        
        }
                        endif;
                       
                        return $return;
        }
        public static function processSms($sms , $id)
        {
            $return['to'] = '';
            $return['sms'] = '';
             $phones = Yii::app()->db->createCommand("SELECT * From tbl_phone_numbers WHERE PhoneNumber = $id LIMIT 1")->queryAll();
             if($phones):
             foreach ($phones As $p){
                        $body = $sms;
                        $columns = Yii::app()->db->schema->getTable('tbl_phone_numbers')->columns;
                        foreach ($columns as $col){
                            $body = preg_replace('/' . preg_quote('{'.$col->name.'}') .  '/', $p[$col->name], $body);
                        }
                         $return['to'] = $p['PhoneNumber'];
                         $return['sms'] = $body;
                        
        }
                        endif;
                       
                        return $return;
        }


//        public function getName($id)
//        {
//                if (empty($id))
//                        return NULL;
//                $name = SmsGroup::model()->getScaler('group_name', '`id`=:t1 ', array(':t1' => $id));
//                return !empty($name) ? $name : NULL;
//        }

}

