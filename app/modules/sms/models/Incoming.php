<?php

/**
 * This is the model class for table "tbl_antenatal".
 *
 * The followings are the available columns in table 'tbl_antenatal':
 * @property integer $SmsID
 * @property string $Recepient
 * @property string $Body
 * @property string $SenderID
 * @property string $Status
 * @property string $Datetime
 * @property string $Smsnotifystatus
 * @property string $Status
 * @property string $Batch
 * @property string $Sender_account
 * @property string $SmsType
 * @property string $Schedule
 * @property string $Time
 * @property string $Time
 * @property string $Peer_reviewer
 
 */
class Incoming extends ActiveRecord implements IMyActiveSearch {
        // SMS Notification Status
       const STATUS_NEW = "New";
       const STATUS_READ = "Read";
       
       const E_VERIFY = 'E_VERIFY';
       const INBOUND = 'INBOUND';
      
       public $duration;

      
       /**
         * Returns the static model of the specified AR class.
         * @param string $className active record class name.
         * @return SettingsEmailTemplate the static model class
         */
        public static function model($className = __CLASS__)
        {
                return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName()
        {
                return 'tbl_incoming';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules()
        {

                return array(
                     array('sender, text, receiver ', 'required', 'message' => "{attribute} is required"), 
                     array('text', 'filter', 'filter'=>'trim'),
                     array('text, sender, type, receiver, time', 'safe'),
                     array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
                    );
        }
        
        public function afterFind() {
            $this->duration = $this->dateDiff($this->time, date("Y-m-d H:i:s"));
            return parent::afterFind();
        }
       
        
        public static function dateDiff($time1, $time2, $precision = 6) {
    // If not numeric then convert texts to unix timestamps
           date_default_timezone_set("Africa/Nairobi");
           if($time2 === null)
               $time2 = date('Y-m-d h:i:s');
    if (!is_int($time1)) {
      $time1 = strtotime($time1);
    }
    if (!is_int($time2)) {
      $time2 = strtotime($time2);
    }
 
    // If time1 is bigger than time2
    // Then swap time1 and time2
    if ($time1 > $time2) {
      $ttime = $time1;
      $time1 = $time2;
      $time2 = $ttime;
    }
 
    // Set up intervals and diffs arrays
    $intervals = array('year','month','day','hour','minute','second');
    $diffs = array();
 
    // Loop thru all intervals
    foreach ($intervals as $interval) {
      // Create temp time from time1 and interval
      $ttime = strtotime('+1 ' . $interval, $time1);
      // Set initial values
      $add = 1;
      $looped = 0;
      // Loop until temp time is smaller than time2
      while ($time2 >= $ttime) {
        // Create new temp time from time1 and interval
        $add++;
        $ttime = strtotime("+" . $add . " " . $interval, $time1);
        $looped++;
      }
      $time1 = strtotime("+" . $looped . " " . $interval, $time1);
      $diffs[$interval] = $looped;
    }
    
    $count = 0;
    $times = array();
    // Loop thru all diffs
    foreach ($diffs as $interval => $value) {
      // Break if we have needed precission
      if ($count >= $precision) {
break;
      }
      // Add value and interval
      // if value is bigger than 0
      if ($value > 0) {
// Add s if value is not 1
if ($value != 1) {
  $interval .= "s";
}
// Add value and interval to times array
$times[] = $value . " " . $interval;
$count++;
      }
    }
 
    // Return string with times
    return implode(", ", $times);
  }
        
        /**
         * @return array relational rules.
         */
        public function relations()
        {
                return array(
                );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels()
        {
              return array(
                    'id' => Lang::t('ID'),
                    'sender' => Lang::t('Sender'),
                    'receiver' => Lang::t('Receiver'),
                    'text' => Lang::t('Text'),
                    'timestamp' => Lang::t('Timestamp'),
                    'status' => Lang::t('Status'),
                    'type' => Lang::t('Sms type'),
                    
                );
        }

        public function searchParams()
        {
                return array(
                    array('sender', self::SEARCH_FIELD, true, 'OR'),
                    array('receiver', self::SEARCH_FIELD, true, 'OR'),
                    array('text', "%" . self::SEARCH_FIELD . "%", true, 'OR'),
                    array('timestamp', self::SEARCH_FIELD, true, 'OR'),
                    array('status', self::SEARCH_FIELD, true, 'OR'),
                    
                    
                );
        }

       


//        public function getName($id)
//        {
//                if (empty($id))
//                        return NULL;
//                $name = SmsGroup::model()->getScaler('group_name', '`id`=:t1 ', array(':t1' => $id));
//                return !empty($name) ? $name : NULL;
//        }

}

