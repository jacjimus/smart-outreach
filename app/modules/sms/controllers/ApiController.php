<?php

class ApiController extends SmsModuleController {

    // Members
    /**
     * Key which has to be in HTTP USERNAME and PASSWORD headers 
     */
    Const APPLICATION_ID = 'ASCCPE';
    const USERNAME = "Konvergenz";
    const PASSWORD = "1nKom1NGTraF1K";

    /**
     * Default response format
     * either 'json' or 'xml'
     */
    private $format = 'json';
    
    public $doWrite;

    /**
     * @return array action filters
     */
    public function filters() {
        return array();
    }
    
    
    public function __construct($id, $module = null) {
         $this->doWrite = 1;
         parent::__construct($id, $module);
    }

    // Actions
    public function actionXml() {

        $xml_post = file_get_contents('php://input');
        // If we receive data, save it.
        if ($xml_post) {

            $data = new SimpleXMLElement($xml_post);
            foreach ($data As $ele) {
                $username = $ele->username;
                $apiKey = $ele->apiKey;
                $id = $ele->msgID;
                $msisdn = $ele->msisdn;
                $body = $ele->body;
                $account = @isset($ele->accountID) ? $ele->accountID : null;
            }



            if ($this->doWrite == 1) {
                $myFile = "/var/www/html/logs/api.log";
                $fh = fopen($myFile, 'w') or die("can't open file");
                $req_dump = print_r($data, TRUE);
                fwrite($fh, "---------------------------------End of URL-----------------------------");
                fwrite($fh, $req_dump);
                fclose($fh);
            }
            $auth_details = Yii::app()->db->createCommand("SELECT * FROM tbl_acc_senders WHERE Username LIKE '$username' LIMIT 1")->queryAll();
            if (!empty($auth_details)):
                if ($username != $auth_details[0]['Username']):
                    $this->_sendResponse(401);
                elseif ($apiKey != $auth_details[0]['ApiKey']):
                    $this->_sendResponse(402);
                elseif ($auth_details[0]['Status'] == Sender::STATUS_INACTIVE):
                    $this->_sendResponse(403);
                else:
					$phone = $msisdn;
                    Yii::app()->user->setState('id', $auth_details[0]['id']);
                    Yii::app()->user->setState('account', $auth_details[0]['id']);
					/**
					* Remove duplicate messages based on Phone number and Message body.
					*/
					if(Smslogs::model()->exists("Body LIKE '$body' AND RIGHT(Recepient,9) LIKE '%$phone'")):
					
						$this->_sendResponse(500, "Duplicate Message", null);	
                                        else:
                                            $insert = new Smslogs;
						$insert->SenderID = $auth_details[0]['SenderID'];
						$insert->Recepient = $msisdn;
						$insert->Body = $body;
						$insert->FKey = $id;
						$insert->User_account = $account;
						$insert->Datetime = date('Y-m-d h:i:s');
						$insert->Status = Smslogs::STATUS_PENDING;
						if ($insert->save(FALSE))
							$message = Smslogs::STATUS_DELIVERED;
						$msg = $id == "" ? $insert->SmsID : $id;
						$this->_sendResponse(200, $message, $msg);
					
					endif;
                endif;
            else:
                $this->_sendResponse(400);
            endif;
        }
    }
    
    /*
     * This is the JSON api for the SMS connection and forwarding form other systems
     */

    public function actionJson() {

        $json_post = file_get_contents('php://input');
        // If we receive data, save it.
        if ($json_post) {

            $data = CJSON::decode($json_post);
            //var_dump($data);die;
            foreach ($data As $ele) {
                $username = $data['username'];
                $apiKey = $data['apiKey'];
                $msgID = $data['msgID'];
                $msisdn = $data['msisdn'];
                $body = $data['body'];
                $account = isset($data['accountID']) ? $data['accountID'] : null;
            }


            if ($this->doWrite == 1) {
                $myFile = "/var/www/html/logs/api.log";
                $fh = fopen($myFile, 'w') or die("can't open file");
                $req_dump = print_r($data, TRUE);
                fwrite($fh, "---------------------------------End of URL-----------------------------");
                fwrite($fh, $req_dump);
                fclose($fh);
            }

            $auth_details = Yii::app()->db->createCommand("SELECT * FROM tbl_acc_senders WHERE Username = '$username'")->queryAll();
            //var_dump($auth_details[0]['id']);die;
            if (!empty($auth_details)):
                if ($username != $auth_details[0]['Username']):
                    $this->_sendResponse(401);
                elseif ($apiKey != $auth_details[0]['ApiKey']):
                    $this->_sendResponse(402);
                elseif ($auth_details[0]['Status'] == Sender::STATUS_INACTIVE):
                    $this->_sendResponse(403);
                else:
				
					/**
					* Remove duplicate messages based on Phone number and Message body.
					*/
					$phone = $msisdn;
					if(Smslogs::model()->exists("Body LIKE '$body' AND RIGHT(Recepient,9) LIKE '%$phone'")):
                                            $this->_sendResponse(500, "Duplicate Message", null);
                                        else:
						Yii::app()->user->setState('id', $auth_details[0]['id']);
						Yii::app()->user->setState('account', $auth_details[0]['id']);

						$insert = new Smslogs;
						$insert->SenderID = $auth_details[0]['SenderID'];
						$insert->SmsType = Sms::APISMS;
						$insert->Recepient = $msisdn;
						$insert->Body = $body;
						$insert->FKey = $msgID;
						$insert->Datetime = date('Y-m-d h:i:s');
						$insert->Status = Smslogs::STATUS_PENDING;
						if ($insert->save(FALSE))
							$message = Smslogs::STATUS_DELIVERED;
						$msg = $msgID == "" ? $insert->SmsID : $msgID;
						$this->_sendResponse(200, $message, $msg);
						
					endif;
                endif;
            else:
                $this->_sendResponse(400);
            endif;
        }
    }

    private function _sendResponse($status = 200, $body = '', $id = null) {

        switch ($status) {
            case 400:
                $message = 'The Username / Api key doesnt exist';
                break;
            case 401:
                $message = 'The Username doesnt exist';
                break;
            case 402:
                $message = 'You used un-authorized API key';
                break;
            case 403:
                $message = 'The account is Inactive';
                break;
				
			case 500:
                $message = 'Duplicate Message';
                break;

            default:
                $message = $body;
                break;
        }
        $array = array("code" => $status, "messageID" => $id, "status" => $message);
        echo CJSON::encode($array);

        Yii::app()->end();
    }

    private function _getStatusCodeMessage($status) {
        // these could be stored in a .ini file and loaded
        // via parse_ini_file()... however, this will suffice
        // for an example
        $codes = Array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Wrong Username',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Unauthorized ApiKey',
            501 => 'Not Implemented',
        );
        return (isset($codes[$status])) ? $codes[$status] : '';
    }

    // Actions
    public function actionIncoming() {

       // $xml_post = file_get_contents('php://input');
        $xml_post = $_GET;
        // If we receive data, save it.
        if (isset($xml_post)) {
            
        if($_GET['username'] == self::USERNAME && $_GET['password'] == self::PASSWORD) :   


            if ($this->doWrite == 1) {
                $myFile = "/var/www/html/logs/incoming.log";
                $fh = fopen($myFile, 'w') or die("can't open file");
                $req_dump = print_r($xml_post, TRUE);
                fwrite($fh, "---------------------------------End of URL-----------------------------");
                fwrite($fh, $req_dump);
                fclose($fh);
            }
            
            $model = new Incoming();
            $model->attributes = $_GET;
            $model->time = date("Y-m-d H:i:s");
            $model->status = Incoming::STATUS_NEW;
            $model->type = Eaccounts::model()->exists("keyword like '" . $model->text . "%'") ? Incoming::E_VERIFY : Incoming::INBOUND;
            if($model->validate())
                $model->save();
            echo "Sms received successfully";
            else:
             echo "Invalid Partner!";   
            endif;
                   
            
            
        }
    }

}
