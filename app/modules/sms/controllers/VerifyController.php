<?php

class VerifyController extends SmsModuleController {

    public function init() {
        $this->resource = UserResources::RES_SMS_E_VERIFY;
        $this->activeMenu = self::MENU_E_VERIFY;
        $this->showPageTitle = false;
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'accounts' , 'create' , 'update' , 'delete' , 'logs','successful','failed','tabidsession','smslogs'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    
    public function actionIndex() {
        
       $this->resource = UserResources::RES_SMS_E_VERIFY;
       $this->hasPrivilege(Acl::ACTION_VIEW);
       $this->activeTab = 1;
        $this->PageTitle = "Manage E verify Accounts";
        $this->activeMenu = self::MENU_E_VERIFY;
        $this->showPageTitle = false;
       
       $this->render('everify', array('model' => Eaccounts::model()->searchModel(array(), $this->settings[Constants::KEY_PAGINATION], 'account_name'),));
   
    }
    
    /**
         * Creates a new model.
         * If creation is successful, the browser will be redirected to the 'view' page.
         */
        public function actionCreate()
        {
                $this->hasPrivilege(Acl::ACTION_CREATE);
                 $this->resourceLabel = "E-verify account";
                 $this->pageTitle = Lang::t('Add ' . $this->resourceLabel);

                $model = new Eaccounts();
                $model_class_name = $model->getClassName();

                if (isset($_POST[$model_class_name])) {
                   
                        $model->attributes = $_POST[$model_class_name];
                         if ($model->save()) {
                                Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                                $this->redirect(array('index'));
                        }
 
                }

                $this->render('create', array(
                    'model' => $model,
                ));
        }

        /**
         * Updates a particular model.
         * If update is successful, the browser will be redirected to the 'view' page.
         * @param integer $id the ID of the model to be updated
         */
        public function actionUpdate($id)
        {
                $this->hasPrivilege(Acl::ACTION_UPDATE);
                $this->resourceLabel = "E-verify account";
                $this->pageTitle = Lang::t('Edit ' . $this->resourceLabel);


                $model = Eaccounts::model()->loadModel($id);
                $model_class_name = $model->getClassName();

                if (isset($_POST[$model_class_name])) {
                        $model->attributes = $_POST[$model_class_name];
                        if ($model->save()) {
                                Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                                $this->redirect(array('index'));
                        }
                }

                $this->render('update', array(
                    'model' => $model,
                ));
        }
		
/////tad sesion id
	public function actionSmslogs(){
        $this->hasPrivilege(Acl::ACTION_VIEW);
        $this->activeTab = 0;
        $this->activeMenu = self::MENU_LOGS;
        $this->pageTitle = "View Sms Logs and Status";
        $this->showPageTitle = false;
	$searchModel = Smslogsarchive::model()->searchModel(array(), $this->settings[Constants::KEY_PAGINATION], 'Datetime DESC' , " SenderID !=2 ");
        $this->render('logs', array(
            'model' => $searchModel,
        ));	
    }


		//faide smses
		 public function actionFailed(){
			 
	     $this->hasPrivilege(Acl::ACTION_VIEW);
             $this->activeTab = 2;
             $this->activeMenu = self::MENU_LOGS;
             $this->pageTitle = "Failed Logs";
             $this->showPageTitle = false;
			 $model= Smslogs::model()->searchModel(array(), $this->settings[Constants::KEY_PAGINATION], 'Datetime Desc', "Status NOT LIKE 'Success' AND Status NOT LIKE 'Pending' AND SenderID !=2 ");
			 // var_dump($model);die;
			  $this->render('failed', array(
                    'model' => $model,
                ));
			}
		//Lists all successfull logs
		 public function actionSuccessful(){
			 
	     $this->hasPrivilege(Acl::ACTION_VIEW);
             $this->activeTab = 1;
             $this->activeMenu = self::MENU_LOGS;
             $this->pageTitle = "Successful Logs";
             $this->showPageTitle = false;
	     $model= Smslogsarchive::model()->searchModel(array(), $this->settings[Constants::KEY_PAGINATION], 'Datetime Desc', "Status LIKE 'Success' AND SenderID !=2 ");
			 // var_dump($model);die;
	     $this->render('success', array(
                    'model' => $model,
                ));
				
			}

        /**
         * Deletes a particular model.
         * If deletion is successful, the browser will be redirected to the 'admin' page.
         * @param integer $id the ID of the model to be deleted
         */
        public function actionDelete($id)
        {
                Acl::hasPrivilege($this->privileges, $this->resource, Acl::ACTION_DELETE);
                Signatures::model()->loadModel($id)->delete();
                if (!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
       
        public function actionLogs() {
        $this->hasPrivilege(Acl::ACTION_VIEW);
        $this->activeTab = 1;
        $this->activeMenu = self::MENU_LOGS;
        $this->pageTitle = "View Sms Logs and Status";
        $this->showPageTitle = false;
        
    }
	
}
