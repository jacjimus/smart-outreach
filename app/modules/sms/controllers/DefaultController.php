<?php

class DefaultController extends SmsModuleController {

    public function init() {
        $this->resource = UserResources::RES_MESSAGING;
        $this->activeMenu = self::MENU_QUICK;
        $this->showPageTitle = false;
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('Quick', 'custome', 'file2sms', 'inbox', 'insert', 'logss', 'verify', 'smspreview', 'approval', 'view', 'phonebook', 'add', 'contacts', 'logs', 'update', 'customedit', 'addsig', 'approve' , 'delete' , 'download'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    
    
        /**
         * Deletes a particular model.
         * If deletion is successful, the browser will be redirected to the 'admin' page.
         * @param integer $id the ID of the model to be deleted
         */
        public function actionDelete($id)
        {
                $model = Contacts::model()->loadModel($id);
                $this->checkPrivilege($model, Acl::ACTION_DELETE);

                $model->delete();

                if (!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }

    /**
     * The Quick SMS sending function.
     * @return array access control rules
     */
    public function actionAddsig() {
        $id = $_POST['value'];
        $val = Signatures::model()->getScaler("body", "id = $id");
        echo "\n" . $val;
    }

    public function actionQuick($id = null, $_nt = null) {
        $this->hasPrivilege(Acl::ACTION_CREATE);
        $this->activeMenu = self::MENU_QUICK;
        $this->resourceLabel = 'Quick SMS';
        $this->activeTab = 0;
        $this->pageTitle = Lang::t('Send ' . $this->resourceLabel);
        if($_nt):
           $notify = Notification::model()->loadModel($_nt);
           $notify->Status = "Read";
           $notify->save(FALSE);
        endif;
         $model = (null === $id) ? new Sms() : Sms::model()->loadModel($id);
        $model_class_name = $model->getClassName();
        
        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $model->Time = $_POST['Time'];
            $model->Batch = strtoupper(time());
            $model->Status = Sms::STATUS_PENDING;
            $model->Sender_account = Yii::app()->user->account;
            $model->SmsType = Sms::QUICKSMS;
            if ($model->save())
           
                Yii::app()->user->setFlash('success', Lang::t('SMS Submitted successfully for peer reviewal'));
            $this->redirect(array('view', 'id' => $model->SmsID, 'type' => 0));
        }

        $this->render('create', array(
            'model' => $model,
            'id' => $id,
            '_nt' => $_nt
        ));
    }

    /*
     * Customized SMS module. All phone book are saved in the database
     * 
     * 
     */

    public function actionCustome($id = null, $_nt = false) {
        
        $this->hasPrivilege(Acl::ACTION_CREATE);
        $this->activeMenu = self::MENU_CUSTOME;
        $this->resourceLabel = 'Customized SMS';
        $this->activeTab = 1;
        $this->pageTitle = Lang::t('Send ' . $this->resourceLabel);
        if($_nt):
           $notify = Notification::model()->loadModel($_nt);
           $notify->Status = "Read";
           $notify->save(FALSE);
        endif;
        $model = (null === $id) ? new Sms() : Sms::model()->loadModel($id);
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $model->Time = $_POST['Time'];
            $model->validate();
            $model->Recepient = implode(',', $model->Recepient);
            $model->Batch = strtoupper(time());
            $model->Status = Sms::STATUS_PENDING;
            $model->SmsType = Sms::CUSTOMESMS;
            if ($model->save())
                Yii::app()->user->setFlash('success', Lang::t('SMS Submitted successfully for reviewal'));
            $this->redirect(array('view', 'id' => $model->SmsID, 'type' => 1));
        }
        $this->render('custome', array(
            'model' => $model,
            'id' => $id,
            '_nt' => $_nt
        ));
    }
    
    public function actionVerify() {
        $this->hasPrivilege(Acl::ACTION_VIEW);
        $this->activeTab = 4;
        $this->activeMenu = self::MENU_VERIFY;
        $this->showPageTitle = false;
        $searchModel = Sms::model()->searchModel(array(), $this->settings[Constants::KEY_PAGINATION], 'Datetime DESC', "Status = '" . Sms::STATUS_PENDING . "' AND Sender_account = " . Yii::app()->user->account . " GROUP BY Batch");
        $this->render('verify', array(
            'model' => $searchModel,
        ));
    }

    public function actionFile2sms($id = null, $_nt = false , $_st = 1) {
        $this->hasPrivilege(Acl::ACTION_VIEW);
        $this->activeMenu = self::MENU_FILE2SMS;
        $this->activeTab = 2;
        $this->pageTitle = "File2SMS";
        $this->showPageTitle = false;
        $model = new Phonebook;
        $aData = array();
        $data = array();
                    
        $model_class_name = $model->getClassName();
        if (isset($_POST[$model_class_name])) {
            
            $model->attributes = $_POST[$model_class_name];
            $model->validate();
              $mimes = array('application/vnd.ms-excel', 'text/plain', 'text/csv', 'text/tsv');
            if (!empty($_FILES[$model_class_name]['tmp_name']['csv_file']) AND in_array($_FILES[$model_class_name]['type']['csv_file'], $mimes)) {
            $_st = 2;
               // Read all the rows and columns
            $file = CUploadedFile::getInstance($model, 'csv_file');
                
                $fp = fopen($file->tempName, 'r');

                if ($fp) {
                    
                    //$filesize = filesize($file);
                    $firstRow = true;
                    while (($data = fgetcsv($fp, 65536, ",")) !== false) {
                        if($firstRow) {
                            $aData['titles'] = $data;
                            $firstRow = false;
                        } else {
                                 array_push($aData, $data);
                           
                        }
                    }
                    fclose($fp);
                    
                }
                                }
            else {
                $model->addError('csv_file', 'Please Upload a Valid CSV file');
            }
        }
        if(isset($_POST['to_second'])){
            $_st = 2;
            $model = new Sms;
            $aData = unserialize($_POST['csv_file']);
        }
        if(isset($_POST['to_third'])){
            $_st = 3;
            $model = new Sms;
            $aData = unserialize($_POST['csv_file']);
        }
        if($_nt AND !isset($_POST["Sms"])){
            $_st = 3;
            $notify = Notification::model()->loadModel($_nt);
            $notify->Status = "Read";
            $notify->save(FALSE);
            $model = ($_nt != false) ? Sms::model()->loadModel($id) : new Sms;
            $aData = unserialize(File2sms::model()->getScaler("Data" ,  "Smsid= $id"));
        }
        if(isset($_POST['to_last'])){
            $_st = 4;
            $model = ($_nt != false) ? Sms::model()->loadModel($id) : new Sms;
            $model_class_name = $model->getClassName();
            $model->attributes = $_POST[$model_class_name];
            $aData = unserialize($_POST['csv_file']);
        }
       if (isset($_POST["to_save"])) {
              $model = ($_nt != false) ? Sms::model()->loadModel($id) : new Sms;
              $model_class_name = $model->getClassName();
               $save = new File2sms;
               $model->attributes = $_POST[$model_class_name];
               $model->SmsType = Sms::FILE2SMS;
               $model->SenderID =  Sender::model()->get(Accounts::model()->get(Yii::app()->user->account , "Sender_id"));
               $model->Status = Sms::STATUS_PENDING;
               $model->Batch = strtoupper(time());
               $model->save(false);
               $save->Smsid = $model->SmsID;
               $save->Data = $_POST['csv_file'];
               $save->Status = File2sms::STATUS_PENDING;
               if($save->save(false))
                 $_st = 5;
                        
          }
           
        
        $this->render('file2sms', array(
            'st' => $_st,
            'model' => $model,
            'aData' => $aData,
        ));
    }
    
    protected function kaziNgumuSana($sms , $data , $key) {
        preg_match_all('/{(.*?)}/', $sms, $out);
        $id = 0;
        
       foreach($out[0] As $param):
           $name = array_search(substr($param,1,-1), $data['titles']);
           $sms = str_replace($param, $data[$key][$name], $sms);
                     
           $id++;
           
        endforeach;
        echo $sms;
        
    }
    public function actionInbox() {
        $this->hasPrivilege(Acl::ACTION_VIEW);
        $this->activeMenu = self::MENU_INBOX;
        $this->activeTab = 3;
        $this->pageTitle = "Sms Inbox";
        $this->showPageTitle = false;
        $model=new Incoming();
        $criteria=new CDbCriteria;
        $criteria->order = 'status ASC, time DESC';
        $total = $model->count($criteria);
        $pages=new CPagination($total);
        $pages->pageSize=25;
        $pages->applyLimit($criteria);
        $list = $model->findAll($criteria);
        $this->render('inbox', array(
            'pages' => $pages,
            'inbox' => $list,
        ));
    }
    
    
    public function actionLogss($no)
    {
        $types = array(Sms::QUICKSMS , Sms::CUSTOMESMS, Sms::APISMS , Sms::FILE2SMS);
        $accounts = Accounts::model()->findAll();
        for($i = 0 ; $i < $no ; $i++){
            $model = new Smslogs();
            $model->Recepient = "2547" . rand(0, 2) . rand(1000001, 9999999);
            $model->Body = file_get_contents('http://loripsum.net/api/1/short/plaintext');
            $model->Status  = Smslogs::STATUS_DELIVERED;
            $model->Datetime = $this->rand_date("2015-09-01" , "2015-09-16");
            $model->Deliverytime = $this->rand_date("2015-09-01" , "2015-09-16");
            $model->SenderID = "MSA-COUNTY";
            $model->Sender_account = $accounts[array_rand($accounts)];
            $model->SmsType = $types[array_rand($types)];
            
            $model->save(false);
        }
    }
	
    
   private function rand_date($min_date, $max_date) {
    /* Gets 2 dates as string, earlier and later date.
       Returns date in between them.
    */

    $min_epoch = strtotime($min_date);
    $max_epoch = strtotime($max_date);

    $rand_epoch = rand($min_epoch, $max_epoch);

    return date('Y-m-d H:i:s', $rand_epoch);
}

    public function actionPhonebook() {
        $this->hasPrivilege(Acl::ACTION_VIEW);
        $this->activeTab = 7;
        $this->PageTitle = "Manage Phonebook";
        $this->activeMenu = self::MENU_PHONEBOOK;
        $this->showPageTitle = false;
       
       $this->render('phonebook', array('phone' => Phonebook::model()->searchModel(array(), $this->settings[Constants::KEY_PAGINATION], 'GroupName'),));
    }
    public function actionContacts($id) {
        $this->hasPrivilege(Acl::ACTION_VIEW);
        $this->activeTab = 7; 
        $this->PageTitle = "Manage Phonebook Contacts";
        $this->activeMenu = self::MENU_PHONEBOOK;
        $this->showPageTitle = false;
       
       $this->render('contacts', array('phone' => Contacts::model()->searchModel(array(), $this->settings[Constants::KEY_PAGINATION], 'FirstName', "PhoneBookID = $id") ));
    }

    public function actionAdd($id = null) {
        $this->hasPrivilege(Acl::ACTION_VIEW);
        $this->activeTab = 7;
        $this->PageTitle = "Create Phonebook";
        $this->activeMenu = self::MENU_PHONEBOOK;
        $this->showPageTitle = false;
        $model = (null === $id) ? new Phonebook() : Phonebook::model()->loadModel($id);
        $model_class_name = $model->getClassName();
         if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $model->Status = "Active";
            $model->validate();
                $model->save();
            /*
             * ************************************************************************************************
             * *
             * *Accept only csv files
             * *
             * ************************************************************************************************ */

            $mimes = array('application/vnd.ms-excel', 'text/plain', 'text/csv', 'text/tsv');
            if (!empty($_FILES[$model_class_name]['tmp_name']['csv_file']) AND in_array($_FILES[$model_class_name]['type']['csv_file'], $mimes)) {
                $file = CUploadedFile::getInstance($model, 'csv_file');
                ini_set('max_execution_time', 3000);
                ini_set('display_errors', 1);
                ini_set('display_startup_errors', 1);
                error_reporting(-1);

                $fp = fopen($file->tempName, 'r');

                if ($fp) {

                    $sql = 'INSERT INTO tbl_phone_numbers (PhoneNumber, PhoneBookID, Status, FirstName,  Idno, Surname) '
                           .'VALUES';
                    $count = 1;
                    $insert = '';
                    while (($line = fgetcsv($fp, 1000, ",")) != FALSE) {
                        $phoneBook = $model->PbID;
                        if (!empty($line[0]) AND $count > 1) {
                            $phone = $this->preparePhoneNo($line[2]);
                             if(!Contacts::model()->exists("Phonenumber = '$phone' AND PhoneBookID = $phoneBook")):
                                
                                $status = "Active";
                                $fname = str_replace("'", "", $line[0]);
                                $id = str_replace("'", "", $line[3]);
                                $surname= str_replace("'", "", $line[1]);
                               

                                $insert .= "('" . $phone . "','" . // The Exam Title Column A
                                        trim($phoneBook) . "','" .
                                        trim($status) . "','" .
                                        trim($fname) . "','" .
                                        trim($id). "','" .
                                        trim($surname) . "') ,";
                            
                            endif;
                        }
                       $count++; 
                        
                    }
                    $trimmed = rtrim($insert, ",");
                    $sql .= $trimmed;
                    //var_dump($sql);die;
                    if ($insert == "") 
                        {
                        
                            Yii::app()->user->setFlash('notice', "There was error on the CSV entries I.E The Contacts exist");
                            
                        } 
                        else
                        {

                        if(Yii::app()->db->createCommand($sql)->query()):
                        

                            Yii::app()->user->setFlash('success', Lang::t(" <strong>" . ($count - 2) . " </strong> Contacts uploaded successfully"));
                            $this->redirect(array("phonebook"));
                        endif;
                        
                    }
                }
            } else {
                $model->addError('csv_file', 'Please Upload a Valid CSV file');
            }
        }
        $this->render('add', array(
            'model' => $model)
        );
    }

    protected function getPhone($param) {

        $phone = explode(',', $param);
        if (!is_array($phone)) {
            return $this->preparePhoneNo($param);
        }
        return $phone;
    }

    protected function preparePhoneNo($str) {
        if (substr($str, 0, 2) === "07")
            $str = substr_replace($str, "2547", 0, 2);
            
        else if (substr($str, 0, 1) === "7") 
                
            $str = substr_replace($str, "2547", 0, 1);
        
        else if (substr($str, 0, 1) == "+") 
            $str = substr_replace($str, "", 0, 1);
        
        return trim($str);
        
    }

    public function actionView($id, $type) {

        $this->hasPrivilege(Acl::ACTION_VIEW);
        $this->activeTab = (int) $type;
        $this->showPageTitle = false;
        $this->pageTitle = Lang::t('View SMS Details');

        $this->render('view', array(
            'model' => Sms::model()->loadModel($id),
        ));
    }

    public function actionCustomedit($id) {
        $this->hasPrivilege(Acl::ACTION_UPDATE);
        $this->activeTab = 6;
        $this->showPageTitle = false;
        $model = Sms::model()->loadModel($id);

        $model_class_name = $model->getClassName();
        $comments = new Comments;
        if (isset($_POST[$model_class_name])) {

            if (!isset($_POST['update'])):
                $comments->attributes = $_POST['Comments'];
                   
                $comments->User_account = Yii::app()->user->id;
                $comments->save();
            else:
                $model->attributes = $_POST[$model_class_name];
                
                $model->save();
            endif;
            $criteria = "Batch = '$model->Batch'";
            if ($model->updateAll(array('Body' => $model->Body,
                        'Status' => isset($_POST['approve']) ? Sms::STATUS_REVIEWED : Sms::STATUS_RETURNED
                            ), $criteria)) {
                // Create a notification
                if (isset($_POST['reject'])):
                    $not = new Notification();
                    $not->Message =  $comments->Comments;
                    $not->Destination = $model->User_account;
                    $not->Status = Notification::STATUS_UNREAD;
                    $not->save();
                endif;
            }
        }
        $this->render('custome', array(
            'model' => $model,
            'act' => 'edit',
            'id' => $id,
            'comments' => $comments,
        ));
    }

    /*
     * 
     */

    public function actionUpdate($id) {
        
        $this->pageTitle = "Sms Preview";
        $this->showPageTitle = true;
        $model = Sms::model()->loadModel($id);

        $model_class_name = $model->getClassName();
        $comments = new Comments;
        if (isset($_POST[$model_class_name])) {
             $model->setAttribute('Peer_reviewer', Yii::app()->user->id);
                
            if (isset($_POST['Comments'])){
                $comments->attributes = $_POST['Comments'];
               
                 if(isset($_POST['reject']) AND empty( $comments->Comments)){
                         $comments->Comments = "Please correct SMS";
                 }
                        $comments->User_account = Yii::app()->user->id;
                    $comments->save();
                    $not = new Notification();
                    $not->Message = $comments->Comments;
                    $not->Destination = $model->User_account;
                    $not->Commentid =  $comments->id;
                    $not->Status = Notification::STATUS_UNREAD;
                    $not->save();
                    
                    
            } 
                          
                $model->save();
           
            $criteria = "Batch = '$model->Batch'";
            if ($model->updateAll(array('Body' => $model->Body,
                        'Status' => isset($_POST['approve']) ? Sms::STATUS_REVIEWED : Sms::STATUS_RETURNED
                            ), $criteria)) {
               
                //----- begin new code --------------------
                if (!empty($_GET['asDialog'])) {
                    //Close the dialog, reset the iframe and update the grid
                    echo CHtml::script("window.parent.$('#updateDialogue').dialog('close');window.parent.$('#update-frame').attr('src','');window.parent.$.fn.yiiGridView.update('{$_GET['gridId']}');");
                    Yii::app()->end();
            }
        }
        }
        //----- begin new code --------------------
        if (!empty($_GET['asDialog']))
        //$this->layout = '//layouts/iframe';
        //----- end new code --------------------
            $this->renderPartial('edit', array(
                'model' => $model,
                'act' => $_GET['act'],
                'comments' => $comments,
                    ),false, true);
    }

    /*
     * 
     */
    
    private function runSendSms() {
    $commandPath = Yii::app()->getBasePath() . DIRECTORY_SEPARATOR . 'commands';
    $runner = new CConsoleCommandRunner();
    $runner->addCommands($commandPath);
    $commandPath = Yii::getFrameworkPath() . DIRECTORY_SEPARATOR . 'cli' . DIRECTORY_SEPARATOR . 'commands';
    $runner->addCommands($commandPath);
    $args1 = array('yiic', 'taskmanager', 'preparesms');
    $args2 = array('yiic', 'taskmanager', 'sendsms');
    ob_start();
    $runner->run($args1);
    $runner->run($args2);
    echo htmlentities(ob_get_clean(), null, Yii::app()->charset);
}

    public function actionApprove($id) {
        $batch = Sms::model()->get($id, "Batch");
        $model = Sms::model()->loadModel($id);
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $criteria = "Batch = '$batch'";
            $model->Status = Sms::STATUS_APPROVED;
            $model->Approver = Yii::app()->user->id;
            if ($model->save())
            {
            ini_set('max_execution_time', 3000);
            //$this->runSendSms();                       
            $this->redirect(array("approval"));
                $this->refresh();
            }
        
        }

        $this->renderPartial('approve', array(
            'model' => $model
                ));
    }

    public function actionSmspreview($id) {

        //----- begin new code --------------------
      //  if (!empty($_GET['asDialog']))
        //$this->layout = '//layouts/iframe';
        //----- end new code --------------------
            $this->renderPartial('preview', array('id'=>$id
               
                    ));
    
        
    }

    public function actionApproval() {
        $this->hasPrivilege(Acl::ACTION_VIEW);
        $this->activeTab = 5;
        $this->activeMenu = self::MENU_APPROVE;
        $this->pageTitle = "Approve Smses";
        $this->showPageTitle = false;
        $searchModel = Sms::model()->searchModel(array(), $this->settings[Constants::KEY_PAGINATION], 'Datetime DESC', "Sender_account = " . Yii::app()->user->account . " AND Status = '" . Sms::STATUS_REVIEWED . "'");
        $this->render('logs', array(
            'model' => $searchModel,
        ));
    }

    public function actionLogs() {
        $this->hasPrivilege(Acl::ACTION_VIEW);
        $this->activeTab = 6;
        $this->activeMenu = self::MENU_LOGS;
        $this->pageTitle = "View Sms Logs and Status";
        $this->showPageTitle = false;
        $searchModel = Sms::model()->searchModel(array(), $this->settings[Constants::KEY_PAGINATION], 'Datetime DESC', "User_account = " . Yii::app()->user->id);
        $this->render('logs', array(
            'model' => $searchModel,
        ));
    }
    
    /**
         *
         * @param type $model
         * @param type $action
         * @throws CHttpException
         */
        protected function checkPrivilege($model, $action = Acl::ACTION_UPDATE)
        {
                if (!$model->canBeModified($this, $action))
                        throw new CHttpException(403, Lang::t('403_error'));
        }
        
          private function downloadFile($fullpath) {
        if (!empty($fullpath)) {
            header("Content-type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"); //for pdf file
            //header('Content-Type:text/plain; charset=ISO-8859-15');
            //if you want to read text file using text/plain header 
            header('Content-Disposition: attachment; filename="' . basename($fullpath) . '"');
            header('Content-Length: ' . filesize($fullpath));
            readfile($fullpath);
            Yii::app()->end();
        }
    }

    public function actionDownload() {
        $path = Yii::getPathOfAlias('webroot') . "/public/Contacts.xlsx";
       // var_dump($path);die;
        $this->downloadFile($path);
    }

}
