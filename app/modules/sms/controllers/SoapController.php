<?php

class SoapController extends SmsModuleController
        {

           public function actions()
            {
              // here you need to mention the class for the web service
                return array(
                    'send'=>array(
                        'class'=>  'CWebServiceAction',
                    )
                );
            }
        // these comments are used by Yii to create wsdl for you. The following functions is a webservice
              /**
                 * @param string the username of the sms
                 * @param string the apiKey of the sms
                 * @param integer the msisdn of the sms
                 * @param integer the msgID of the sms
                 * @param string the body of the sms
                 * @return string this is the status  of the message
                 * 
                 * 
                 * @soap
                 */
                public function sendSms($username , $apiKey ,   $msgID , $msisdn, $body)
                {
                    if($username != "0uTr3@cH"):
                        $array = array("code" => 500, "message" => "wrong username");
                        return CJSON::encode($array);
                    elseif ($apiKey != "4dm1n2015sd0343994dsmdnw23"):
                        $array = array("code" => 500, "message" => "wrong api_key");
                         return CJSON::encode($array);
                    else:
                    $query="select Status from tbl_sms_logs where SmsID = '$msgID'";
                    $record=  Yii::app()->db->createCommand($query)->queryScalar();
                    $array = array("code" => 200 , "message" => $record);
                    return CJSON::encode($array);
                    endif;
                }

/*this action is created to consume the webservice.
If you will visit this action then you will get the timestamp which is sent to you as a webservice response
*/
             public function actionLets()
                {
                 
                    $msgID = "";
                    $msisdn = "254725830529" ;
                    $apiKey = "4dmSdd0SDLsd0uiwqedvbds0882";
                    $username = "msaCounty" ;
                    $body = "Hello James Makau" ;
                    $accID = 900;
                    
                    $xml_data ='<?xml version="1.0"?>'
                            . '<smslist>'
                                . '<sms>'
                                        . '<username>'.$username.'</username>'
                                        . '<apiKey>'.$apiKey.'</apiKey>'
                                        . '<msgID>'.$msgID.'</msgID>'
                                        . '<accountID>'.$accID.'</accountID>'
                                        . '<msisdn>'.$msisdn.'</msisdn>'
                                        . '<body>'.$body.'</body>'
                                        . '<type>1</type>'
                                        . '<group>-1</group>'
                                .'</sms>'
                            . '</smslist>'; 
             $array = array("username" => $username,
                            "apiKey" => $apiKey,
                            "msgID" => $msgID,
                            "msisdn" => $msisdn,
                            "body" => $body);
                    $json_data = CJSON::encode($array);   
                    
                    
 
$URL = "http://localhost/sms/api/xml";
 
			$ch = curl_init($URL);			
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml_data");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$output = curl_exec($ch);
			curl_close($ch);

print_r($output);
 
                }

        
/*this action is created to consume the webservice.
If you will visit this action then you will get the timestamp which is sent to you as a webservice response
*/
             public function actionSms()
                {
                 
                    $msgID = "";
                    $msisdn = "254725830529" ;
                    $apiKey = "4dmSdd0SDLsd0uiwqedvbds0882";
                    $username = "msaCounty" ;
                    $body = "This is the sms body" ;
                    
                    $xml_data ='<?xml version="1.0"?>'
                            . '<smslist>'
                                . '<sms>'
                                        . '<username>'.$username.'</username>'
                                        . '<apiKey>'.$apiKey.'</apiKey>'
                                        . '<msgID>'.$msgID.'</msgID>'
                                        . '<msisdn>'.$msisdn.'</msisdn>'
                                        . '<body>'.$body.'</body>'
                                        . '<type>1</type>'
                                        . '<group>-1</group>'
                                .'</sms>'
                            . '</smslist>'; 
             $array = array("username" => $username,
                            "apiKey" => $apiKey,
                            "msgID" => $msgID,
                            "msisdn" => $msisdn,
                            "body" => $body);
                    $json_data = CJSON::encode($array);   
                    
                    
 
$URL = "http://localhost/sms/api/incoming";
 
			$ch = curl_init($URL);			
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			curl_setopt($ch, CURLOPT_POSTFIELDS, "$json_data");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$output = curl_exec($ch);
			curl_close($ch);

print_r($output);
 
                }

        }
