
<?php

$grid_id = 'quick-sms-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Lang::t($this->pageTitle),
    'titleIcon' => null,
    'showExportButton' => false,
    'showSearch' => true,
    'createButton' => array('visible'=> false),
    'showRefreshButton' => false,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        
        'columns' => array(
        array(
            'name' => 'SenderID',
            'type' => 'raw',
            'value' => 'Sender::model()->get($data->SenderID , "SenderID")',
        ),
        
        array(
            'name' => 'Body',
            'type' => 'raw',
            'value' => '$data->Body',
        ),
           
        array(
            'name' => 'Status',
            'type' => 'raw',
            'value' => 'CHtml::tag(\'span\', array(\'class\' => "badge $data->class"), $data->Status)',
        ),
       
        array(
            'class' => 'ButtonColumn',
            'template' => ' {preview}&nbsp;&nbsp; {custome}&nbsp;&nbsp;{amend}&nbsp;&nbsp;{approve}',
            'htmlOptions' => array('class' => 'text-left', 'style' => 'width: 100px; text-align: left !important;'),
            'buttons' => array(
                
                'custome' => array(
                    'imageUrl' => false,
                    'label' => '<i class="icon-edit bigger-130"></i>',
                    'url' => 'Yii::app()->controller->createUrl("custome",array("id"=>$data->primaryKey))',
                    'visible' => '$this->grid->owner->showLink("' . UserResources::RES_QUICK_MESSAGING . '", "' . Acl::ACTION_UPDATE . '") AND ($data->Status == "' .Sms::STATUS_PENDING. '" OR $data->Status == "' .Sms::STATUS_RETURNED. '") AND $data->SmsType == "'. Sms::CUSTOMESMS .'" ?true:false',
                    'options' => array(
                        'class' => 'blue',
                        'title' => 'Amend custome SMS',
                    ),
                ),
                
                   
                'amend' => array(
                    'imageUrl' => false,
                    'label' => '<i class="icon-pencil bigger-130"></i>',
                    'url' => 'Yii::app()->controller->createUrl("quick",array("id"=>$data->primaryKey, "act"=>"edit", "asDialog"=>1,"gridId"=>$this->grid->id))',
                    
                    'visible' => '$this->grid->owner->showLink("' . UserResources::RES_QUICK_MESSAGING . '", "' . Acl::ACTION_UPDATE . '") AND ($data->Status == "' .Sms::STATUS_PENDING. '" OR $data->Status == "' .Sms::STATUS_RETURNED. '") AND $data->SmsType == "' .Sms::QUICKSMS. '" ?true:false',
                    'options' => array(
                        'class' => 'blue',
                        'title' => 'Amend SMS',
                    ),
                ),
                
                
                 'preview' => array(
                    'imageUrl' => false,
                    'label' => '<i class="icon-eye-open bigger-130"></i>',
                    'url' => 'Yii::app()->controller->createUrl("smspreview",array("id"=>$data->primaryKey,"asDialog"=>1,"gridId"=>$this->grid->id))',
                    'click'=>'function(){$("#previewframe").attr("src",$(this).attr("href")); $("#previewDialogue").dialog("open");  return false;}',
                    'visible' => '$this->grid->owner->showLink("' . UserResources::RES_SMS_APPROVALS . '", "' . Acl::ACTION_VIEW . '") ',
                    'options' => array(
                        'class' => 'brown show-colorbox',
                        'title' => 'preview SMSes',
                    ),
                ),
                
                'approve' => array(
                    'imageUrl' => false,
                    'label' => '<i class="icon-check bigger-130"></i>',
                    'url' => 'Yii::app()->controller->createUrl("approve",array("id"=>$data->primaryKey,"asDialog"=>1,"gridId"=>$this->grid->id))',
                    //'click'=>'function(){$("#approve-frame").attr("src",$(this).attr("href")); $("#approveDialogue").dialog("open");  return false;}',
                    'visible' => '$this->grid->owner->showLink("' . UserResources::RES_SMS_APPROVALS . '", "' . Acl::ACTION_VIEW . '") AND $data->Status == "' .Sms::STATUS_REVIEWED. '"?true:false',
                    'options' => array(
                        'class' => 'green show-colorbox',
                        'title' => 'Approve SMS',
                    ),
                ),
            )
        ),
    ),
)
)
)
;
?>


<?php
// Diaogue for adding new custom report
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'updateDialogue',
    'options' => array(
        'title' => 'Amend SMS ',
        'autoOpen' => false,
        'modal' => false,
        'width' => 500,
        'height' => 400,
    ),
));
?>
<iframe id="update-frame" width="100%" height="100%"></iframe>
<?php $this->endWidget(); ?>



<?php
// Diaogue for approving SMS
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'approveDialogue',
    'options' => array(
        'title' => 'Approve SMS ',
        'autoOpen' => false,
        'modal' => false,
        'width' => 500,
        'height' => 400,
    ),
));
?>
<iframe id="approve-frame" width="100%" height="100%"></iframe>
<?php $this->endWidget(); ?>


<?php
// Diaogue for adding new custom report
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'previewDialogue',
    'options' => array(
        'title' => 'Preview SMS',
        'autoOpen' => false,
        'modal' => true,
        'width' => 900,
        'height' => 500,
    ),
));
?>
<iframe id="previewframe" width="100%" height="100%">
 
</iframe>
<?php $this->endWidget(); ?>