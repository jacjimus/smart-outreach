<?php
$this->breadcrumbs = array('Messaging Module ' => '',
    $this->pageTitle,
);
?>
<div class="widget-box transparent">
    <div class="widget-header">
        <?php echo $this->renderPartial('sms.views.verify._tab') ?>
    </div> 
</div>
<div class="widget-box transparent">
  
    <?php
$grid_id = 'everify-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Lang::t($this->pageTitle),
    'titleIcon' => null,
    'showExportButton' => false,
    'showSearch' => true,
    'createButton' => array('visible'=> true , 'url'=>'create' , 'label' => "Create E-Account"),
    'showRefreshButton' => false,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        
        'columns' => array(
        array(
            'name' => "account_name",
            'type' => 'raw',
            'value' => '$data->account_name',
        ),
        array(
            'name' => "keyword",
            'type' => 'raw',
            'value' => '$data->keyword',
        ),
         
       array(
            'name' => 'data_table',
            'type' => 'raw',
            'value' => 'CHtml::link(CHtml::encode($data->data_table),Yii::app()->controller->createUrl("contacts",array("id"=>$data->primaryKey)))',
        ),
           
        array(
            'name' => 'status',
            'type' => 'raw',
            'value' => 'CHtml::tag(\'span\', array(\'class\' => "badge badge-success"), $data->status)',
        ),
       
        
        array(
            'class' => 'ButtonColumn',
            'template' => ' {edit}&nbsp;&nbsp; {delete}',
            'htmlOptions' => array('class' => 'text-left', 'style' => 'width: 100px; text-align: left !important;'),
            'buttons' => array(
                
                
             
                
                'edit' => array(
                    'imageUrl' => false,
                    'label' => '<i class="icon-edit bigger-130"></i>',
                    'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->primaryKey))',
                    'visible' => '$this->grid->owner->showLink("' . UserResources::RES_SMS_E_VERIFY . '", "' . Acl::ACTION_UPDATE . '")',
                    'options' => array(
                        'class' => 'blue',
                        'title' => 'Update E-account',
                    ),
                ),
                
                   
                
                
                
                'delete' => array(
                    'imageUrl' => false,
                    'label' => '<i class="icon-trash bigger-130"></i>',
                    'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->primaryKey))',
                    'visible' => '$this->grid->owner->showLink("' . UserResources::RES_SMS_E_VERIFY . '", "' . Acl::ACTION_DELETE . '")?true:false',
                    'options' => array(
                        'class' => 'delete red',
                        'title' => 'Delete',
                    ),
                ),
              
            )
        ),
    ),
)
)
)
;
?>


<?php
// Diaogue for adding new custom report
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'updateDialogue',
    'options' => array(
        'title' => 'Amend SMS ',
        'autoOpen' => false,
        'modal' => false,
        'width' => 500,
        'height' => 400,
    ),
));
?>
<iframe id="update-frame" width="100%" height="100%"></iframe>
<?php $this->endWidget(); ?>



<?php
// Diaogue for approving SMS
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'approveDialogue',
    'options' => array(
        'title' => 'Approve SMS ',
        'autoOpen' => false,
        'modal' => false,
        'width' => 500,
        'height' => 400,
    ),
));
?>
<iframe id="approve-frame" width="100%" height="100%"></iframe>
<?php $this->endWidget(); ?>


<?php
// Diaogue for adding new custom report
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'previewDialogue',
    'options' => array(
        'title' => 'Preview SMS',
        'autoOpen' => false,
        'modal' => true,
        'width' => 900,
        'height' => 500,
    ),
));
?>
<iframe id="previewframe" width="100%" height="100%">
 
</iframe>
<?php $this->endWidget(); ?>
     </div>

    