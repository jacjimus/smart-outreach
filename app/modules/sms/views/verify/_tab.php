
<div class="widget-toolbar no-border" style="float:left !important;">
    <ul class="nav nav-tabs my-nav">

        <?php if ($this->showLink(UserResources::RES_SMS_E_VERIFY)): ?>
            <li class="<?php echo $this->activeTab === 1 ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl(ModulesEnabled::MOD_MESSAGING . '/verify/accounts') ?>"><i class="icon-signal"></i> <?php echo Lang::t('Manage e-verify Accounts') ?></a></li>
        <?php endif; ?>
        <?php if ($this->showLink(UserResources::RES_E_VERIFY_INCOMING)): ?>
            <li class="<?php echo $this->activeTab === 2 ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl(ModulesEnabled::MOD_MESSAGING . '/verify/inbox') ?>"><i class="icon-phone"></i> <?php echo Lang::t('Incoming Requests') ?></a></li>
        <?php endif; ?>
        <?php if ($this->showLink(UserResources::RES_E_VERIFY_REPORTING)): ?>
            <li class="<?php echo $this->activeTab === 3 ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl(ModulesEnabled::MOD_MESSAGING . '/verify/reports') ?>"><i class="icon-bar-chart"></i> <?php echo Lang::t('E-Verification Reports') ?></a></li>
            <?php endif; ?>

    </ul>
</div>
