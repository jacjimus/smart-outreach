 <?php
    $grid_id = 'logs-grid';

    $this->widget('ext.MyGridView.ShowGrid', array(
        'title' => Lang::t($this->pageTitle),
        'titleIcon' => null,
        'showExportButton' => true,
        //'dataProvider'=>$model->search(),
        //'filter'=>$model,
        'showSearch' => true,
        'createButton' => array('visible' => false),
        'showRefreshButton' => false,
        'grid' => array(
            'id' => $grid_id,
            'model' => $model,
            'columns' => array(
               
                array(
                    'name' => 'SenderID',
                    'type' => 'raw',
                    'value' => 'Sender::model()->get($data->SenderID , "SenderID")',
                ),
                array(
                    'name' => 'Body',
                    'type' => 'raw',
                    'value' => '$data->Body',
                ),
                array(
                    'name' => 'Datetime',
                    'type' => 'raw',
                    'value' => '$data->Datetime',
                ),
                array(
                    'name' => 'Status',
                    'type' => 'raw',
                    'value' => '$data->Status',
                ),
                array(
                    'name' => 'Recepient',
                    'type' => 'raw',
                    'value' => '$data->Recepient',
                ),
            ),
        )
            )
    )
    ;
    ?>

