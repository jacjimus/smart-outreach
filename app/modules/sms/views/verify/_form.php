


<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'email-template-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
        'role' => 'form',
    )
        ));
?>
<div class="form-group">
                <?php echo $form->labelEx($model, 'account_name', array('class' => 'col-lg-2 control-label')); ?>
                <div class="col-lg-5">
                        <?php echo $form->textField($model, 'account_name', array('class' => 'form-control')); ?>
                        <?php echo CHtml::error($model, 'account_name') ?>
                </div>
        </div>
        <div class="form-group">
                <?php echo $form->labelEx($model, 'keyword', array('class' => 'col-lg-2 control-label')); ?>
                <div class="col-lg-5">
                        <?php echo $form->textField($model, 'keyword', array('class' => 'form-control', 'maxlength' => 10)); ?>
                      <?php echo CHtml::error($model, 'keyword') ?>   
                </div>
        </div>
        <div class="form-group">
                <?php echo $form->labelEx($model, 'data_table', array('class' => 'col-lg-2 control-label')); ?>
                <div class="col-lg-5">
                        <?php 
  $curdb  = explode('=', Yii::app()->db->connectionString);

$tables = $sql="SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA LIKE '" . $curdb[3]."' AND TABLE_NAME LIKE 'ver_%'";
$tables = Yii::app()->db
         ->createCommand($sql)
         ->queryAll();
 //var_dump($tables);die;
$options = array();
foreach($tables as $tbl)
{//for example
    $options[$tbl['TABLE_NAME']] = $tbl['TABLE_NAME'];
}
echo $form->dropDownList($model,'data_table', $options, array('class' => 'form-control')); ?>
      <?php echo CHtml::error($model, 'data_table') ?>   
                </div>
        </div>
        
<div class="clearfix form-actions">
        <div class="col-lg-offset-2 col-lg-9">
                <button class="btn btn-primary" type="submit"><i class="icon-ok bigger-110"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
                &nbsp; &nbsp; &nbsp;
                <a class="btn" href="<?php echo $this->createUrl('accounts') ?>"><i class="icon-remove bigger-110"></i><?php echo Lang::t('Cancel') ?></a>
        </div>
</div>
<?php $this->endWidget(); ?>
