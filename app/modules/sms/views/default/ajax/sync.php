<script type="text/javascript">
    function getADReports()
    {
        var prevdate = $("#Antenatal_Prevdate").val();
        var previd = $("#Antenatal_Previd").val();
        var prevstatus = $("#Antenatal_Status").val();
        var smsstatus = $("#Antenatal_Smsnotifystatus").val();
        if (prevdate == "")
        {
            alert('Please select Issuance date');
            return false;
        }
        var values = 'prevdate=' + prevdate + "&previd=" + previd + "&prevstatus=" + prevstatus + 
                "&smsstatus=" + smsstatus + '&cache=' + Math.random();
        // Preloder for generating Reports JM 
        $('#daily_reports').show();
        $('#daily_reports').html("<img src= '<?php echo Yii::app()->request->baseUrl ?>/themes/default/images/ajax-loader.gif '/>");
        $.ajax
                ({
                    type: "POST",
                    url: "daily",
                    data: values,
                    cache: false,
                    success: function(html)
                    {
                        $("#daily_reports").html(html);

                    }
                });

    }
    ;
    function getWeeklyReports()
    {
        var start = $("#Syncorder_w_start").val();
        var end = $("#Syncorder_w_end").val();
        var status = $("#Syncorder_w_status").val();
        var short_code = $("#Syncorder_w_short_code").val();
        if (status == "")
        {
            alert('Please select sub type');
            return false;
        }
        var values = 'start=' + start + "&end=" + end + "&status=" + status + "&short_code=" + short_code + '&cache=' + Math.random();
        // Preloder for generating Reports JM 
        $('#weekly_reports').show();
        $('#weekly_reports').html("<img src= '<?php echo Yii::app()->request->baseUrl ?>/themes/default/images/ajax-loader.gif '/>");
        $.ajax
                ({
                    type: "POST",
                    url: "weeklySubs",
                    data: values,
                    cache: false,
                    success: function(html)
                    {
                        $("#weekly_reports").html(html);

                    }
                });

    }
    ;
    function getCustomReports()
    {
        var start = $("#Syncorder_c_start").val();
        var status = $("#Syncorder_c_status").val();
        var end = $("#Syncorder_c_end").val();
        var report = $("#Syncorder_report_name").val();
        var values = 'start=' + start + '&end=' + end + "&status=" + status + "&report=" + report + '&cache=' + Math.random();
        // Preloder for generating Reports JM 
        $('#custome_reports').show();
        $('#custome_reports').html("<img src= '<?php echo Yii::app()->request->baseUrl ?>/themes/default/images/ajax-loader.gif '/>");
        $.ajax
                ({
                    type: "POST",
                    url: "CustomSubs",
                    data: values,
                    cache: false,
                    success: function(html)
                    {
                        $("#custome_reports").html(html);

                    }
                });

    }
    ;
    function getMonthlyReports()
    {
        var year = $("#Syncorder_year").val();
        var month = $("#Syncorder_month").val();
        var status = $("#Syncorder_m_status").val();
        var short_code = $("#Syncorder_m_short_code").val();
        if (status == "")
        {
            alert('Please select sub type');
            return false;
        }
        var values = 'year=' + year + '&month=' + month + "&short_code=" + short_code + "&status=" + status + '&cache=' + Math.random();
        // Preloder for generating Reports JM 
        $('#monthly_reports').show();
        $('#monthly_reports').html("<img src= '<?php echo Yii::app()->request->baseUrl ?>/themes/default/images/ajax-loader.gif '/>");
        $.ajax
                ({
                    type: "POST",
                    url: "monthlySubs",
                    data: values,
                    cache: false,
                    success: function(html)
                    {
                        $("#monthly_reports").html(html);

                    }
                });

    }
    ;


    $(function() {
        $("#tabs").tabs();
    });


//****************************Ajax file for all Termination reports requests************
    function getDailyTerms()
    {
        var d_date = $("#Terminations_d_date").val();
        var d_short_code = $("#Terminations_d_short_code").val();

        var values = 'd_date=' + d_date + "&d_short_code=" + d_short_code + '&cache=' + Math.random();
        // Preloder for generating Reports JM 
        $('#daily_terms').show();
        $('#daily_terms').html("<img src= '<?php echo Yii::app()->request->baseUrl ?>/themes/default/images/ajax-loader.gif '/>");
        $.ajax
                ({
                    type: "POST",
                    url: "showTermReports",
                    data: values,
                    cache: false,
                    success: function(html)
                    {
                        $("#daily_terms").html(html);

                    }
                });

    }
    ;

    function getWeeklyTerms()
    {
        var start = $("#Terminations_w_start").val();
        var end = $("#Terminations_w_end").val();
        var w_short_code = $("#Terminations_w_short_code").val();

        var values = 'start=' + start + '&end=' + end + "&w_short_code=" + w_short_code + '&cache=' + Math.random();
        // Preloder for generating Reports JM 
        $('#weekly_terms').show();
        $('#weekly_terms').html("<img src= '<?php echo Yii::app()->request->baseUrl ?>/themes/default/images/ajax-loader.gif '/>");
        $.ajax
                ({
                    type: "POST",
                    url: "showWeeklyTermReports",
                    data: values,
                    cache: false,
                    success: function(html)
                    {
                        $("#weekly_terms").html(html);

                    }
                });

    }
    ;
    function getMonthlyTerms()
    {
        var year = $("#Terminations_year").val();
        var month = $("#Terminations_month").val();
        var short_code = $("#Terminations_m_short_code").val();

        var values = 'year=' + year + '&month=' + month + "&short_code=" + short_code + '&cache=' + Math.random();
        // Preloder for generating Reports JM 
        $('#monthly_terms').show();
        $('#monthly_terms').html("<img src= '<?php echo Yii::app()->request->baseUrl ?>/themes/default/images/ajax-loader.gif '/>");
        $.ajax
                ({
                    type: "POST",
                    url: "showMonthlyTermReports",
                    data: values,
                    cache: false,
                    success: function(html)
                    {
                        $("#monthly_terms").html(html);

                    }
                });

    }
    ;

    function getCustomTermReports()
    {
        var start = $("#Terminations_c_start").val();
        var end = $("#Terminations_c_end").val();
        var report = $("#Terminations_report_name").val();
        var values = 'start=' + start + '&end=' + end + "&report=" + report + '&cache=' + Math.random();
        // Preloder for generating Reports JM 
        $('#custome_terms').show();
        $('#custome_terms').html("<img src= '<?php echo Yii::app()->request->baseUrl ?>/themes/default/images/ajax-loader.gif '/>");
        $.ajax
                ({
                    type: "POST",
                    url: "CustomTerms",
                    data: values,
                    cache: false,
                    success: function(html)
                    {
                        $("#custome_terms").html(html);

                    }
                });

    }
    ;
</script>
