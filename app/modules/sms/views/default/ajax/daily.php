<?php
$this->breadcrumbs = array('Mothers Module ' => 'manage',
    $this->pageTitle,
);
?>
<div class="widget-box transparent">
  <div class="widget-header">
      <h4><?php echo CHtml::encode($this->pageTitle); ?></h4>
                <div class="widget-toolbar">
                        <?php
$this->widget('application.extensions.print.printWidget', array(
    'cssFile' => 'print.css',
    'printedElement' => '#daily-report-antenatal',
));
?>
                </div>
      
    </div>      
        <div class="widget-body widget-body-style2">
                <div class="widget-main padding-12 no-padding-left no-padding-right">
                        <div class="tab-content padding-4">
                            <table class="table table-striped table-bordered table-hover" id="daily-report-antenatal">
    <?php if(!empty($data)):?>  
                                                <thead>
                                                    <tr>
                                                        <th>Patient No</th>
                                                        <th>Patient Name</th>
                                                         <th>Preventive Care</th>
                                                         <th>SMS Date</th>
                                                         <th>Date Prescribed</th>
                                                         <th> Date Issued</th>
                                                        <th>Facility</th>
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    foreach ($data As $mod):
                                                        
                                                        $sms = Yii::app()->db->createCommand("Select Smsdate FROM tbl_smslogs "
                                                                . "WHERE Motherid = $mod[Motherid] AND Careid = $mod[Previd] AND Caretype LIKE '". Smslogs::STATUS_SMS_TYPE_ANTENATAL ."' ")->queryScalar();
                                                    ?>
                                                    <tr>

                                                        <td><?php echo Patient::model()->get($mod['Motherid'] , "Patientno") ?></td>
                                                            <td><?php echo Patient::model()->get($mod['Motherid'] , "CONCAT(Lname, ' ', Onames)")  ?></td>
                                                            <td><?php echo Preventive::model()->get($mod['Previd'] , "Prevname") ?> </td>
                                                            <td><?php echo $mod['Smsnotifystatus'] == Antenatal::STATUS_SMS_NOTIFY_YES ? $sms : Antenatal::STATUS_SMS_NOTIFY_NO  ?> </td>
                                                            <td><?php echo $mod['Prevdate'] ?> </td>
                                                            <td><?php echo $mod['Status'] == Antenatal::STATUS_ANTENATAL_GIVEN ? $mod['Dategiven'] : Antenatal::STATUS_ANTENATAL_PENDING ?> </td>
                                                            <td><?php echo $mod['Facility'] ?> </td>
                                                            
                                                        </tr>
                                                      <?php
                                                    endforeach;
                                                      ?>  
                                                </tbody>
                                             <?php
        else: 
            echo "<div class=\"alert alert-block alert-warning\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><i class=\"icon-remove\"></i></button>
                        <div>No data for the report!</div>
                </div>";
        endif; ?>
                                            </table>
                            </div>
                </div>
        </div>
 </div>