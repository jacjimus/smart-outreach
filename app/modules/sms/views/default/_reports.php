<?php
$grid_id = 'bulk-sms-grid';
$search_form_id = $grid_id . '-active-search-form';
?>
<div class="row grid-view-header">
        
        
            <div class="widget-header">
                <h4><?php  echo Lang::t('Search results for the date ' . $date); ?></h4>
                
        </div>
               
               
        

</div>
<?php

 
 $this->widget('application.components.widgets.GridView', array(
    'id' => $grid_id,
    'dataProvider' => $model->search(),
    'enablePagination' => $model->enablePagination,
    'enableSummary' => $model->enableSummary,
    'columns' => array(
        
        array(
            'name' => 'dest_msisdn',
            ),
        array(
            'name' => 'timestamp',
        ),
        array(
            'name' => 'text_message',
        ),
       
        array(
            'name' => 'delivery_status',
            'type' => 'raw',
            'value' => 'CHtml::tag("span", array("class"=>$data->delivery_status=="Delivered"?"badge badge-success":"badge badge-danger"), $data->delivery_status)',
        ),
            
        
        
       
    ),
));