<?php
$this->breadcrumbs = array('Messaging Module ' => 'quick',
    "Manage phonebook" => "phonebook",
    $this->pageTitle,
);
?>

<div class="widget-box transparent">
  <div class="widget-header">
        <?php echo $this->renderPartial('sms.views.default._tab') ?>
    </div>  
    <div class="widget-header">
                <h4><?php echo CHtml::encode($this->pageTitle); ?></h4>
                <div class="widget-toolbar">
                </div>
        </div>
    <?php
$grid_id = 'contacts-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Lang::t($this->pageTitle),
    'titleIcon' => null,
    'showExportButton' => false,
    'showSearch' => true,
    'createButton' => array('visible'=> false , 'url'=>'add' , 'label' => "Add contact"),
    'showRefreshButton' => false,
    'grid' => array(
        'id' => $grid_id,
        'model' => $phone,
        
        'columns' => array(
        array(
            'name' => "FirstName",
            'type' => 'raw',
            'value' => '$data->FirstName',
        ),
        array(
            'name' => "Surname",
            'type' => 'raw',
            'value' => '$data->Surname',
        ),
        array(
            'name' => "PhoneNumber",
            'type' => 'raw',
            'value' => '$data->PhoneNumber',
        ),
        array(
            'name' => "IdNo",
            'type' => 'raw',
            'value' => '$data->IdNo',
        ),
       
           
        array(
            'name' => 'Status',
            'type' => 'raw',
            'value' => 'CHtml::tag(\'span\', array(\'class\' => "badge badge-success"), $data->Status)',
        ),
       
        
        array(
            'class' => 'ButtonColumn',
            'template' => ' {delete}',
            'htmlOptions' => array('class' => 'text-left', 'style' => 'width: 100px;'),
            'buttons' => array(
                
                 'delete' => array(
                    'imageUrl' => false,
                    'label' => '<i class="icon-trash bigger-130"></i>',
                    'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->primaryKey))',
                    'visible' => '$this->grid->owner->showLink("' . UserResources::RES_SMS_PHONEBOOK . '", "' . Acl::ACTION_UPDATE . '")',
                    'options' => array(
                        'class' => 'delete red',
                        'title' => Lang::t('Delete'),
                    ),
                ),
                
            )
        ),
    ),
)
)
)
;
?>


<?php
// Diaogue for adding new custom report
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'updateDialogue',
    'options' => array(
        'title' => 'Amend SMS ',
        'autoOpen' => false,
        'modal' => false,
        'width' => 500,
        'height' => 400,
    ),
));
?>
<iframe id="update-frame" width="100%" height="100%"></iframe>
<?php $this->endWidget(); ?>



<?php
// Diaogue for approving SMS
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'approveDialogue',
    'options' => array(
        'title' => 'Approve SMS ',
        'autoOpen' => false,
        'modal' => false,
        'width' => 500,
        'height' => 400,
    ),
));
?>
<iframe id="approve-frame" width="100%" height="100%"></iframe>
<?php $this->endWidget(); ?>


<?php
// Diaogue for adding new custom report
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'previewDialogue',
    'options' => array(
        'title' => 'Preview SMS',
        'autoOpen' => false,
        'modal' => true,
        'width' => 900,
        'height' => 500,
    ),
));
?>
<iframe id="previewframe" width="100%" height="100%">
 
</iframe>
<?php $this->endWidget(); ?>
     </div>

    