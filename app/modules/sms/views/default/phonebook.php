<?php
$this->breadcrumbs = array('Messaging Module ' => 'quick',
    $this->pageTitle,
);
?>

<div class="widget-box transparent">
  <div class="widget-header">
        <?php echo $this->renderPartial('sms.views.default._tab') ?>
    </div>  
    <div class="widget-header">
                <h4><?php echo CHtml::encode($this->pageTitle); ?></h4>
                <div class="widget-toolbar">
                </div>
        </div>
    <?php
$grid_id = 'phonebook-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Lang::t($this->pageTitle),
    'titleIcon' => null,
    'showExportButton' => false,
    'showSearch' => true,
    'createButton' => array('visible'=> true , 'url'=>'add' , 'label' => "Create Phonebook"),
    'showRefreshButton' => false,
    'grid' => array(
        'id' => $grid_id,
        'model' => $phone,
        
        'columns' => array(
        array(
            'name' => "GroupName",
            'type' => 'raw',
            'value' => '$data->GroupName',
        ),
         
       array(
            'name' => 'contacts',
            'type' => 'raw',
            'value' => 'CHtml::link(CHtml::encode($data->contacts),Yii::app()->controller->createUrl("contacts",array("id"=>$data->primaryKey)))',
        ),
           
        array(
            'name' => 'Status',
            'type' => 'raw',
            'value' => 'CHtml::tag(\'span\', array(\'class\' => "badge badge-success"), $data->Status)',
        ),
       
        
        array(
            'class' => 'ButtonColumn',
            'template' => ' {view}&nbsp;&nbsp; {edit}',
            'htmlOptions' => array('class' => 'text-left', 'style' => 'width: 100px; text-align: left !important;'),
            'buttons' => array(
                
                
             
                
                'edit' => array(
                    'imageUrl' => false,
                    'label' => '<i class="icon-edit bigger-130"></i>',
                    'url' => 'Yii::app()->controller->createUrl("add",array("id"=>$data->primaryKey))',
                    'visible' => '$this->grid->owner->showLink("' . UserResources::RES_SMS_PHONEBOOK . '", "' . Acl::ACTION_UPDATE . '")',
                    'options' => array(
                        'class' => 'blue',
                        'title' => 'Update phonebook',
                    ),
                ),
                
                   
                
                
                
                 'view' => array(
                    'imageUrl' => false,
                    'label' => '<i class="icon-eye-open bigger-130"></i>',
                    'url' => 'Yii::app()->controller->createUrl("contacts",array("id"=>$data->primaryKey))',
                    'visible' => '$this->grid->owner->showLink("' . UserResources::RES_SMS_PHONEBOOK . '", "' . Acl::ACTION_VIEW . '") ',
                    'options' => array(
                        'class' => 'brown',
                        'title' => 'preview contacts',
                    ),
                ),
              
            )
        ),
    ),
)
)
)
;
?>


<?php
// Diaogue for adding new custom report
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'updateDialogue',
    'options' => array(
        'title' => 'Amend SMS ',
        'autoOpen' => false,
        'modal' => false,
        'width' => 500,
        'height' => 400,
    ),
));
?>
<iframe id="update-frame" width="100%" height="100%"></iframe>
<?php $this->endWidget(); ?>



<?php
// Diaogue for approving SMS
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'approveDialogue',
    'options' => array(
        'title' => 'Approve SMS ',
        'autoOpen' => false,
        'modal' => false,
        'width' => 500,
        'height' => 400,
    ),
));
?>
<iframe id="approve-frame" width="100%" height="100%"></iframe>
<?php $this->endWidget(); ?>


<?php
// Diaogue for adding new custom report
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'previewDialogue',
    'options' => array(
        'title' => 'Preview SMS',
        'autoOpen' => false,
        'modal' => true,
        'width' => 900,
        'height' => 500,
    ),
));
?>
<iframe id="previewframe" width="100%" height="100%">
 
</iframe>
<?php $this->endWidget(); ?>
     </div>

    