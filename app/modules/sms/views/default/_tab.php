
<div class="widget-toolbar no-border" style="float:left !important;">
    <ul class="nav nav-tabs my-nav">
        <?php if ($this->showLink(UserResources::RES_QUICK_MESSAGING)): ?>
                     <li class="<?php echo $this->activeTab === 0 ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl(ModulesEnabled::MOD_MESSAGING.'/default/quick') ?>"><i class="icon-envelope"></i> <?php echo Lang::t('Quick SMS') ?></a></li>
        <?php endif; ?>
         <?php if ($this->showLink(UserResources::RES_PROFESSIONAL_MESSAGING)): ?>
                     <li class="<?php echo $this->activeTab === 1 ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl(ModulesEnabled::MOD_MESSAGING.'/default/custome') ?>"><i class="icon-wrench"></i> <?php echo Lang::t('Customized SMS') ?></a></li>
          <?php endif; ?>
         <?php if ($this->showLink(UserResources::RES_SMS2FILE_MESSAGING)): ?>
                     <li class="<?php echo $this->activeTab === 2 ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl(ModulesEnabled::MOD_MESSAGING.'/default/file2sms') ?>"><i class="icon-folder-open"></i> <?php echo Lang::t('File2SMS') ?></a></li>
          <?php endif; ?>
               
          <?php if ($this->showLink(UserResources::RES_SMS_VERIFY)): ?>
                     <li class="<?php echo $this->activeTab === 4 ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl(ModulesEnabled::MOD_MESSAGING.'/default/verify') ?>"><i class="icon-check"></i> <?php echo Lang::t('Peer SMS Review') ?></a></li>
          <?php endif; ?>
        <?php if ($this->showLink(UserResources::RES_SMS_APPROVALS)): ?>
                     <li class="<?php echo $this->activeTab === 5 ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl(ModulesEnabled::MOD_MESSAGING.'/default/approval') ?>"><i class="icon-info-sign"></i> <?php echo Lang::t('Approve SMS') ?></a></li>
         <?php endif; ?> 
        
        <?php if ($this->showLink(UserResources::RES_SMS_PHONEBOOK)): ?>
            <li class="<?php echo $this->activeTab === 7 ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl(ModulesEnabled::MOD_MESSAGING.'/default/phonebook') ?>"><i class="icon-phone"></i> <?php echo Lang::t('Manage Phonebook') ?></a></li>
        <?php endif; ?>   
    </ul>
</div>
