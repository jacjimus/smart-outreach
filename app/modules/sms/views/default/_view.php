<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <i class="fa fa-anchor"></i> <?php echo Lang::t('SMS Details') ?>

        </h4>
    </div>
    <div id="exam_info" class="panel-collapse collapse in">
        <div class="panel-body">
            <div class="detail-view">
                <?php
                $this->widget('application.components.widgets.DetailView', array(
                    'data' => $model,
                    'attributes' => array(
                        
                        array(
                            'name' => 'Batch',
                            ),
                        array(
                            'name' => 'Recepient',
                            'type' => 'raw',
                            'visible' => $model->SmsType == Sms::QUICKSMS ? true : false,
                            ),
                        array(
                            'name' => 'grp',
                            'type' => 'raw',
                            'visible' => $model->SmsType == Sms::CUSTOMESMS ? true : false,
                            ),
                        array(
                            'name' => 'SenderID',
                            "type" => "raw",
                            'value' => Sender::model()->get($model->SenderID , "SenderID")
                            
                             ),
                        array(
                            'name' => 'Schedule',
                            'type' => 'raw',
                            'value' => date('Y-M-d' , strtotime($model->Schedule)),
                             ),
                        array(
                            'name' => 'Time',
                            'type' => 'raw',
                            'value' => date('H:i', strtotime($model->Time)),
                             ),
                        
                        array(
                            'name' => 'Body',
                            ),
                        
                                        array(
                                            'name' => 'Status',
                                            'value' => CHtml::tag('span', array('class' => $model->Status === Sms::STATUS_PENDING ? 'badge badge-warning' : 'badge badge-success'), $model->Status),
                                            'type' => 'raw',
                                        ),
                        
                )
                    )
                        );
                ?>
            </div>
        </div>
        </div>
     
</div>

