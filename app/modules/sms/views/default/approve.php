<?php

$form = $this->beginWidget('CActiveForm', array(
    'id' => 'sms-form',
    'enableAjaxValidation' => true,
    'htmlOptions' => array(
        'class' => 'form-vertical',
        'role' => 'form',
    )
        ));

?>
  
<div class="form-group">
        <?php echo $form->labelEx($model, 'Recepient', array('class' => 'col-lg-3 control-label')); ?>
    
        <div class="col-lg-9">
            <?php echo CHtml::error($model, 'Recepient') ?>
                <?php echo $model->SmsType == Sms::QUICKSMS ? 
                        $form->textArea($model, 'Recepient', array( 'value' => $model->Recepient , "class" => 'form-control' , 'readonly' =>  true, )): 

                        $form->textField($model, 'Recepient', array( 'value' =>  $model->grp,'class' => 'form-control', 'readonly' =>  true,)); ?>
</div>
</div> 
<div class="form-group">
        <?php echo $form->labelEx($model, 'Body', array('class' => 'col-lg-3 control-label')); ?>
    <small class="text-muted"  id="charNum">You have 160 characters left
</small>
        <div class="col-lg-9">
            <?php echo CHtml::error($model, 'Body') ?>
                <?php echo $form->textArea($model, 'Body', array('class' => 'form-control', 'rows' => 6 , 'cols'=>50, "readonly"=>TRUE)); ?>
            </div>
</div>

<div class="clearfix"></div>      
 
<div style="padding-top: 20px;">
        <div class="col-lg-offset-4 col-lg-8">
                <button class="btn btn-success" type="submit"><i class="icon-ok bigger-110"></i> <?php echo Lang::t('Approve') ?></button>
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                <button class="btn btn-danger" type="submit"><i class="icon-exclamation-sign bigger-110"></i> <?php echo Lang::t('Reject') ?></button>
                 </div>
</div>


<?php $this->endWidget(); ?>




    <script>
      function countChar(val) {
        var len = val.value.length;
        if (len >= 161) {
          val.value = val.value.substring(0, 160);
        } else {
            document.getElementById('charNum').value = 'You have ' + (160 - len) + ' characters left'
         }
      };
    </script>
    