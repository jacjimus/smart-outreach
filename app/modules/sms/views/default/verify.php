<?php
$this->breadcrumbs = array('Messaging Management Module ' => 'quick',
    $this->pageTitle,
);
?>
<div class="widget-box transparent">
  <div class="widget-header">
        <?php echo $this->renderPartial('sms.views.default._tab') ?>
    </div>  
    <div class="widget-header">
                 <div class="widget-toolbar">
                </div>
        </div>
        <div class="widget-body widget-body-style2">
                <div class="widget-main padding-12 no-padding-left no-padding-right">
                        <div class="tab-content padding-4">
                                
<?php

$grid_id = 'quick-sms-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Lang::t('Peer SMS Review'),
    'titleIcon' => null,
    'showExportButton' => false,
    'showSearch' => true,
    'createButton' => array('visible'=> false),
    'showRefreshButton' => false,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        
        'columns' => array(
        array(
            'name' => "User_account",
            'type' => 'raw',
            'value' => 'UsersView::model()->get($data->User_account , "name")',
        ),
         
        array(
            'name' => 'SenderID',
            'type' => 'raw',
            'value' => 'Sender::model()->get($data->SenderID , "SenderID")',
        ),
        array(
            'name' => 'Datetime',
            'type' => 'raw',
            'value' => '$data->Datetime',
        ),
        
        array(
            'name' => 'Body',
            'type' => 'raw',
            'value' => '$data->Body',
        ),
        
            array(
            'name' => 'Recepient',
            'type' => 'raw',
            'value' => '$data->SmsType == Sms::CUSTOMESMS ? $data->grp : MyYiiUtils::myShortenedString( $data->Recepient , 25 , "...",25 )',
        ),
           
        array(
            'name' => 'Status',
            'type' => 'raw',
            'value' => 'CHtml::tag(\'span\', array(\'class\' => $data->Status === Sms::STATUS_PENDING ? \'badge badge-warning\' : \'badge badge-success\'), $data->Status)',
        ),
        
        
        
        array(
            'class' => 'ButtonColumn',
            'template' => '{preview}&nbsp;&nbsp;{verify}',
            'htmlOptions' => array('class' => 'text-center', 'style' => 'width: 100px;'),
            'buttons' => array(
                
                
                'preview' => array(
                    'imageUrl' => false,
                    'label' => '<i class="icon-eye-open bigger-130"></i>',
                    'url' => 'Yii::app()->controller->createUrl("smspreview",array("id"=>$data->primaryKey,"asDialog"=>1,"gridId"=>$this->grid->id))',
                    //'click'=>'function(){$("#previewframe").attr("src",$(this).attr("href")); $("#previewDialogue").dialog("open");  return false;}',
                    'visible' => '$this->grid->owner->showLink("' . UserResources::RES_SMS_VERIFY . '", "' . Acl::ACTION_VIEW . '") AND $data->Status == "' .Sms::STATUS_PENDING. '"?true:false',
                    'options' => array(
                        'class' => 'brown show-colorbox',
                        'title' => 'preview SMSes',
                    ),
                ),
                'verify' => array(
                    'imageUrl' => false,
                    'label' => '<i class="icon-check bigger-130"></i>',
                    'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->primaryKey, "act" => "verify", "asDialog"=>1,"gridId"=>$this->grid->id))',
                    'click'=>'function(){$("#update-frame").attr("src",$(this).attr("href")); $("#updateDialogue").dialog("open");  return false;}',
                    'visible' => '$this->grid->owner->showLink("' . UserResources::RES_SMS_VERIFY . '", "' . Acl::ACTION_UPDATE . '") AND $data->Status == "' .Sms::STATUS_PENDING. '"?true:false',
                    'options' => array(
                        'class' => 'green',
                        'title' => 'Verify SMS',
                    ),
                ),
                
            )
        ),
    ),
)
)
)
;
?>


<?php
// Diaogue for adding new custom report
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'updateDialogue',
    'options' => array(
        'title' => 'Peer SMS Review',
        'autoOpen' => false,
        'modal' => false,
        'width' => 500,
        'height' => 550,
    ),
));
?>
<iframe id="update-frame" width="100%" height="100%"></iframe>
<?php $this->endWidget(); ?>


