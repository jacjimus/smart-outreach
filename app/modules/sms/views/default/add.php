<?php
$this->breadcrumbs = array('Messaging Module ' => 'quick',
    $this->pageTitle,
);
?>
<div class="widget-box transparent">
  <div class="widget-header">
        <?php echo $this->renderPartial('sms.views.default._tab') ?>
    </div>  
    <div class="widget-header">
                <h4><?php echo CHtml::encode($this->pageTitle); ?></h4>
                <div class="widget-toolbar">
                </div>
        </div>
        <div class="widget-body widget-body-style2">
                <div class="widget-main padding-12 no-padding-left no-padding-right">
                        <div class="tab-content padding-4">
                                
<?php

$form = $this->beginWidget('CActiveForm', array(
    'id' => 'phonebook-form',
    'enableAjaxValidation' => true,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
        'enctype' => 'multipart/form-data'),));

?>
   
  
<div class="form-group">
                <?php echo $form->labelEx($model, 'GroupName', array('class' => 'col-lg-3 control-label')); ?>
    <div class="col-lg-7">
             <?php
            echo $form->textField($model, 'GroupName', array('class' => "form-control"));
             ?>
                        
            <?php echo CHtml::error($model, 'GroupName') ?>
                </div>
        </div>
                            
                           
<div class="form-group">
    <span class="col-lg-8 form-line control-label">Download the Excel template and fill all the Contact details  <?php echo CHtml::link('Contacts.xlsx', "download") ?></span>
                
</div>
    <div class="form-group">
    <?php echo $form->labelEx($model, 'csv_file', array('class' => "col-lg-3 control-label", 'label' => Lang::t('Attach Csv with Contacts:'))); ?>


                <div class="col-lg-7">
                    
                    <?php echo $form->fileField($model, 'csv_file', array("class" => "btn-grey")); ?> 
                    <?php echo $form->error($model, 'csv_file'); ?>
</div>
</div>
   
  
<div class="clearfix">
        <div class="col-lg-offset-3 col-lg-9">
                <button class="btn btn-success" type="submit"><i class="icon-ok bigger-110"></i> <?php echo Lang::t('Create Phonebook') ?></button>
        </div>
</div>
<?php $this->endWidget(); ?>

                        </div>
                </div>
        </div>
            </div>
            