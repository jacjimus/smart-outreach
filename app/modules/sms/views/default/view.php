<?php
$this->breadcrumbs = array('Messaging Module ' => 'quick',
    $this->pageTitle,
);
?>
<div class="widget-box transparent">
  <div class="widget-header">
        <?php echo $this->renderPartial('sms.views.default._tab') ?>
    </div>  
    <div class="widget-header">
                <h4><?php echo CHtml::encode($this->pageTitle); ?></h4>
                <div class="widget-toolbar">
                </div>
        </div>
        <div class="widget-body widget-body-style2">
                <div class="widget-main padding-12 no-padding-left no-padding-right">
                        <div class="tab-content padding-4">
                                <?php $this->renderPartial('_view', array('model' => $model)); ?>
                        </div>
                </div>
        </div>
</div>
