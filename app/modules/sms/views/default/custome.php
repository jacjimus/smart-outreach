<?php
$this->breadcrumbs = array('Messaging Module ' => 'quick',
    $this->pageTitle,
);

?>

<div class="widget-box transparent">
    <div class="widget-header">
        <?php echo $this->renderPartial('sms.views.default._tab', array('id' => $id)) ?>
    </div>  
    <div class="widget-header">
        <h4><?php echo CHtml::encode($this->pageTitle); ?></h4>
        <div class="widget-toolbar">
        </div>
    </div>
    <div class="widget-body widget-body-style2">
        <div class="widget-main padding-12 no-padding-left no-padding-right">

<?php
 if($_nt):
            
        $comment = Notification::model()->get($_nt , "Message");
        $user = UsersView::model()->get(Notification::model()->get($_nt , "User_account") , "name");
          echo "<div class=\"alert alert-block alert-danger clearfix\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><i class=\"icon-remove\"></i></button>
                        <div><span class='text-muted Italic'>Reviewed by $user:</span>   '$comment'</div>
                </div>" ; 
           
        endif; 
        ?>

            <div class="tab-content padding-4">
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'quick-sms-form',
                    'enableAjaxValidation' => false,
                    'htmlOptions' => array(
                        'class' => 'form-horizontal',
                        'role' => 'form',
                    )
                ));
                ?>

                <div class="form-group">
<?php echo $form->labelEx($model, 'Recepient', array('class' => 'col-lg-3 control-label')); ?>
                    <div class="col-lg-5">
                    <?php
                    $data = Phonebook::model()->findAll();
                    $return = array();

                    foreach ($data as $d)
                        $return[$d->PbID] = $d->GroupName;

                    echo $form->dropDownList($model, 'Recepient', $return, array('multiple' => 'multiple', 'class' => 'col-lg-8 chosen-select',
                        "id" => "recepient_id" , 'required' => true));
                    ?>
                        <?php echo CHtml::error($model, 'Recepient') ?>
                    </div>
                </div> 

                <div class="form-group">
                    <?php echo $form->labelEx($model, 'SenderID', array('class' => 'col-lg-3 control-label')); ?>
                    <div class="col-lg-5">
                        <?php echo $form->textField($model, 'SenderID', array('class' => "form-control", "value" => Sender::model()->get(Accounts::model()->get(Yii::app()->user->account, "Sender_id"), "SenderID"), 'readonly' => true));
                        ?>

                        <?php echo CHtml::error($model, 'SenderID') ?>
                    </div>
                </div>
                
                <div class="form-group">
                <?php echo $form->labelEx($model, 'Schedule', array('class' => 'col-lg-3 control-label')); ?>
                <div class="col-lg-5">
             <?php echo $form->textField($model, 'Schedule', array('class' => "form-control " , 'id'=> 'datepicker' , 'value' => date('Y-m-d')));
              ?>
                        
            <?php echo CHtml::error($model, 'Schedule') ?>
                </div>
 </div>
<div class="form-group">
                <?php echo $form->labelEx($model, 'Time', array('class' => 'col-lg-3 control-label')); ?>
                <div class="col-lg-5">
            <?php echo  CHtml::textField('Time', date('h:i'), array('class'=>'timepicker',  'style' => 'width:150px')); ?>
                       
            <?php echo CHtml::error($model, 'Time') ?>
                </div>
 </div>
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'Body', array('class' => 'col-lg-3 control-label')); ?>
                    <small class="text-muted"  id="charNum">You have 160 characters left
                    </small>

                    <div class="col-lg-8">
                        <?php echo CHtml::error($model, 'Body') ?>
                        <?php echo $form->textArea($model, 'Body', array('class' => 'col-lg-7', 'rows' => 8, 'cols' => 50, "onkeyup" => "countChar(this)", "id" => "sms-body", 'placeholder' => "Ensure cursor is in the text area first before adding any placeholder", 'required' => true)); ?>
                        <?php
                        $data = Yii::app()->db->schema->getTable('tbl_phone_numbers')->columns;
                        $return = array();
//foreach (array_slice($columns,0,3) as $d)
                        foreach ($data as $d)
                            if ($d->comment != "")
                                $return[$d->name] = $d->comment;

                        echo CHtml::dropDownList('Params', '', $return, array('prompt' => '[Append Value to SMS]', 'class' => 'col-lg-offset-1 col-lg-4', "id" => "sms-custome"
                        ));
                        ?>
                        <br />
                        <small class=" col-lg-offset-1 col-lg-4 text-muted" >Select  Value to append to SMS</small>
                        <br /><br />
                        <a class="col-lg-offset-1 col-lg-4 hide" id="view" href="#" style = 'cursor: pointer;' onclick = "{
                                    preview();
                                    $('#preview').dialog('open');
                                }">
                                &nbsp;&nbsp;&nbsp;&nbsp;<i class="icon-eye-open"></i>  <?php echo "&nbsp;" . Lang::t('Preview Sms') ?></a>    
                    
                    </div>
                </div>

<div class="form-group">
        <?php echo $form->labelEx($model, 'Signature', array('class' => 'col-lg-3 control-label')); ?>
    
        <div class="col-lg-5">
            <?php echo CHtml::error($model, 'Signature') ?>
                <?php
                    $datas = Signatures::model()->findAll();
                    $sigs = array();

                    foreach ($datas as $ds)
                        $sigs[$ds->id] = $ds->name;

                    echo $form->dropDownList($model, 'Signature', $sigs, array('prompt' => '[No Footer]',  'class' => 'forn-control chosen-select', 'id' => "signature"
                        ));
                    ?>
            <span id="loading"></span>
            </div>
</div>

        


                <div class="clearfix form-actions">
                    <div class="col-lg-offset-2 col-lg-9">
                        <button class="btn btn-primary" type="submit"><i class="icon-ok bigger-110"></i> <?php echo Lang::t('submit for peer review') ?></button>
                        &nbsp; &nbsp; &nbsp;
                    </div>
                </div>
                <?php $this->endWidget(); ?>


                <script>
                    function countChar(val) {
                        var len = val.value.length;
                        if (len >= 161) {
                            val.value = val.value.substring(0, 160);
                        } else {
                            $('#charNum').html('You have ' + (160 - len) + ' characters left');
                        }
                    }
                    ;
                </script>

            </div>
        </div>
    </div>
</div>



<?php
// Diaogue for adding new custom report
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogAccount',
    'options' => array(
        'title' => 'Create a Phonebook',
        'autoOpen' => false,
        'show'=>array(

	                'effect'=>'blind',

	                'duration'=>1000,

	            ),

	        'hide'=>array(

	                'effect'=>'blind',

	                'duration'=>500),
        'modal' => false,
        'width' => 500,
        'height' => 300,
    ),
));
?>
<div class="divAcc"></div>

<?php $this->endWidget(); ?>



<?php
// Diaogue for adding new custom report
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'preview',
    'options' => array(
        'title' => 'Sample Sms Preview',
        'autoOpen' => false,
        'modal' => false,
        
	        'show'=>array(

	                'effect'=>'blind',

	                'duration'=>1000,

	            ),

	        'hide'=>array(

	                'effect'=>'blind',

	                'duration'=>500,

	            ),   
        'width' => 500,
        'height' => 250,
    ),
));
?>
<div class="preview"></div>

<?php $this->endWidget(); ?>

<script type="text/javascript">


    function addAcc()
    {
<?php
echo CHtml::ajax(array(
    'url' => array("default/phonebook"),
    'data' => "js:$(this).serialize()",
    'type' => 'post',
    'dataType' => 'json',
    'success' => "function(data)
            {
                if (data.status == 'failure')
                {
                    $('#dialogAccount div.divAcc').html(data.div);
                          // Here is the trick: on submit-> once again this function!
                    $('#dialogAccount div.divAcc form').submit(addAcc);
                }
                else
                {
                    $('#dialogAccount div.divAcc').html(data.div);
                    setTimeout(\"$('#dialogAccount').dialog('close') \",400);
                    $('#Recepients').append(data.options);
                }
 
            } ",
))
?>;
        return false;

    }
    
    
    function preview()
    {
<?php
echo CHtml::ajax(array(
    'url' => array("default/preview"),
    'data' => "js:$('#quick-sms-form').serialize()",
    'type' => 'post',
    'dataType' => 'json',
    'success' => "function(data)
            {
                if (data.status == 'failure')
                {
                    $('#preview div.preview').html(data.div);
                          // Here is the trick: on submit-> once again this function!
                    $('#preview div.preview form').submit(preview);
                }
                else
                {
                    $('#preview div.preview').html(data.div);
                    setTimeout(\"$('#preview').dialog('close') \",400);
                    
                }
 
            } ",
))
?>;
        return false;

    }


    var $textBox;

    $textBox = $("#sms-body");

    function saveSelection() {
        $textBox.data("lastSelection", $textBox.getSelection());
    }

    $textBox.focusout(saveSelection);

    $textBox.bind("beforedeactivate", function () {
        saveSelection();
        $textBox.unbind("focusout");
    });


    $('#sms-custome').on('change', function (e) {
        e.preventDefault();
        var optionSelected = $(this);
        var valueSelected = " {" + optionSelected.val() + "} ";
        //document.getElementById('sms-body').value = $('#sms-body').val() + valueSelected;

        var selection = $textBox.data("lastSelection");
        $textBox.focus();
        $textBox.setSelection(selection.start, selection.end);
        $textBox.replaceSelectedText(valueSelected);

    });
    $('#sms-body').on('keypress', function () {
       var valu = $(this);
         if(valu != "")
             $("#view").removeClass("hide");
         else
             $("#view").addClass("hide");

    });


$('#signature').on('change', function (e) {
        e.preventDefault();
        var num = $(this).val();
        var optionSelected = "value=" + num;
        $('#loading').html("<img src= '<?php echo Yii::app()->request->baseUrl ?>/themes/default/images/ajax-loader.gif '/>");
        $.ajax
                ({
                    type: "POST",
                    url: "addsig",
                    data: optionSelected,
                    cache: false,
                    success: function(html)
                    {
                        $("#sms-body").val($("#sms-body").val() + html);
                         $('#loading').html("");
                    }
                });

    });
</script>



<script>
    /*
     Rangy Text Inputs, a cross-browser textarea and text input library plug-in for jQuery.
     
     Part of Rangy, a cross-browser JavaScript range and selection library
     http://code.google.com/p/rangy/
     
     Depends on jQuery 1.0 or later.
     
     Copyright 2010, Tim Down
     Licensed under the MIT license.
     Version: 0.1.205
     Build date: 5 November 2010
     */
    (function (n) {
        function o(e, g) {
            var a = typeof e[g];
            return a === "function" || !!(a == "object" && e[g]) || a == "unknown"
        }
        function p(e, g, a) {
            if (g < 0)
                g += e.value.length;
            if (typeof a == "undefined")
                a = g;
            if (a < 0)
                a += e.value.length;
            return{start: g, end: a}
        }
        function k() {
            return typeof document.body == "object" && document.body ? document.body : document.getElementsByTagName("body")[0]
        }
        var i, h, q, l, r, s, t, u, m;
        n(document).ready(function () {
            function e(a, b) {
                return function () {
                    var c = this.jquery ? this[0] : this, d = c.nodeName.toLowerCase();
                    if (c.nodeType ==
                            1 && (d == "textarea" || d == "input" && c.type == "text")) {
                        c = [c].concat(Array.prototype.slice.call(arguments));
                        c = a.apply(this, c);
                        if (!b)
                            return c
                    }
                    if (b)
                        return this
                }
            }
            var g = document.createElement("textarea");
            k().appendChild(g);
            if (typeof g.selectionStart != "undefined" && typeof g.selectionEnd != "undefined") {
                i = function (a) {
                    return{start: a.selectionStart, end: a.selectionEnd, length: a.selectionEnd - a.selectionStart, text: a.value.slice(a.selectionStart, a.selectionEnd)}
                };
                h = function (a, b, c) {
                    b = p(a, b, c);
                    a.selectionStart = b.start;
                    a.selectionEnd =
                            b.end
                };
                m = function (a, b) {
                    if (b)
                        a.selectionEnd = a.selectionStart;
                    else
                        a.selectionStart = a.selectionEnd
                }
            } else if (o(g, "createTextRange") && typeof document.selection == "object" && document.selection && o(document.selection, "createRange")) {
                i = function (a) {
                    var b = 0, c = 0, d, f, j;
                    if ((j = document.selection.createRange()) && j.parentElement() == a) {
                        f = a.value.length;
                        d = a.value.replace(/\r\n/g, "\n");
                        c = a.createTextRange();
                        c.moveToBookmark(j.getBookmark());
                        j = a.createTextRange();
                        j.collapse(false);
                        if (c.compareEndPoints("StartToEnd", j) >
                                -1)
                            b = c = f;
                        else {
                            b = -c.moveStart("character", -f);
                            b += d.slice(0, b).split("\n").length - 1;
                            if (c.compareEndPoints("EndToEnd", j) > -1)
                                c = f;
                            else {
                                c = -c.moveEnd("character", -f);
                                c += d.slice(0, c).split("\n").length - 1
                            }
                        }
                    }
                    return{start: b, end: c, length: c - b, text: a.value.slice(b, c)}
                };
                h = function (a, b, c) {
                    b = p(a, b, c);
                    c = a.createTextRange();
                    var d = b.start - (a.value.slice(0, b.start).split("\r\n").length - 1);
                    c.collapse(true);
                    if (b.start == b.end)
                        c.move("character", d);
                    else {
                        c.moveEnd("character", b.end - (a.value.slice(0, b.end).split("\r\n").length -
                                1));
                        c.moveStart("character", d)
                    }
                    c.select()
                };
                m = function (a, b) {
                    var c = document.selection.createRange();
                    c.collapse(b);
                    c.select()
                }
            } else {
                k().removeChild(g);
                window.console && window.console.log && window.console.log("TextInputs module for Rangy not supported in your browser. Reason: No means of finding text input caret position");
                return
            }
            k().removeChild(g);
            l = function (a, b, c, d) {
                var f;
                if (b != c) {
                    f = a.value;
                    a.value = f.slice(0, b) + f.slice(c)
                }
                d && h(a, b, b)
            };
            q = function (a) {
                var b = i(a);
                l(a, b.start, b.end, true)
            };
            u = function (a) {
                var b =
                        i(a), c;
                if (b.start != b.end) {
                    c = a.value;
                    a.value = c.slice(0, b.start) + c.slice(b.end)
                }
                h(a, b.start, b.start);
                return b.text
            };
            r = function (a, b, c, d) {
                var f = a.value;
                a.value = f.slice(0, c) + b + f.slice(c);
                if (d) {
                    b = c + b.length;
                    h(a, b, b)
                }
            };
            s = function (a, b) {
                var c = i(a), d = a.value;
                a.value = d.slice(0, c.start) + b + d.slice(c.end);
                c = c.start + b.length;
                h(a, c, c)
            };
            t = function (a, b, c) {
                var d = i(a), f = a.value;
                a.value = f.slice(0, d.start) + b + d.text + c + f.slice(d.end);
                b = d.start + b.length;
                h(a, b, b + d.length)
            };
            n.fn.extend({getSelection: e(i, false), setSelection: e(h,
                        true), collapseSelection: e(m, true), deleteSelectedText: e(q, true), deleteText: e(l, true), extractSelectedText: e(u, false), insertText: e(r, true), replaceSelectedText: e(s, true), surroundSelectedText: e(t, true)})
        })
    })(jQuery);
</script>


<?php
Yii::app()->clientScript
        ->registerCssFile(Yii::app()->theme->baseUrl . '/plugins/chosen/chosen.min.css')
        ->registerScriptFile(Yii::app()->theme->baseUrl . '/plugins/chosen/chosen.jquery.min.js', CClientScript::POS_END)
        ->registerScript('recepient_id', "$('.chosen-select').chosen();");
?>