<?php
$model = Sms::model()->loadModel($id);
$recepient = $model->Recepient;
$sender = Sender::model()->get($model->SenderID , "SenderID");
$sms = $model->Body;
$time = $model->Schedule;
$sms_type = $model->SmsType;

?>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <th>Recepient</th>
    <th>SenderID</th>
    <th>Message</th>
    <th>Send Time</th>
    </thead>
    <tbody>
       <?php
       if($sms_type == Sms::CUSTOMESMS):
           $recep = explode(",", $recepient);
           $phones = Contacts::model()->findAll("PhoneBookID = '$recep[0]' LIMIT 10");
        foreach($phones As $p):
            echo "<tr>";
                 echo "<td>";
                 echo $p->PhoneNumber;
                 echo "</td>";
                 echo "<td>";
                 echo Sender::model()->get($model->SenderID , "SenderID");
                 echo "</td>";
                 echo "<td>";
                 echo Sms::processSms($sms, $p->PhoneNumber)['sms'];
                 echo "</td>";
                 echo "<td>";
                 echo $time;
                 echo "</td>";
            echo "</tr>";
        endforeach;
        
        elseif($sms_type == Sms::QUICKSMS):
            $phones = explode(",", $recepient);
            foreach($phones As $p):
            echo "<tr>";
                 echo "<td>";
                 echo $p;
                 echo "</td>";
                 echo "<td>";
                 echo Sender::model()->get($model->SenderID , "SenderID");
                 echo "</td>";
                 echo "<td>";
                 echo $sms;
                 echo "</td>";
                 echo "<td>";
                 echo $time;
                 echo "</td>";
            echo "</tr>";
        endforeach;
       endif;
       ?> 
    </tbody>
</table>
 