
<div class="widget-toolbar no-border" style="float:left !important;">
    <ul class="nav nav-tabs my-nav">
       <li class="<?php echo $this->activeTab === 0 ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl(ModulesEnabled::MOD_MESSAGING.'/verify/smslogs') ?>"><i class="icon-envelope"></i> <?php echo Lang::t('All sms logs') ?></a></li>
       <li class="<?php echo $this->activeTab === 1 ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl(ModulesEnabled::MOD_MESSAGING.'/verify/successful') ?>"><i class="icon-check-sign"></i> <?php echo Lang::t('successful deliveries') ?></a></li>
       <li class="<?php echo $this->activeTab === 2 ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl(ModulesEnabled::MOD_MESSAGING.'/verify/failed') ?>"><i class="icon-exclamation-sign"></i> <?php echo Lang::t('failed sms logs') ?></a></li>
          
    </ul>
</div>
