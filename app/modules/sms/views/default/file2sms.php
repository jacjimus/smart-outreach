<?php
$this->breadcrumbs = array('Messaging Module ' => 'quick',
    $this->pageTitle,
);

?>
<div class="widget-box transparent">
<div class="widget-header">
        <?php echo $this->renderPartial('sms.views.default._tab') ?>
    </div> 
</div>
<div class="row">
                            <div class="col-xs-12">
                                
                                <div class="hr hr-18 hr-double dotted"></div>

                                <div class="widget-box">
                                    <div class="widget-header widget-header-blue widget-header-flat">
                                        <h4 class="widget-title lighter"><?php echo $this->pageTitle?> Wizard</h4>

                                        <div class="widget-toolbar">
                                            <label>
                                                <small class="green">
                                                    <b>Validation</b>
                                                </small>

                                                <input id="skip-validation" type="checkbox" class="ace ace-switch ace-switch-4" />
                                                <span class="lbl middle"></span>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <div id="fuelux-wizard-container">
                                                <div>
                                                    <ul class="steps">
                                                        <li data-step="1" class="<?php echo $st == 1 ? "active" : "complete" ?>">
                                                            <span class="step">1</span>
                                                            <span class="title">CSV File Upload:</span>
                                                            </li> 

                                                        <li data-step="2" class="<?php  if($st == 2) echo "active"; elseif($st > 2) echo "complete"; ?>">
                                                            <span class="step">2</span>
                                                            <span class="title">Verification:</span>
                                                        </li>

                                                        <li data-step="3" class="<?php  if($st == 3) echo "active"; elseif($st > 3) echo "complete"; ?>">
                                                            <span class="step">3</span>
                                                            <span class="title">Column Matching</span>
                                                        </li>

                                                        <li data-step="4" class="<?php  if($st == 4) echo "active"; elseif($st > 4) echo "complete"; ?>">
                                                            <span class="step">4</span>
                                                            <span class="title">Confirm and Send</span>
                                                        </li>
                                                    </ul>
                                                </div>

                                                <hr />

                                                <div class="step-content pos-rel">
                                                    <div class="step-pane active" data-step="1">
                                                        
                                                        <h3 class="lighter block green">
                                                            <?php
                                                             switch ($st)
                                                             {
                                                                 case 2:
                                                                     echo "Verify uploaded records";
                                                                     break;
                                                                 case 3:
                                                                     echo "Match columns to the SMS content";
                                                                     break;
                                                                 case 4:
                                                                     echo "Preview SMS & Send";
                                                                     break;
                                                                 default:
                                                                     echo "Attach a CSV File with records:";
                                                                     break;
                                                             }
                                                            ?>
                                                        </h3>

                            <?php

                            if($st == 1):
                                    $form = $this->beginWidget('CActiveForm', array(
                                        'id' => 'file2sms-form',
                                        'enableAjaxValidation' => false,
                                        'htmlOptions' => array(
                                            'class' => 'form-horizontal',
                                            'role' => 'form',
                                            'enctype' => 'multipart/form-data'
                                        )
                                     ));
                            ?>
                                                        <div class="form-group has-warning">
                                                           
                                                             <div class="col-lg-offset-2 col-lg-7">


                                                                <?php echo $form->fileField($model, 'csv_file', array("class" => "btn btn-primary")); ?> 
                                                                <?php echo $form->error($model, 'csv_file'); ?>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer wizard-actions">
                                                <button class="btn btn-sm btn-prev">
                                                    <i class="ace-icon fa fa-arrow-left"></i>
                                                    Prev
                                                </button>

                                                            <button type="submit" class="btn btn-success btn-sm btn-next" data-last="Finish">
                                                    Next
                                                    <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                                                </button>

                                                <button class="btn btn-danger btn-sm pull-left" data-dismiss="modal">
                                                    <i class="ace-icon fa fa-times"></i>
                                                    Cancel
                                                </button>
                                            </div>

                                         <?php $this->endWidget(); 
                           elseif ($st ==2 ): 
                                   $i = 0;
                                        //var_dump($aData);die;
                                        ?>
                                        
                                                        <table class="table table-responsive">
                                                            <thead>
                                                                <?php for ( $count = 0; $count < count($aData['titles']); $count++):?>
                                                                
                                                            <th><?php echo ($aData["titles"][$count]);?></th>
                                                            <?php endfor; ?>  
                                                              </thead>  
                                                            <tbody>
                                                                <?php foreach ($aData As $rows):?>
                                                                <tr>
                                                                    <?php for ( $col = 0; $col < count($aData['titles']) ; $col++):?>
                                                                      <?php if($i < count($aData) - 1) {?>  
                                                                    <td><?php echo $aData[$i][$col]; ?> </td>
                                                                    
                                                                      <?php }
                                                                     endfor;
                                                                    $i++;
                                                                    ?>
                                                                </tr>
                                                                   <?php endforeach; ?>  
                                                            </tbody>

                                                                  
                                                            
                                                            
                                                        </table>                    
                                   <?php $form = $this->beginWidget('CActiveForm', array(
                                        'id' => 'match-form',
                                        'enableAjaxValidation' => false,
                                        'htmlOptions' => array(
                                            'class' => 'form-horizontal',
                                            'role' => 'form',
                                            
                                        )
                                     )); ?>
                                     <?php echo CHtml::hiddenField('csv_file',serialize($aData)); ?> 
                                                               
                                     <div class="modal-footer wizard-actions">
                                                <button class="btn btn-sm btn-prev">
                                                    <i class="ace-icon fa fa-arrow-left"></i>
                                                    Prev
                                                </button>

                                         <button type="submit" class="btn btn-success btn-sm btn-next" data-last="Finish" name="to_third">
                                                    Next
                                                    <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                                                </button>

                                                <button class="btn btn-danger btn-sm pull-left" data-dismiss="modal">
                                                    <i class="ace-icon fa fa-times"></i>
                                                    Cancel
                                                </button>
                                            </div>                      
                                   <?php $this->endWidget(); ?>
<?php  elseif($st == 3): 
                                                   
                       $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'sms-form',
                    'enableAjaxValidation' => false,
                    'htmlOptions' => array(
                        'class' => 'form-horizontal',
                        'role' => 'form',
                    )
                ));
                ?>
                 <?php echo CHtml::hiddenField('csv_file',serialize($aData)); ?> 
                <div class="form-group">
                <?php echo CHtml::label("Mobile No. Column", 'Recepient', array('class' => 'col-lg-3 control-label')); ?>
                    <div class="col-lg-5">
                    <?php
                    $data = $aData['titles'];
                    $return = array();

                    foreach ($data as $d)
                        $return[$d] = $d;

                    echo $form->dropDownList($model, 'Recepient', $return, array('prompt' => '[Select CSV Column]','class' => 'col-lg-8 chosen-select',
                        "id" => "recepient_id") );
                    ?>
                        <?php echo CHtml::error($model, 'Recepient') ?>
                    </div>
                </div> 

                <div class="form-group">
                    <?php echo $form->labelEx($model, 'SenderID', array('class' => 'col-lg-3 control-label')); ?>
                    <div class="col-lg-5">
                        <?php echo $form->textField($model, 'SenderID', array('class' => "form-control", "value" => Sender::model()->get(Accounts::model()->get(Yii::app()->user->account, "Sender_id"), "SenderID"), 'readonly' => true));
                        ?>

                        <?php echo CHtml::error($model, 'SenderID') ?>
                    </div>
                </div>
                
                <div class="form-group">
                <?php echo $form->labelEx($model, 'Schedule', array('class' => 'col-lg-3 control-label')); ?>
                <div class="col-lg-5">
             <?php echo $form->textField($model, 'Schedule', array('class' => "form-control " , 'id'=> 'datepicker' , 'value' => date('Y-m-d')));
              ?>
                        
            <?php echo CHtml::error($model, 'Schedule') ?>
                </div>
 </div>
<div class="form-group">
                <?php echo $form->labelEx($model, 'Time', array('class' => 'col-lg-3 control-label')); ?>
                <div class="col-lg-5">
            <?php echo  CHtml::textField('Time', date('h:i'), array('class'=>'timepicker',  'style' => 'width:150px')); ?>
                       
            <?php echo CHtml::error($model, 'Time') ?>
                </div>
 </div>
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'Body', array('class' => 'col-lg-3 control-label')); ?>
                    <small class="text-muted"  id="charNum">You have 160 characters left
                    </small>

                    <div class="col-lg-8">
                        <?php echo CHtml::error($model, 'Body') ?>
                        <?php echo $form->textArea($model, 'Body', array('class' => 'col-lg-7', 'rows' => 8, 'cols' => 50, "onkeyup" => "countChar(this)", "id" => "sms-body", 'placeholder' => "Ensure cursor is in the text area first before adding any placeholder")); ?>
                        <?php
                        $data = $aData['titles'];
                        $return = array();
//foreach (array_slice($columns,0,3) as $d)
                        foreach ($data as $d)
                            
                                $return[$d] = $d;

                        echo CHtml::dropDownList('Params', '', $return, array('prompt' => '[Append Value to SMS]', 'class' => 'col-lg-offset-1 col-lg-4', "id" => "sms-custome"
                        ));
                        ?>
                        <br />
                        <small class=" col-lg-offset-1 col-lg-4 text-muted" >Select  Value to append to SMS</small>
                        <br /><br />
                        <a class="col-lg-offset-1 col-lg-4 hide" id="view" href="#" style = 'cursor: pointer;' onclick = "{
                                    preview();
                                    $('#preview').dialog('open');
                                }">
                                &nbsp;&nbsp;&nbsp;&nbsp;<i class="icon-eye-open"></i>  <?php echo "&nbsp;" . Lang::t('Preview Sms') ?></a>    
                    
                    </div>
                </div>

<div class="form-group">
        <?php echo $form->labelEx($model, 'Signature', array('class' => 'col-lg-3 control-label')); ?>
    
        <div class="col-lg-5">
            <?php echo CHtml::error($model, 'Signature') ?>
                <?php
                    $datas = Signatures::model()->findAll();
                    $sigs = array();

                    foreach ($datas as $ds)
                        $sigs[$ds->id] = $ds->name;

                    echo $form->dropDownList($model, 'Signature', $sigs, array('prompt' => '[No Footer]',  'class' => 'forn-control chosen-select', 'id' => "signature"
                        ));
                    ?>
            <span id="loading"></span>
            </div>
</div>
                       <div class="modal-footer wizard-actions">
                                                <button type="submit" class="btn btn-sm btn-prev"  name="to_second">
                                                    <i class="ace-icon fa fa-arrow-left"></i>
                                                    Prev
                                                </button>

                                               <button type="submit" class="btn btn-success btn-sm btn-next" data-last="Finish" name="to_last">
                                                    Next
                                                    <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                                                </button>

                                                <button class="btn btn-danger btn-sm pull-left" data-dismiss="modal">
                                                    <i class="ace-icon fa fa-times"></i>
                                                    Cancel
                                                </button>
                                            </div>                                     
                                                        
                           <?php $this->endWidget();                            
                                   elseif ($st == 4 ): 
                                   $i = 0;
                                          ?>
                                        
                                                        <table class="table table-responsive">
                                                            <thead>
                                                               
                                                            <th>Recepient</th>
                                                            <th>Sms Body</th>
                                                            <th>Send Date</th>
                                                            <th>Send Time</th>
                                                            <th>Sender ID</th>
                                                            </thead>  
                                                            <tbody>
                                                                <?php foreach ($aData As $rows):?>
                                                            <?php  $key = array_search($model->Recepient, $aData['titles']); ?>
                                                                <tr>
                                                                    <?php 
                                              
                                                                    if($i < count($aData) - 1) {?>  
                                                                    <td><?php echo $this->preparePhoneNo($aData[$i][$key]); ?> </td>
                                                                    <td><?php $this->kaziNgumuSana($model->Body , $aData, $i ) ?> </td>
                                                                    <td><?php echo $model->Schedule ?> </td>
                                                                    <td><?php echo $model->Time ?> </td>
                                                                    <td><?php echo $model->SenderID ?> </td>
                                                                    
                                                                      
                                                                      <?php
                                                                      }
                                                                      $i++;
                                                                      endforeach;?>
                                                                    
                                                                    
                                                                </tr>
                                                                  
                                                            </tbody>

                                                                  
                                                            
                                                            
                                                        </table>                    
                                   <?php $form = $this->beginWidget('CActiveForm', array(
                                        'id' => 'match-form',
                                        'enableAjaxValidation' => false,
                                        'htmlOptions' => array(
                                            'class' => 'form-horizontal',
                                            'role' => 'form',
                                            
                                        )
                                     )); 
                                   
      
                                   ?>
                                     <?php echo $form->hiddenField($model, 'Body'); ?> 
                                                        
                                     <?php echo CHtml::hiddenField('csv_file',serialize($aData)); ?> 
                                                               
                                     <div class="modal-footer wizard-actions">
                                         <button type="submit" class="btn btn-sm btn-prev" name="to_last">
                                                    <i class="ace-icon fa fa-arrow-left"></i>
                                                    Prev
                                                </button>

                                         <button type="submit" class="btn btn-success btn-sm btn-next" data-last="Finish" name="to_save">
                                                    Submit SMS for Reviewal
                                                    <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                                                </button>

                                                <button class="btn btn-danger btn-sm pull-left" data-dismiss="modal">
                                                    <i class="ace-icon fa fa-times"></i>
                                                    Cancel
                                                </button>
                                            </div>                      
                                   <?php $this->endWidget(); ?>                     
                                                        
                                   <?php
                                   else: ?>
                                   <div class="alert alert-block alert-success">
                                  <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>
                                 <div>Sms submitted successfully for reviewal</div>
                </div>
                                   
                                <?php endif; ?> 
                                                    
                                                </div>
                                            </div>

                                        </div><!-- /.widget-main -->
                                    </div><!-- /.widget-body -->
                                </div>

                                
                            </div><!-- /.col -->
                        </div><!-- /.row -->
     
                  
        
<?php
Yii::app()->clientScript
        ->registerCssFile(Yii::app()->theme->baseUrl . '/css/steps.css')
        ->registerCssFile(Yii::app()->theme->baseUrl . '/plugins/chosen/chosen.min.css')
        ->registerScriptFile(Yii::app()->theme->baseUrl . '/plugins/chosen/chosen.jquery.min.js', CClientScript::POS_END)
        ->registerScript('recepient_id', "$('.chosen-select').chosen();");
        
        
         ?>

 
                        
                <script>
                    function countChar(val) {
                        var len = val.value.length;
                        if (len >= 161) {
                            val.value = val.value.substring(0, 160);
                        } else {
                            $('#charNum').html('You have ' + (160 - len) + ' characters left');
                        }
                    }
                    ;
                </script>

            </div>
        </div>
    </div>
</div>



<?php
// Diaogue for adding new custom report
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogAccount',
    'options' => array(
        'title' => 'Create a Phonebook',
        'autoOpen' => false,
        'show'=>array(

	                'effect'=>'blind',

	                'duration'=>1000,

	            ),

	        'hide'=>array(

	                'effect'=>'blind',

	                'duration'=>500),
        'modal' => false,
        'width' => 500,
        'height' => 300,
    ),
));
?>
<div class="divAcc"></div>

<?php $this->endWidget(); ?>



<?php
// Diaogue for adding new custom report
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'preview',
    'options' => array(
        'title' => 'Sample Sms Preview',
        'autoOpen' => false,
        'modal' => false,
        
	        'show'=>array(

	                'effect'=>'blind',

	                'duration'=>1000,

	            ),

	        'hide'=>array(

	                'effect'=>'blind',

	                'duration'=>500,

	            ),   
        'width' => 500,
        'height' => 250,
    ),
));
?>
<div class="preview"></div>

<?php $this->endWidget(); ?>

<script type="text/javascript">


    function addAcc()
    {
<?php
echo CHtml::ajax(array(
    'url' => array("default/phonebook"),
    'data' => "js:$(this).serialize()",
    'type' => 'post',
    'dataType' => 'json',
    'success' => "function(data)
            {
                if (data.status == 'failure')
                {
                    $('#dialogAccount div.divAcc').html(data.div);
                          // Here is the trick: on submit-> once again this function!
                    $('#dialogAccount div.divAcc form').submit(addAcc);
                }
                else
                {
                    $('#dialogAccount div.divAcc').html(data.div);
                    setTimeout(\"$('#dialogAccount').dialog('close') \",400);
                    $('#Recepients').append(data.options);
                }
 
            } ",
))
?>;
        return false;

    }
    
    
    function preview()
    {
<?php
echo CHtml::ajax(array(
    'url' => array("default/preview"),
    'data' => "js:$('#quick-sms-form').serialize()",
    'type' => 'post',
    'dataType' => 'json',
    'success' => "function(data)
            {
                if (data.status == 'failure')
                {
                    $('#preview div.preview').html(data.div);
                          // Here is the trick: on submit-> once again this function!
                    $('#preview div.preview form').submit(preview);
                }
                else
                {
                    $('#preview div.preview').html(data.div);
                    setTimeout(\"$('#preview').dialog('close') \",400);
                    
                }
 
            } ",
))
?>;
        return false;

    }


    var $textBox;

    $textBox = $("#sms-body");

    function saveSelection() {
        $textBox.data("lastSelection", $textBox.getSelection());
    }

    $textBox.focusout(saveSelection);

    $textBox.bind("beforedeactivate", function () {
        saveSelection();
        $textBox.unbind("focusout");
    });


    $('#sms-custome').on('change', function (e) {
        e.preventDefault();
        var optionSelected = $(this);
        var valueSelected = " {" + optionSelected.val() + "} ";
        //document.getElementById('sms-body').value = $('#sms-body').val() + valueSelected;

        var selection = $textBox.data("lastSelection");
        $textBox.focus();
        $textBox.setSelection(selection.start, selection.end);
        $textBox.replaceSelectedText(valueSelected);

    });
    $('#sms-body').on('keypress', function () {
       var valu = $(this);
         if(valu != "")
             $("#view").removeClass("hide");
         else
             $("#view").addClass("hide");

    });


$('#signature').on('change', function (e) {
        e.preventDefault();
        var num = $(this).val();
        var optionSelected = "value=" + num;
        $('#loading').html("<img src= '<?php echo Yii::app()->request->baseUrl ?>/themes/default/images/ajax-loader.gif '/>");
        $.ajax
                ({
                    type: "POST",
                    url: "addsig",
                    data: optionSelected,
                    cache: false,
                    success: function(html)
                    {
                        $("#sms-body").val($("#sms-body").val() + html);
                         $('#loading').html("");
                    }
                });

    });
</script>



<script>
    /*
     Rangy Text Inputs, a cross-browser textarea and text input library plug-in for jQuery.
     
     Part of Rangy, a cross-browser JavaScript range and selection library
     http://code.google.com/p/rangy/
     
     Depends on jQuery 1.0 or later.
     
     Copyright 2010, Tim Down
     Licensed under the MIT license.
     Version: 0.1.205
     Build date: 5 November 2010
     */
    (function (n) {
        function o(e, g) {
            var a = typeof e[g];
            return a === "function" || !!(a == "object" && e[g]) || a == "unknown"
        }
        function p(e, g, a) {
            if (g < 0)
                g += e.value.length;
            if (typeof a == "undefined")
                a = g;
            if (a < 0)
                a += e.value.length;
            return{start: g, end: a}
        }
        function k() {
            return typeof document.body == "object" && document.body ? document.body : document.getElementsByTagName("body")[0]
        }
        var i, h, q, l, r, s, t, u, m;
        n(document).ready(function () {
            function e(a, b) {
                return function () {
                    var c = this.jquery ? this[0] : this, d = c.nodeName.toLowerCase();
                    if (c.nodeType ==
                            1 && (d == "textarea" || d == "input" && c.type == "text")) {
                        c = [c].concat(Array.prototype.slice.call(arguments));
                        c = a.apply(this, c);
                        if (!b)
                            return c
                    }
                    if (b)
                        return this
                }
            }
            var g = document.createElement("textarea");
            k().appendChild(g);
            if (typeof g.selectionStart != "undefined" && typeof g.selectionEnd != "undefined") {
                i = function (a) {
                    return{start: a.selectionStart, end: a.selectionEnd, length: a.selectionEnd - a.selectionStart, text: a.value.slice(a.selectionStart, a.selectionEnd)}
                };
                h = function (a, b, c) {
                    b = p(a, b, c);
                    a.selectionStart = b.start;
                    a.selectionEnd =
                            b.end
                };
                m = function (a, b) {
                    if (b)
                        a.selectionEnd = a.selectionStart;
                    else
                        a.selectionStart = a.selectionEnd
                }
            } else if (o(g, "createTextRange") && typeof document.selection == "object" && document.selection && o(document.selection, "createRange")) {
                i = function (a) {
                    var b = 0, c = 0, d, f, j;
                    if ((j = document.selection.createRange()) && j.parentElement() == a) {
                        f = a.value.length;
                        d = a.value.replace(/\r\n/g, "\n");
                        c = a.createTextRange();
                        c.moveToBookmark(j.getBookmark());
                        j = a.createTextRange();
                        j.collapse(false);
                        if (c.compareEndPoints("StartToEnd", j) >
                                -1)
                            b = c = f;
                        else {
                            b = -c.moveStart("character", -f);
                            b += d.slice(0, b).split("\n").length - 1;
                            if (c.compareEndPoints("EndToEnd", j) > -1)
                                c = f;
                            else {
                                c = -c.moveEnd("character", -f);
                                c += d.slice(0, c).split("\n").length - 1
                            }
                        }
                    }
                    return{start: b, end: c, length: c - b, text: a.value.slice(b, c)}
                };
                h = function (a, b, c) {
                    b = p(a, b, c);
                    c = a.createTextRange();
                    var d = b.start - (a.value.slice(0, b.start).split("\r\n").length - 1);
                    c.collapse(true);
                    if (b.start == b.end)
                        c.move("character", d);
                    else {
                        c.moveEnd("character", b.end - (a.value.slice(0, b.end).split("\r\n").length -
                                1));
                        c.moveStart("character", d)
                    }
                    c.select()
                };
                m = function (a, b) {
                    var c = document.selection.createRange();
                    c.collapse(b);
                    c.select()
                }
            } else {
                k().removeChild(g);
                window.console && window.console.log && window.console.log("TextInputs module for Rangy not supported in your browser. Reason: No means of finding text input caret position");
                return
            }
            k().removeChild(g);
            l = function (a, b, c, d) {
                var f;
                if (b != c) {
                    f = a.value;
                    a.value = f.slice(0, b) + f.slice(c)
                }
                d && h(a, b, b)
            };
            q = function (a) {
                var b = i(a);
                l(a, b.start, b.end, true)
            };
            u = function (a) {
                var b =
                        i(a), c;
                if (b.start != b.end) {
                    c = a.value;
                    a.value = c.slice(0, b.start) + c.slice(b.end)
                }
                h(a, b.start, b.start);
                return b.text
            };
            r = function (a, b, c, d) {
                var f = a.value;
                a.value = f.slice(0, c) + b + f.slice(c);
                if (d) {
                    b = c + b.length;
                    h(a, b, b)
                }
            };
            s = function (a, b) {
                var c = i(a), d = a.value;
                a.value = d.slice(0, c.start) + b + d.slice(c.end);
                c = c.start + b.length;
                h(a, c, c)
            };
            t = function (a, b, c) {
                var d = i(a), f = a.value;
                a.value = f.slice(0, d.start) + b + d.text + c + f.slice(d.end);
                b = d.start + b.length;
                h(a, b, b + d.length)
            };
            n.fn.extend({getSelection: e(i, false), setSelection: e(h,
                        true), collapseSelection: e(m, true), deleteSelectedText: e(q, true), deleteText: e(l, true), extractSelectedText: e(u, false), insertText: e(r, true), replaceSelectedText: e(s, true), surroundSelectedText: e(t, true)})
        })
    })(jQuery);
</script>
