<?php

$form = $this->beginWidget('CActiveForm', array(
    'id' => 'sms-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-vertical',
        'role' => 'form',
    )
        ));

?>
<?php echo $form->hiddenField($comments, 'SmsID', array('value' =>$model->SmsID)); ?>
                <?php echo $form->hiddenField($comments, 'Stage', array('value' => SmsModuleController::MENU_VERIFY)); ?>

<div class="form-group">
        <?php echo $form->labelEx($model, 'Recepient', array('class' => 'col-lg-3 control-label')); ?>
    
        <div class="col-lg-8">
            <?php echo CHtml::error($model, 'Recepient') ?>
                <?php echo $model->SmsType == Sms::QUICKSMS ? 
                        $form->textArea($model, 'Recepient', array( 'value' => $model->Recepient , "class" => 'form-control' , 'readonly' => $act == "edit" ? false : true, )): 

                        $form->textField($model, 'Recepient', array( 'value' =>  $model->grp,'class' => 'form-control', 'readonly' => $act == "edit" ? false : true,)); ?>
</div>
</div>
<div class="form-group">
        <?php echo $form->labelEx($model, 'Body', array('class' => 'col-lg-3 control-label')); ?>
    <small class="text-muted"  id="charNum">You have 160 characters left
</small>
        <div class="col-lg-8">
            <?php echo CHtml::error($model, 'Body') ?>
                <?php echo $form->textArea($model, 'Body', array('readonly' => $act == "edit" ? false : true,
                    'class' => 'form-control',  'cols'=>50, 'rows'=> 3, "value"=> $act == "edit" ? $model->Body:$model->preview)); ?>
            </div>
</div>
<?php if($act != 'edit'): ?>
<div class="form-group">
        <?php echo $form->labelEx($comments, 'Comments', array('class' => 'col-lg-3 control-label')); ?>
    
        <div class="col-lg-8">
            <?php echo CHtml::error($comments, 'Comments') ?>
                <?php echo $form->textArea($comments, 'Comments', array('class' => 'form-control', 'rows' => 2  )); ?>
                
            </div>
</div>

      
             
  
<div class="clearfix form-actions">
                 <div class=" col-md-8 col-lg-offset-2">
                     <button class="btn btn-success" name="approve" type="submit"><i class="icon-ok bigger-110"></i> <?php echo Lang::t('Accept sms') ?></button>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <button class="btn btn-danger" name="reject" type="submit"><i class="icon-edit-sign bigger-110"></i> <?php echo Lang::t('Return for Editing') ?></button>
                
                 </div>
                 
</div>
 <?php else: ?>
 
<div class="clearfix form-actions">
                 <div class=" col-md-8 col-lg-offset-2">
                     <button class="btn btn-success" name="update" type="submit"><i class="icon-ok bigger-110"></i> <?php echo Lang::t('Update sms') ?></button>
                
                </div>
                 
</div>
 <?php endif; ?> 

<?php $this->endWidget();  
