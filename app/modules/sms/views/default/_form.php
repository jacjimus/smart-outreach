<?php

$form = $this->beginWidget('CActiveForm', array(
    'id' => 'quick-sms-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
        'role' => 'form',
    )
        ));
?>


<?php
 if($_nt):
            
        $comment = Notification::model()->get($_nt , "Message");
        $user = UsersView::model()->get(Notification::model()->get($_nt , "User_account") , "name");
          echo "<div class=\"alert alert-block alert-danger clearfix\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><i class=\"icon-remove\"></i></button>
                        <div><span class='text-muted Italic'>Reviewed by $user:</span>   '$comment'</div>
                </div>" ; 
           
        endif; 
        ?>
<div class="form-group">
                <?php echo $form->labelEx($model, 'Recepient', array('class' => 'col-lg-3 control-label')); ?>
    <small class="text-muted">Enter phone numbers separated by comma</small>
                <div class="col-lg-5">
             <?php
            echo $form->textArea($model, 'Recepient', array('class' => "form-control" , "cols"=>"50" , 'rows' => '2' ,'required'=> true));
             ?>
                        
            <?php echo CHtml::error($model, 'Recepient') ?>
                </div>
        </div>
<div class="form-group">
                <?php echo $form->labelEx($model, 'SenderID', array('class' => 'col-lg-3 control-label')); ?>
                <div class="col-lg-5">
             <?php echo $form->textField($model, 'SenderID', array('class' => "form-control" , "value" => Sender::model()->get(Accounts::model()->get(Yii::app()->user->account , "Sender_id"), "SenderID"), 'readonly' => true));
              ?>
                        
            <?php echo CHtml::error($model, 'SenderID') ?>
                </div>
 </div>
<div class="form-group">
                <?php echo $form->labelEx($model, 'Schedule', array('class' => 'col-lg-3 control-label')); ?>
                <div class="col-lg-5">
             <?php echo $form->textField($model, 'Schedule', array('class' => "form-control " , 'id'=> 'datepicker' , 'value' => date('Y-m-d')));
              ?>
                        
            <?php echo CHtml::error($model, 'Schedule') ?>
                </div>
 </div>
<div class="form-group">
                <?php echo $form->labelEx($model, 'Time', array('class' => 'col-lg-3 control-label')); ?>
                <div class="col-lg-5">
            <?php echo  CHtml::textField('Time', date('h:i'), array('class'=>'timepicker',  'style' => 'width:150px')); ?>
                       
            <?php echo CHtml::error($model, 'Time') ?>
                </div>
 </div>
<div class="form-group">
        <?php echo $form->labelEx($model, 'Body', array('class' => 'col-lg-3 control-label')); ?>
    <small class="text-muted"  id="charNum">You have 160 characters left
</small>
        <div class="col-lg-5">
            <?php echo CHtml::error($model, 'Body') ?>
                <?php echo $form->textArea($model, 'Body', array('class' => 'form-control ', 'rows' => 6 , 'cols'=>50, "onkeyup"=>"countChar(this)" , 'id'=> 'body','required'=> true)); ?>
            </div>
</div>
<div class="form-group">
        <?php echo $form->labelEx($model, 'Signature', array('class' => 'col-lg-3 control-label')); ?>
    
        <div class="col-lg-5">
            <?php echo CHtml::error($model, 'Signature') ?>
                <?php
                    $datas = Signatures::model()->findAll();
                    $sigs = array();

                    foreach ($datas as $ds)
                        $sigs[$ds->id] = $ds->name;

                    echo $form->dropDownList($model, 'Signature', $sigs, array('prompt' => '[No Footer]',  'class' => 'forn-control chosen-select', 'id' => "signature"
                        ));
                    ?>
            <span id="loading"></span>
            </div>
</div>

        
             
  
<div class="clearfix form-actions">
        <div class="col-lg-offset-2 col-lg-9">
                <button class="btn btn-primary" type="submit"><i class="icon-ok bigger-110"></i> <?php echo Lang::t('submit for peer review') ?></button>
                &nbsp; &nbsp; &nbsp;
                <a class="btn" href="<?php echo $this->createUrl('manage') ?>"><i class="icon-remove bigger-110"></i><?php echo Lang::t('Cancel') ?></a>
        </div>
</div>
<?php $this->endWidget(); ?>


    <script>
      function countChar(val) {
        var len = val.value.length;
        if (len >= 161) {
          val.value = val.value.substring(0, 160);
        } else {
          $('#charNum').html('You have ' + (160 - len) + ' characters left');
        }
      };
      
      
      $('#signature').on('change', function (e) {
        e.preventDefault();
        var num = $(this).val();
        var optionSelected = "value=" + num;
        $('#loading').html("<img src= '<?php echo Yii::app()->request->baseUrl ?>/themes/default/images/ajax-loader.gif '/>");
        $.ajax
                ({
                    type: "POST",
                    url: "addsig",
                    data: optionSelected,
                    cache: false,
                    success: function(html)
                    {
                        $("#body").val($("#body").val() + html);
                         $('#loading').html("");
                    }
                });

    });
    </script>
    
    

<?php
Yii::app()->clientScript
        ->registerCssFile(Yii::app()->theme->baseUrl . '/plugins/chosen/chosen.min.css')
        ->registerScriptFile(Yii::app()->theme->baseUrl . '/plugins/chosen/chosen.jquery.min.js', CClientScript::POS_END)
        ->registerScript('recepient_id', "$('.chosen-select').chosen();");
?>
    