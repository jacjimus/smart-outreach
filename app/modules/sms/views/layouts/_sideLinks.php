<?php if ($this->showLink(UserResources::RES_MESSAGING)): ?>
        <li  class="<?php echo $this->getModuleName() === 'sms' ? 'active open' : '' ?>">
                <a href="#" class="dropdown-toggle">
                        <i class="icon-comments-alt"></i>
                        <span class="menu-text"> <?php echo Lang::t('Messaging Module') ?></span>
                         <b class="arrow icon-angle-down"></b>
                </a>
            <ul class="submenu">
                <?php if ($this->showLink(UserResources::RES_QUICK_MESSAGING)): ?>
                    <li class="<?php echo $this->activeMenu === SmsModuleController::MENU_QUICK ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl(ModulesEnabled::MOD_MESSAGING . '/default/quick') ?>"><i class="icon-envelope"></i><?php echo Lang::t('Outgoing / Send SMS') ?></a></li>
                <?php endif; ?>
                
                <?php if ($this->showLink(UserResources::RES_SMS_INBOX)): ?>
                    <li class="<?php echo $this->activeMenu === SmsModuleController::MENU_INBOX ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl(ModulesEnabled::MOD_MESSAGING . '/default/inbox') ?>"><i class="icon-comments"></i><?php echo Lang::t('Incoming / Inbound SMS') ?></a></li>
                <?php endif; ?>
                    
                <?php if ($this->showLink(UserResources::RES_SMS_E_VERIFY)): ?>
                    <li class="<?php echo $this->activeMenu === SmsModuleController::MENU_E_VERIFY ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl(ModulesEnabled::MOD_MESSAGING . '/verify/') ?>"><i class="icon-comments"></i><?php echo Lang::t('E-verify sub module') ?></a></li>
                <?php endif; ?>
                    
                <?php if ($this->showLink(UserResources::RES_SMS_LOGS)): ?>
                    <li class="<?php echo $this->activeMenu === SmsModuleController::MENU_E_VERIFY ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl(ModulesEnabled::MOD_MESSAGING . '/verify/smslogs') ?>"><i class="icon-comments"></i><?php echo Lang::t('Sms Logs') ?></a></li>
                <?php endif; ?>
                
                        
                </ul>
        </li>
<?php endif; ?>

