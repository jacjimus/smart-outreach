<?php

/**
 * stores the database configurations
 * @author James Makau <jacjimus@gmail.com>
 */

return array(
    'class' => 'CDbConnection',
  // 'connectionString' => 'sqlsrv:Server=PANORAMA\sqlexpress;Database=smartoutreachdb',
    'connectionString' => 'mysql:host=localhost;port=3306;dbname=smartoutreach',
    'emulatePrepare' => true,
    'username' => 'root',
    'password' => '4dm1n2015',
    'schemaCachingDuration' => 600,
    'tablePrefix' => '',
    'enableParamLogging' => false,
    'enableProfiling' => false,
    'charset' => 'utf8',
    'nullConversion' => PDO::NULL_EMPTY_STRING,
    'initSQLs' => array("set time_zone='+03:00';"),
);

