<?php

/**
 * stores the import configurations
 * @author James Makau <jacjimus@gmail.com>
 * @since 1.0
 * @version 1.0
 * @package bluebound
 */
return array(
    'application.models.*',
    'application.models.forms.*',
    'application.components.*',
    'application.components.widgets.*',
    'ext.easyimage.EasyImage',
    // autoloading Quich dialogue Extension
    'ext.quickdlgs.*',
    //user module
    'application.modules.users.models.*',
    'application.modules.users.components.*',
    'application.modules.users.components.behaviors.*',
    //auth module
    'application.modules.auth.models.*',
    'application.modules.auth.components.*',
    
    //Account module
    'application.modules.account.models.*',
    'application.modules.account.components.*',
    
    //Analytics module
    'application.modules.analytics.models.*',
    'application.modules.analytics.components.*',
    //Customization module
    'application.modules.customization.models.*',
    'application.modules.customization.components.*',
    //Messaaging module
    'application.modules.sms.models.*',
    'application.modules.sms.components.*',
    //tools module
    'application.modules.tools.models.*',
    'application.modules.tools.components.*',
    //msg module
    'application.modules.msg.components.*',
    'application.modules.msg.components.smsLib.*',
    'application.modules.msg.models.*',
    
    
   
    // PHPExcel Library
    'application.vendors.phpexcel.PHPExcel',
    'application.ext.yiireport.*',
    'application.ext.YiiMailer.YiiMailer',
    
    
    // autoloading model and component classes
   

);
