<?php

/**
 * Description of MyPHPExcelHelper
 *
 * @author Fred <mconyango@gmail.com>
 */
class MyPHPExcelHelper
{

    /**
     *
     * @param type $file
     * @return type
     */
    public static function getSheetNames($file)
    {
        $file_type = self::getInputFileType($file);
        $objReader = PHPExcel_IOFactory::createReader($file_type);
        $objReader->setReadDataOnly(true);
        $chunkFilter = new MyPHPExcelChunkReadFilter(range('A', 'D'));
        $objReader->setReadFilter($chunkFilter);
        $chunkFilter->setRows(1, 2);
        $objPHPExcel = $objReader->load($file);
        return $objPHPExcel->getSheetNames();
    }

    public static function registerPHPExcel()
    {
        spl_autoload_unregister(array('YiiBase', 'autoload'));
        Yii::import('application.vendors.PHPExcel.Classes.PHPExcel', true);
        spl_autoload_register(array('YiiBase', 'autoload'));
    }

    /**
     *
     * @param type $file
     */
    public static function getInputFileType($file)
    {
        $ext = FileHelper::getFileExtension($file);
        if ($ext === 'xls')
            $input_file_type = 'Excel5';
        else if ($ext === 'xlsx')
            $input_file_type = 'Excel2007';
        else {
            $input_file_type = 'CSV';
        }
        return $input_file_type;
    }

}
