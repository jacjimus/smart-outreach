<?php

/**
 * This class is mainly used for defining system constants that is not associated to any particular model
 * @author James Makau <jacjimus@gmail.com>
 */
class Constants {

        //general settings keys

        const CATEGORY_GENERAL = 'general';
        const KEY_MAINTENACE_MODE = 'maintenance_mode';
        const KEY_ADMIN_EMAIL = 'admin_email';
        const KEY_COMPANY_NAME = 'company_name';
        const KEY_COMPANY_EMAIL = 'company_email';
        const KEY_CURRENCY = 'currency';
        const KEY_PAGINATION = 'pagination';
        const KEY_SITE_NAME = 'site_name';
        const KEY_COMPANY_ADDRESS = 'company_address';
        const KEY_MIN_SMS_THREASHHOLD = 'min_sms_threashhold';
        const KEY_VENDOR_EMAIL = 'vendor_email';
        const KEY_MAIN_ACC_BALANCE = 'main_acc_balance';
        const KEY_DEFAULT_TIMEZONE = 'default_timezone';
        const KEY_LOGO = 'logo';
        //social media
        const CATEGORY_SOCIAL_MEDIA = 'social_media';
        const KEY_SM_FACEBOOK_PAGE = 'sm_facebook_page';
        const KEY_SM_TWITTER_PAGE = 'sm_twitter_page';
        const KEY_SM_LINKEDIN_PAGE = 'sm_linkedin_page';
        const KEY_SM_GOOGLEPLUS_PAGE = 'sm_googleplus_page';
        const KEY_SM_YOUTUBE_PAGE = 'sm_youtube_page';
        
        //advanced settings
        const CATEGORY_ADVANCED = 'advanced';
        const KEY_PHP_PATH = 'PHP_PATH';
        const KEY_JOBS_LOG_FILE = 'JOBS_LOG_FILE';
        //sms settings
        const CATEGORY_SMS = 'sms';
        const KEY_SMS_GATEWAY = 'sms_gateway';
        const KEY_GATEWAY_USERNAME = 'gateway_username';
        const KEY_GATEWAY_PASSWORD = 'gateway_password';
        const KEY_SMS_SECURITY = 'sms_security';
        
        //email settings
        const CATEGORY_EMAIL = 'email';
        const KEY_EMAIL_MAILER = 'email_mailer';
        const KEY_EMAIL_HOST = 'email_host';
        const KEY_EMAIL_PORT = 'email_port';
        const KEY_EMAIL_USERNAME = 'email_username';
        const KEY_EMAIL_PASSWORD = 'email_password';
        const KEY_EMAIL_SECURITY = 'email_security';
        const KEY_EMAIL_MASTER_THEME = 'email_master_theme';
        const KEY_EMAIL_SENDMAIL_COMMAND = 'email_sendmail_command';
        
        //survey constants
        const CATEGORY_SURVEY = 'survey';
        const KEY_SURVEY_LANG_SELECTION = 'lang_selection';
        //active record constants
        const LABEL_CREATE = 'Add';
        const LABEL_UPDATE = 'Edit';
        const LABEL_DELETE = 'Delete';
        const LABEL_VIEW = 'View details';
        //miscelleneous constants
        const SPACE = ' ';
        //queue manager
        const CATEGORY_QUEUEMANAGER = 'queuemanager';
        const KEY_QUEUEMANAGER_STATUS = 'queuemanager_status';
        

}
