
<div class="breadcrumbs breadcrumbs-fixed" id="breadcrumbs">
        <?php
        $this->widget('zii.widgets.CBreadcrumbs', array(
            'homeLink' => '<li>' . CHtml::link('<i class="icon-dashboard"></i> Home', array('/')) . '</li>',
            'links' => $this->breadcrumbs,
            'tagName' => 'ul',
            'separator' => '<li>',
            'htmlOptions' => array('class' => 'breadcrumb'),
        ));
        ?>
        
</div>
