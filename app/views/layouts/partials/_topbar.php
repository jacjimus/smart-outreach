<div class="navbar navbar-default navbar-fixed-top" id="navbar">
    <div class="navbar-container" id="navbar-container">
        <div class="navbar-header pull-left" style="margin-left: -8px !important;">
            <a href="<?php echo $this->home_url ?>" class="left">
                <?php
                /**
                 * The system allows for use of customised Logo. It will pick the user department's logo, if its not set\
                 * then the Main account logo will be used
                 */
                $dept = Yii::app()->user->account;
                $folder = Accounts::model()->get($dept, 'AccountName');
                $logo = Accounts::model()->get($dept, 'Account_logo');
                $acc_logo = Yii::app()->baseUrl . DS . "public" . DS . "accounts" . DS . $folder . DS . $logo;
                $path = !empty($acc_logo) ? $acc_logo : Yii::app()->baseUrl . DS . "public" . DS . "accounts" . DS . "Main" . DS . Yii::app()->settings->get(Constants::CATEGORY_GENERAL, Constants::KEY_LOGO);
                
                echo CHtml::image($path, '', array('height' => "42px", 'width' => '200px', 'align' => 'left')) . '   '
                ?>
            </a><!-- /.brand -->
        </div><!-- /.navbar-header -->
        <div class="navbar-header pull-left" style="margin-left: -8px !important;">
            <a href="<?php echo $this->home_url ?>" class="navbar-brand">
                <small><?php echo CHtml::encode($this->settings[Constants::KEY_SITE_NAME]) ; ?></small>
                
            </a><!-- /.brand -->
            
        </div><!-- /.navbar-header -->
        <div class="navbar-header pull-right" role="navigation">
            <ul class="nav ace-nav">
                <?php $this->renderPartial('application.views.widgets._topbar_messages') ?>
                <?php $this->renderPartial('application.views.layouts.partials._topbar_usermenu') ?>
            </ul><!-- /.ace-nav -->
        </div><!-- /.navbar-header -->
    </div><!-- /.container -->
</div>