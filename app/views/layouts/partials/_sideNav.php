
<div class="sidebar sidebar-fixed<?php echo $this->sidebar_collapsed ? ' menu-min' : '' ?>" id="sidebar">
<?php
/*
 * check if User id login in for the first time and hasnt changed password and prompty for that!
 */
$id = Yii::app()->user->id;
 $changed  = Users::model()->get($id , 'password_change');
 if($changed <> 1):
?>
        <div class="sidebar-shortcuts" id="sidebar-shortcuts">
                <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
                
              
                </div>
                <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
                        <span class="btn btn-success"></span>
                        <span class="btn btn-info"></span>
                        <span class="btn btn-warning"></span>
                        <span class="btn btn-danger"></span>
                </div>
        </div><!-- #sidebar-shortcuts -->
 
        <ul class="nav nav-list my-nav">
            <li class="<?php echo $this->getModuleName() === 'admin' ? 'active open' : '' ?>"><a href="<?php echo Yii::app()->createUrl('admin/default/index') ?>" class=" dropdown-toggle">
                        <i class="icon-dashboard"></i>
                        <span class="menu-text"> <?php echo Lang::t('Dashboard') ?> </span>
                    <b class="arrow icon-angle-down"></b>
                    </a>
                     <ul class="submenu">
                       <li class="<?php echo $this->activeMenu === AdminModuleController::MENU_DASHBOARD ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('admin/default/index') ?>"><i class="icon-double-angle-right"></i><?php echo Lang::t('Home') ?></a></li>
                       </ul>  
                     
                </li>
                <!--Messaging Module-->
                
                 <?php
                 
                if (Controller::isModuleEnabled(ModulesEnabled::MOD_MESSAGING)):
                        $this->renderPartial(ModulesEnabled::MOD_MESSAGING . '.views.layouts._sideLinks');
                endif;
                ?>
                              
                <!--SMS Analytics Module-->
                 <?php
                if (Controller::isModuleEnabled(ModulesEnabled::MOD_ANALYTICS)):
                        $this->renderPartial(ModulesEnabled::MOD_ANALYTICS . '.views.layouts._sideLinks');
                endif;
                ?>
                
                
                <!--System Tools Module-->
                 <?php
                if (Controller::isModuleEnabled(ModulesEnabled::MOD_TOOLS)):
                        $this->renderPartial(ModulesEnabled::MOD_TOOLS . '.views.layouts._sideLinks');
                endif;
                ?>
                
                <!--Account Management Module-->
                 <?php
                if (Controller::isModuleEnabled(ModulesEnabled::MOD_ACCOUNT)):
                        $this->renderPartial(ModulesEnabled::MOD_ACCOUNT_MANAGEMENT . '.views.layouts._sideLinks');
                endif;
                ?>
                
                <!--User Management Module-->
                
                <?php
                 if ($this->showLink(UserResources::RES_USER_ACL)):
                        $this->renderPartial('users.views.layouts._sideLinks');
                endif;
                ?>
                <?php 
                if ($this->showLink(UserResources::RES_SETTINGS_GENERAL)):
                    $this->renderPartial('customization.views.layouts._sideLinks');
                endif;?>
        </ul><!-- /.nav-list -->
        
       

        <div class="sidebar-collapse" id="sidebar-collapse">
                <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
        </div>
         <?php
         
         endif; // end password change check?>
</div>
