<?php
$notifications = Notification::model()->findAll("Status LIKE '" . Notification::STATUS_UNREAD . "' AND Destination = " . Yii::app()->user->id);
$no = count($notifications);
?>
<li class="">
            <a data-toggle="dropdown" class="dropdown-toggle">
                     <span class="badge badge-important">
                        <?php
                    $dept = Yii::app()->user->account;
                    $balance = Accounts::model()->get($dept, 'Credit');
                    $crdt = $balance <  Yii::app()->settings->get(Constants::CATEGORY_GENERAL, Constants::KEY_MIN_SMS_THREASHHOLD) ? true : false;
                    echo Accounts::model()->get($dept, 'AccountName') . " Balance: ". number_format($balance, 0, '.', ',');?>
                     </span>
            </a>
    
    <ul class="dropdown-menu-right dropdown-navbar navbar-pink dropdown-menu dropdown-caret dropdown-close ">
<?php if($crdt AND $this->showLink(UserResources::RES_ACCOUNT_BORROW)):?>
<li class="dropdown-header">
<div class="clearfix">
<span class="pull-left">
<?php echo CHtml::link("<i class=\"ace-icon fa fa-briefcase\"></i>&nbsp;&nbsp;Borrow Sms Credit" , Yii::app()->controller->createUrl('/account/accounts/borrow'))?>
</span>
</div> 
</li>
<?php  endif; ?>
<?php if($this->showLink(UserResources::RES_ACCOUNT_PURCHASE AND Accounts::model()->getScaler('purchase_status' , "id = " . Yii::app()->user->account) == Accounts::PURCHASE_INACTIVE)) :?>
<li class="dropdown-header">
<div class="clearfix">
<span class="pull-left">
                                           
<?php echo CHtml::link("<i class=\"ace-icon fa fa-credit-card\"></i>&nbsp;&nbsp;Purchase Credit" , Yii::app()->controller->createUrl('/account/accounts/purchase') , array("class" => "btn btn-link show-colorbox"))?>
</span>
</div> 
</li>
<?php 
else:
 ?> 
<li class="dropdown-header">
<div class="clearfix">
<span class="pull-left">
                                           
<?php echo CHtml::link("<i class=\"ace-icon fa fa-eye\"></i>Purchase submitted , View" , Yii::app()->controller->createUrl('/account/accounts/purchase' , array("_vw" => true)) , array("class" => "btn btn-link show-colorbox"))?>
</span>
</div> 
</li>
<?php endif; ?>
</ul>
    
</li>

<?php if ($no >= 1): ?>


    <li class="green" id="topbar_messages">
        <a data-toggle="dropdown" class="dropdown-toggle" href="#" id="topbar_messages_anchor" data-ajax-url="<?php echo Yii::app()->createUrl('msg/message/getNotifications') ?>" data-unread="<?= $no ?>">
            <i class="icon-envelope icon-animated-bell"></i>
            <span class="badge badge-danger unread "><?= $no ?></span>
        </a>
        <ul id="topbar_messages_list" class="pull-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">

            <li class="dropdown-header">
                <i class="ace-icon fa fa-envelope-o "></i>
                <?= $no ?> Messages
            </li>
             <?php
                    foreach ($notifications As $note):
                        $id = Comments::model()->get($note->Commentid, "SmsID");
                        $type = strtolower(Sms::model()->get(Comments::model()->get($note->Commentid, "SmsID"), "SmsType"));
                        ?>
                        <li>
                            
                                <?php echo CHtml::link('<span class="msg-body"><span class="msg-title Bold">'
                                        .  MyYiiUtils::myShortenedString( $note->Message , 25 , '...',25 ). '<span class="msg-time"><i class="ace-icon fa fa-clock-o"></i>
					<span>' . $note->Period . '</span></span></span>', Yii::app()->controller->createUrl("/sms/default/$type", array("id" => $id, '_nt' => $note->Id)), array('class' => 'clearfix' , "title" => $note->Message))
                                ?>
                        </li>

                <?php endforeach; ?>





            <li class="dropdown-footer">
                <a href="inbox.html">
                    See all messages
                    <i class="ace-icon fa fa-arrow-right"></i>
                </a>
            </li>

        </ul>
    <?php endif; ?>
    <?php
    Yii::app()->clientScript->registerScript('msg.message.notification', "MyController.Msg.Message.init();");
    ?>
