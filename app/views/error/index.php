<!DOCTYPE html>
<html lang="en">
        <?php $this->renderPartial('application.views.layouts.partials._head') ?>
        <body style="background: #E9E9E9;">
                <div class="main-container">
                        <div id="error-unit">
                                <i class="icon-frown"></i>
                                <h1><?php echo $error['code']; ?> Something went wrong </h1>
                                <h3 class="uk-animation-slide-right">
                                        <?php if ($error['code'] == 404): ?>
                                                The page you have requested does not exist. Check the address you have typed in the browser or go back home.
                                        <?php elseif ($error['code'] == 403): ?>
                                                You do not have the privilege to access this page.
                                        <?php elseif ($error['code'] == 400): ?>
                                                <?php echo CHtml::encode($error['message']); ?>
                                        <?php elseif ($error['code'] == 500): ?>
                                                <?php echo CHtml::encode($error['message']); ?>
                                        <?php else: ?>
                                                Sorry for this, Kindly get in touch with out technical gurus <a href="mailto:support@mombasa.go.ke">support@mombasa.go.ke</a>, Thank you for continued support.
                                        <?php endif; ?>
                                        
                                </h3>
                        </div>
                </div><!--/.main-container-->
        </body>
</html>