<?php

/**
 * All cron jobs actions are defined here.
 * @author James Makau  <jacjimus@gmail.com>
 */
class OldCommand extends CConsoleCommand {

   public $date;

    public function init() {

        parent::init();
        $this->date = date("Y-m-d");
    }

    
    /**
     * Start a Task
     * @param type $task_id
     * @return type
     */
    public function actionRespond() {
        try {

            $sms = Incoming::model()->findAll("status = '" . Incoming::STATUS_NEW . "' AND " . "SUBSTRING(text, 1, 3) = 'dkt'");
            //var_dump($sms);die;
            if ($sms):
                foreach ($sms As $s):
                    $code = substr($s->text, 3);
                    $data = Yii::app()->db->createCommand()
                            ->select("*")
                            ->from("tbl_doctors")
                            ->where("regno = :id", array(":id" => $code))
                            ->queryAll();
                    if ($data)
                        $text = "As at " . date("d-M-Y") . ", No displinary case on the Doc Reg no registered as follows \n"
                                . "Name: " . $data[0]['name'] . " \n"
                                . " Contact: " . $data[0]['contact'] . ", \n"
                                . "Specialization: " . $data[0]['specialization'] . "\n"
                                . "Address: " . $data[0]['address'] . ". \n";
                    else
                        $text = "Sorry either you didnt query with the format dkt123 Where 123 is the Doctors Reg No OR the doctor is NOT Registered. Kindly report "
                                . "the matter to us by smsing the details starting with keyword fkdkt and send to 21124 ";

                    $model = new Smslogs();
                    $model->Body = $text;
                    $model->Recepient = $s->sender;
                    $model->SenderID = "KONVERGENZ";
                    $model->SmsType = "QUERY";
                    $model->Status = Smslogs::STATUS_PENDING;
                    if ($model->save(false)) {
                        $update = Incoming::model()->loadModel($s->id);
                        $update->status = Incoming::STATUS_READ;
                        $update->save(FALSE);
                    }
                endforeach;

            endif;
        } catch (Exception $exc) {
            Yii::log($exc->getMessage(), CLogger::LEVEL_ERROR);
        }
    }
    
/*   
 * Archive all the smses which have been send to the 
 *  archives table 
 */
 
    public function actionArchive()
    {
        
        //$send = Smslogs::model()->findAll("Status LIKE 'Success' LIMIT 100");
        $send = Yii::app()->db->createCommand()
                ->select("*")
                ->from("tbl_sms_logs")
                ->where("Status=:status", array(':status'=>Smslogs::STATUS_DELIVERED))
                ->limit(5000)
                ->queryAll();
        foreach($send As $sms):
            $model = new Smslogsarchive();
            $model->attributes = $sms;
            if($model->save(false))
            Smslogs::model()->loadModel($sms['SmsID'])->delete();
       // sleep(5);
        endforeach;
        
    }

}